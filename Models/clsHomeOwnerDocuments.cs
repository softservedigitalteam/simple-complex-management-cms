﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsHomeOwnerDocuments
    {
        public int iHomeOwnerDocumentID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeOwnerID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Title must be at least 2 characters long")]
        [Remote("checkIfHomeOwnerDocumentExists", "HomeOwnerDocuments", HttpMethod = "POST", ErrorMessage = "Title already exists")]
        public string strTitle { get; set; }
        public string strDescription { get; set; }
        public string strPathToDocuments { get; set; }
        public bool bIsDeleted { get; set; }

        public clsHomeOwners clsHomeOwner { get; set; }
    }
}
