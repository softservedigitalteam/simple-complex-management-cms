﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsPortfolios
    {
        public int iPortfolioID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Title must be at least 2 characters long")]
        [Remote("checkIfPorfolioExists", "Porfolios", HttpMethod = "POST", ErrorMessage = "Title already exists")]
        public string strTitle { get; set; }
        public string strEmailAddress1 { get; set; }
        public string strEmailAddress2 { get; set; }
        public string strEmailAddress3 { get; set; }
        public string strEmailAddress4 { get; set; }
        public string strEmailAddress5 { get; set; }
        public string strEmailAddress6 { get; set; }
        public string strEmailAddress7 { get; set; }
        public string strEmailAddress8 { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsFormTickets> lstFormTickets { get; set; }
    }
}
