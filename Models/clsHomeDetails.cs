﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsHomeDetails
    {
        public int iHomeDetailID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(150, MinimumLength = 2, ErrorMessage = "Email must be at least 2 characters long")]
        public string strEmailAddress { get; set; }
        public string strLatitude { get; set; }
        public string strLongitude { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string strUnitNumber { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string strBlockNumber { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeOwnerID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iNumberOfPeopleInHome { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iNumberOfStaffMembers { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iNumberOfVehicles { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iNumberOfPets { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public bool bIsRented { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iLeasingPeriodID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeOccupantID { get; set; }
        public bool bIsDeleted { get; set; }

      
        public List<clsFormTickets> lstFormTickets { get; set; }
        public List<clsHomeHistoryLogs> lstHomeHistoryLogs { get; set; }
        public List<clsHomeOccupants> lstHomeOccupants { get; set; }
        public List<clsHomePets> lstHomePets { get; set; }
        public List<clsHomeStaffMembers> lstHomeStaffMembers { get; set; }
        public List<clsHomeVehicles> lstHomeVehicles { get; set; }
        public clsHomeOwners clsHomeOwner { get; set; }
        public clsLeasingPeriods clsLeasingPeriod { get; set; }
    }
}
