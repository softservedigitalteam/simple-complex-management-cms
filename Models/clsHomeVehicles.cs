﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsHomeVehicles
    {
        public int iHomeVehicleID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeDetailID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iMakeID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Model must be at least 2 characters long")]
        public string strModel { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iColourID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string strRegistrationNumber { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        public bool bIsDeleted { get; set; }

        public clsColours clsColour { get; set; }
        public clsHomeDetails clsHomeDetail { get; set; }
        public clsMakes clsMake { get; set; }
    }
}
