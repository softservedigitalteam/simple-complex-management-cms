﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsHomeHistoryLogs
    {
        public int iHomeHistoryLogID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeOwnerID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeOccupantID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeDetailID { get; set; }
        public bool bIsDeleted { get; set; }

        public clsHomeDetails clsHomeDetail { get; set; }
        public clsHomeOccupants clsHomeOccupant { get; set; }
        public clsHomeOwners clsHomeOwner { get; set; }
    }
}
