﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsHomeOwners
    {
        public int iHomeOwnerID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        public string strLatitude { get; set; }
        public string strLongitude { get; set; }
        public string strRole { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "First Name must be at least 2 characters long")]
        public string strFirstName { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Surname must be at least 2 characters long")]
        public string strSurname { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(13, MinimumLength = 13, ErrorMessage = "First Name must be 13 characters long")]
        public string strIDNumber { get; set; }
        public string strPassportNumber { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        public string strMobileNumber { get; set; }
        public string strPassword { get; set; }
        public int iValidationNumber { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [EmailAddress(ErrorMessage = "Please enter a valid email")]
        [Remote("checkIfHomeOwnerEmailExists", "HomeOwners", HttpMethod = "POST", ErrorMessage = "Email already exists")]
        public string strEmailAddress { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public bool bIsActive { get; set; }
        public bool bIsDeleted { get; set; }

        public List<clsHomeDetails> lstHomeDetails { get; set; }
        public List<clsHomeHistoryLogs> lstHomeHistoryLogs { get; set; }
        public List<clsHomeOwnerDocuments> lstHomeOwnerDocuments { get; set; }
        public List<clsUsers> lstUsers { get; set; }
    }
}
