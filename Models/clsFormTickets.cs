﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsFormTickets
    {
        public int iFormTicketID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Name must be at least 2 characters long")]
        public string strNameAndSurname { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string strContactNumber { get; set; }
        public string strAlternateContactNumber { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string strEmailAddress { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iBlockNumber { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iUnitNumber { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string strDetailsOfQueryComplaintOrCompliment { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iQueryTypeID { get; set; }
        public int iPortfolioID { get; set; }
        public string strPathToDocuments { get; set; }
        public string strMasterDocument { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeDetailID { get; set; }
        public int iFormTicketStatusID { get; set; }
        public bool bIsResolved { get; set; }
        public bool bIsDeleted { get; set; }

        public clsFormTicketStatuses clsFormTicketStatus { get; set; }
        public clsHomeDetails clsHomeDetail { get; set; }
        public clsPortfolios clsPortfolio { get; set; }
        public clsQueryTypes clsQueryType { get; set; }
    }
}
