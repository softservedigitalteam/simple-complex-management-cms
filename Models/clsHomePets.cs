﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsHomePets
    {
        public int iHomePetID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iHomeDetailID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iAnimalTypeID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(150, MinimumLength = 2, ErrorMessage = "Pet Name must be at least 2 characters long")]
        public string strPetName { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iPetColourID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iPetSizeID { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        public bool bIsDeleted { get; set; }

        public clsAnimalTypes clsAnimalType { get; set; }
        public clsHomeDetails clsHomeDetail { get; set; }
        public clsPetColours clsPetColour { get; set; }
        public clsPetSizes clsPetSize { get; set; }
    }
}
