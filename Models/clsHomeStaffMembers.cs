﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SimpleComplexManagementCMS.Models
{
    public class clsHomeStaffMembers
    {
        public int iHomeStaffMemberID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        public int iHomeDetailID { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(150, MinimumLength = 2, ErrorMessage = "First Name must be at least 2 characters long")]
        public string strFirstNames { get; set; }
        [Required(ErrorMessage = "Field is required")]
        [StringLength(150, MinimumLength = 2, ErrorMessage = "Surame must be at least 2 characters long")]
        public string strSurname { get; set; }
        public string strIDNumber { get; set; }
        public string strPassportNumber { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public string strMobileNumber { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public bool bIsLivingOnSite { get; set; }
        public bool bIsDeleted { get; set; }

        public clsHomeDetails clsHomeDetail { get; set; }
    }
}
