﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace SimpleComplexManagementCMS.Models
{
    public class clsComplexes
    {
        public int iComplexID { get; set; }
        public DateTime dtAdded { get; set; }
        public int iAddedBy { get; set; }
        public DateTime? dtEdited { get; set; }
        public int iEditedBy { get; set; }
        public string strLatitude { get; set; }
        public string strLongitude { get; set; }

        [Required(ErrorMessage = "Field is required")]
        [StringLength(250, MinimumLength = 2, ErrorMessage = "Complex Name must be at least 2 characters long")]
        [Remote("checkIfComplexExists", "Complexes", HttpMethod = "POST", ErrorMessage = "Complex Name already exists")]
        public string strComplexName { get; set; }

        [Required(ErrorMessage = "Field is required")]
        public string strComplexPhysicalAddress { get; set; }
        public string strMangementCompanyName { get; set; }
        [Required(ErrorMessage = "Field is required")]
        public int iNumberOfHomes { get; set; }
        public string strPathToImages { get; set; }
        public string strMasterImage { get; set; }
        [Required(ErrorMessage = "Complex Type is required")]
        public int iComplexTypeID { get; set; }
        public bool bIsDeleted { get; set; }

        public clsComplexTypes clsComplexType { get; set; }
    }
}
