﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.Makes;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class MakesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: Makes
        public ActionResult MakesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsMakesView clsMakesView = new clsMakesView();
            clsMakesManager clsMakesManager = new clsMakesManager();
            clsMakesView.lstMakes = clsMakesManager.getAllMakesOnlyList();

            return View(clsMakesView);
        }

        public ActionResult MakeAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsMakes clsMake = new clsMakes();

            return View(clsMake);
        }

        //Add Make
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MakeAdd(clsMakes clsMake)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsMakesManager clsMakesManager = new clsMakesManager();
            clsMakesManager.saveMake(clsMake);

            //Add successful / notification
            TempData["bIsMakeAdded"] = true;

            return RedirectToAction("MakesView", "Makes");
        }

        //Edit Make
        public ActionResult MakeEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("MakesView", "Makes");
            }

            clsMakesManager clsMakesManager = new clsMakesManager();
            clsMakes clsMake = clsMakesManager.getMakeByID(id);

            return View(clsMake);
        }

        //Edit Make
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult MakeEdit(clsMakes clsMake)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsMakesManager clsMakesManager = new clsMakesManager();
            clsMakes clsExistingMake = clsMakesManager.getMakeByID(clsMake.iMakeID);

            clsExistingMake.strTitle = clsMake.strTitle;
            clsMakesManager.saveMake(clsExistingMake);

            //Add successful / notification
            TempData["bIsMakeUpdated"] = true;

            return RedirectToAction("MakesView", "Makes");
        }

        //Delete Make
        [HttpPost]
        public ActionResult MakeDelete(int iMakeID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iMakeID == 0)
            {
                return RedirectToAction("MakesView", "Makes");
            }

            clsMakesManager clsMakesManager = new clsMakesManager();
            clsMakesManager.removeMakeByID(iMakeID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsMakeDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfMakeExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblMakes.Any(Make => Make.strTitle.ToLower() == strTitle.ToLower() && Make.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}