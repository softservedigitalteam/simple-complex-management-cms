﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.Complexes;
using SimpleComplexManagementCMS.Model_Manager;
using SimpleComplexManagementCMS;

namespace SimpleComplexManagementCMS.Controllers
{
    public class ComplexesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: Complexes
        public ActionResult ComplexesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexesView clsComplexesView = new clsComplexesView();
            clsComplexesManager clsComplexesManager = new clsComplexesManager();
            clsComplexesView.lstComplexes = clsComplexesManager.getAllComplexesOnlyList();

            return View(clsComplexesView);
        }

        public ActionResult ComplexAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexes clsComplex = new clsComplexes();

            return View(clsComplex);
        }

        //Add Complex
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ComplexAdd(clsComplexes clsComplex)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexesManager clsComplexesManager = new clsComplexesManager();
            clsComplexesManager.saveComplex(clsComplex);

            //Add successful / notification
            TempData["bIsComplexAdded"] = true;

            return RedirectToAction("ComplexesView", "Complexes");
        }

        //Edit Complex
        public ActionResult ComplexEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("ComplexesView", "Complexes");
            }

            clsComplexesManager clsComplexesManager = new clsComplexesManager();
            clsComplexes clsComplex = clsComplexesManager.getComplexByID(id);

            return View(clsComplex);
        }

        //Edit Complex
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ComplexEdit(clsComplexes clsComplex)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexesManager clsComplexesManager = new clsComplexesManager();
            clsComplexes clsExistingComplex = clsComplexesManager.getComplexByID(clsComplex.iComplexID);

            clsExistingComplex.strLatitude = clsComplex.strLatitude;
            clsExistingComplex.strLongitude = clsComplex.strLongitude;
            clsExistingComplex.strComplexName = clsComplex.strComplexName;
            clsExistingComplex.strComplexPhysicalAddress = clsComplex.strComplexPhysicalAddress;
            clsExistingComplex.strMangementCompanyName = clsComplex.strMangementCompanyName;

            clsExistingComplex.iNumberOfHomes = clsComplex.iNumberOfHomes;
            clsExistingComplex.strPathToImages = clsComplex.strPathToImages;
            clsExistingComplex.strMasterImage = clsComplex.strMasterImage;
            clsExistingComplex.iComplexTypeID = clsComplex.iComplexTypeID;

            clsComplexesManager.saveComplex(clsExistingComplex);

            //Add successful / notification
            TempData["bIsComplexUpdated"] = true;

            return RedirectToAction("ComplexesView", "Complexes");
        }

        //Delete Complex
        [HttpPost]
        public ActionResult ComplexDelete(int iComplexID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iComplexID == 0)
            {
                return RedirectToAction("ComplexesView", "Complexes");
            }

            clsComplexesManager clsComplexesManager = new clsComplexesManager();
            clsComplexesManager.removeComplexByID(iComplexID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsComplexDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfComplexExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblComplexes.Any(Complex => Complex.strComplexName.ToLower() == strTitle.ToLower() && Complex.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}