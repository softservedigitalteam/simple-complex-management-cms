﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomeDetails;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomeDetailsController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomeDetails
        public ActionResult HomeDetailsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeDetailsView clsHomeDetailsView = new clsHomeDetailsView();
            clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
            clsHomeDetailsView.lstHomeDetails = clsHomeDetailsManager.getAllHomeDetailsOnlyList();

            return View(clsHomeDetailsView);
        }

        public ActionResult HomeDetailAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeDetails clsHomeDetail = new clsHomeDetails();

            return View(clsHomeDetail);
        }

        //Add Home Detail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeDetailAdd(clsHomeDetails clsHomeDetail)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
            clsHomeDetailsManager.saveHomeDetail(clsHomeDetail);

            //Add successful / notification
            TempData["bIsHomeDetailAdded"] = true;

            return RedirectToAction("HomeDetailsView", "HomeDetails");
        }

        //Edit Home Detail
        public ActionResult HomeDetailEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomeDetailsView", "HomeDetails");
            }

            clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
            clsHomeDetails clsHomeDetail = clsHomeDetailsManager.getHomeDetailByID(id);

            return View(clsHomeDetail);
        }

        //Edit Home Detail
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeDetailEdit(clsHomeDetails clsHomeDetail)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
            clsHomeDetails clsExistingHomeDetail = clsHomeDetailsManager.getHomeDetailByID(clsHomeDetail.iHomeDetailID);

            clsExistingHomeDetail.strEmailAddress = clsHomeDetail.strEmailAddress;
            clsExistingHomeDetail.strLatitude = clsHomeDetail.strLatitude;
            clsExistingHomeDetail.strLongitude = clsHomeDetail.strLongitude;
            clsExistingHomeDetail.strUnitNumber = clsHomeDetail.strUnitNumber;
            clsExistingHomeDetail.strBlockNumber = clsHomeDetail.strBlockNumber;

            clsExistingHomeDetail.iHomeOwnerID = clsHomeDetail.iHomeOwnerID;
            clsExistingHomeDetail.iNumberOfPeopleInHome = clsHomeDetail.iNumberOfPeopleInHome;
            clsExistingHomeDetail.iNumberOfStaffMembers = clsHomeDetail.iNumberOfStaffMembers;
            clsExistingHomeDetail.iNumberOfVehicles = clsHomeDetail.iNumberOfVehicles;
            clsExistingHomeDetail.iNumberOfPets = clsHomeDetail.iNumberOfPets;

            clsExistingHomeDetail.bIsRented = clsHomeDetail.bIsRented;
            clsExistingHomeDetail.iLeasingPeriodID = clsHomeDetail.iLeasingPeriodID;

            clsHomeDetailsManager.saveHomeDetail(clsExistingHomeDetail);

            //Add successful / notification
            TempData["bIsHomeDetailUpdated"] = true;

            return RedirectToAction("HomeDetailsView", "HomeDetails");
        }

        //Delete Home Detail
        [HttpPost]
        public ActionResult HomeDetailDelete(int iHomeDetailID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomeDetailID == 0)
            {
                return RedirectToAction("HomeDetailsView", "HomeDetails");
            }

            clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
            clsHomeDetailsManager.removeHomeDetailByID(iHomeDetailID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomeDetailDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfHomeDetailExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblHomeDetails.Any(HomeDetail => HomeDetail.strEmailAddress.ToLower() == strTitle.ToLower() && HomeDetail.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}