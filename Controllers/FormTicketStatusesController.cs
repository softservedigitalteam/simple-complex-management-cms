﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.FormTicketStatuses;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class FormTicketStatusesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: FormTicketStatuses
        public ActionResult FormTicketStatusesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTicketStatusesView clsFormTicketStatusesView = new clsFormTicketStatusesView();
            clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
            clsFormTicketStatusesView.lstFormTicketStatuses = clsFormTicketStatusesManager.getAllFormTicketStatusesOnlyList();

            return View(clsFormTicketStatusesView);
        }

        public ActionResult FormTicketStatusAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTicketStatuses clsFormTicketStatus = new clsFormTicketStatuses();

            return View(clsFormTicketStatus);
        }

        //Add Form Ticket Status
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FormTicketStatusAdd(clsFormTicketStatuses clsFormTicketStatus)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
            clsFormTicketStatusesManager.saveFormTicketStatus(clsFormTicketStatus);

            //Add successful / notification
            TempData["bIsFormTicketStatusAdded"] = true;

            return RedirectToAction("FormTicketStatusesView", "FormTicketStatuses");
        }

        //Edit Form Ticket Status
        public ActionResult FormTicketStatusEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("FormTicketStatusesView", "FormTicketStatuses");
            }

            clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
            clsFormTicketStatuses clsFormTicketStatus = clsFormTicketStatusesManager.getFormTicketStatusByID(id);

            return View(clsFormTicketStatus);
        }

        //Edit Form Ticket Status
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FormTicketStatusEdit(clsFormTicketStatuses clsFormTicketStatus)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
            clsFormTicketStatuses clsExistingFormTicketStatus = clsFormTicketStatusesManager.getFormTicketStatusByID(clsFormTicketStatus.iFormTicketStatusID);

            clsExistingFormTicketStatus.strTitle = clsFormTicketStatus.strTitle;
            clsExistingFormTicketStatus.strColor = clsFormTicketStatus.strColor;
            clsFormTicketStatusesManager.saveFormTicketStatus(clsExistingFormTicketStatus);

            //Add successful / notification
            TempData["bIsFormTicketStatusUpdated"] = true;

            return RedirectToAction("FormTicketStatusesView", "FormTicketStatuses");
        }

        //Delete Form Ticket Status
        [HttpPost]
        public ActionResult FormTicketStatusDelete(int iFormTicketStatusID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iFormTicketStatusID == 0)
            {
                return RedirectToAction("FormTicketStatusesView", "FormTicketStatuses");
            }

            clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
            clsFormTicketStatusesManager.removeFormTicketStatusByID(iFormTicketStatusID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsFormTicketStatusDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfFormTicketStatusExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblFormTicketStatuses.Any(FormTicketStatus => FormTicketStatus.strTitle.ToLower() == strTitle.ToLower() && FormTicketStatus.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}