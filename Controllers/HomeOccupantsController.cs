﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomeOccupants;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomeOccupantsController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomeOccupants
        public ActionResult HomeOccupantsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOccupantsView clsHomeOccupantsView = new clsHomeOccupantsView();
            clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
            clsHomeOccupantsView.lstHomeOccupants = clsHomeOccupantsManager.getAllHomeOccupantsOnlyList();

            return View(clsHomeOccupantsView);
        }

        public ActionResult HomeOccupantAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOccupants clsHomeOccupant = new clsHomeOccupants();

            return View(clsHomeOccupant);
        }

        //Add Home Occupant
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeOccupantAdd(clsHomeOccupants clsHomeOccupant)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
            clsHomeOccupantsManager.saveHomeOccupant(clsHomeOccupant);

            //Add successful / notification
            TempData["bIsHomeOccupantAdded"] = true;

            return RedirectToAction("HomeOccupantsView", "HomeOccupants");
        }

        //Edit Home Occupant
        public ActionResult HomeOccupantEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomeOccupantsView", "HomeOccupants");
            }

            clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
            clsHomeOccupants clsHomeOccupant = clsHomeOccupantsManager.getHomeOccupantByID(id);

            return View(clsHomeOccupant);
        }

        //Edit Home Occupant
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeOccupantEdit(clsHomeOccupants clsHomeOccupant)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
            clsHomeOccupants clsExistingHomeOccupant = clsHomeOccupantsManager.getHomeOccupantByID(clsHomeOccupant.iHomeOccupantID);

            clsExistingHomeOccupant.iHomeDetailID = clsHomeOccupant.iHomeDetailID;
            clsExistingHomeOccupant.strFirstNames = clsHomeOccupant.strFirstNames;
            clsExistingHomeOccupant.strSurname = clsHomeOccupant.strSurname;
            clsExistingHomeOccupant.strMobileNumber = clsHomeOccupant.strMobileNumber;
            clsExistingHomeOccupant.strEmailAddress = clsHomeOccupant.strEmailAddress;

            clsExistingHomeOccupant.strIDNumber = clsHomeOccupant.strIDNumber;
            clsExistingHomeOccupant.strPassportNumber = clsHomeOccupant.strPassportNumber;
            clsExistingHomeOccupant.dtDateOfBirth = clsHomeOccupant.dtDateOfBirth;
            clsExistingHomeOccupant.strPathToImages = clsHomeOccupant.strPathToImages;
            clsExistingHomeOccupant.strMasterImage = clsHomeOccupant.strMasterImage;

            clsExistingHomeOccupant.strIndustryOrProfession = clsHomeOccupant.strIndustryOrProfession;

            clsHomeOccupantsManager.saveHomeOccupant(clsExistingHomeOccupant);

            //Add successful / notification
            TempData["bIsHomeOccupantUpdated"] = true;

            return RedirectToAction("HomeOccupantsView", "HomeOccupants");
        }

        //Delete Home Occupant
        [HttpPost]
        public ActionResult HomeOccupantDelete(int iHomeOccupantID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomeOccupantID == 0)
            {
                return RedirectToAction("HomeOccupantsView", "HomeOccupants");
            }

            clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
            clsHomeOccupantsManager.removeHomeOccupantByID(iHomeOccupantID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomeOccupantDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfHomeOccupantExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblHomeOccupants.Any(HomeOccupant => HomeOccupant.strEmailAddress.ToLower() == strTitle.ToLower() && HomeOccupant.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}