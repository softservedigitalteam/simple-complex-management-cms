﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomeHistoryLogs;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomeHistoryLogsController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomeHistoryLogs
        public ActionResult HomeHistoryLogsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeHistoryLogsView clsHomeHistoryLogsView = new clsHomeHistoryLogsView();
            clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
            clsHomeHistoryLogsView.lstHomeHistoryLogs = clsHomeHistoryLogsManager.getAllHomeHistoryLogsOnlyList();

            return View(clsHomeHistoryLogsView);
        }

        public ActionResult HomeHistoryLogAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeHistoryLogs clsHomeHistoryLog = new clsHomeHistoryLogs();

            return View(clsHomeHistoryLog);
        }

        //Add Home History Log
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeHistoryLogAdd(clsHomeHistoryLogs clsHomeHistoryLog)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
            clsHomeHistoryLogsManager.saveHomeHistoryLog(clsHomeHistoryLog);

            //Add successful / notification
            TempData["bIsHomeHistoryLogAdded"] = true;

            return RedirectToAction("HomeHistoryLogsView", "HomeHistoryLogs");
        }

        //Edit Home History Log
        public ActionResult HomeHistoryLogEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomeHistoryLogsView", "HomeHistoryLogs");
            }

            clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
            clsHomeHistoryLogs clsHomeHistoryLog = clsHomeHistoryLogsManager.getHomeHistoryLogByID(id);

            return View(clsHomeHistoryLog);
        }

        //Edit Home History Log
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeHistoryLogEdit(clsHomeHistoryLogs clsHomeHistoryLog)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
            clsHomeHistoryLogs clsExistingHomeHistoryLog = clsHomeHistoryLogsManager.getHomeHistoryLogByID(clsHomeHistoryLog.iHomeHistoryLogID);

            clsExistingHomeHistoryLog.iHomeOwnerID = clsHomeHistoryLog.iHomeOwnerID;
            clsExistingHomeHistoryLog.iHomeOccupantID = clsHomeHistoryLog.iHomeOccupantID;
            clsExistingHomeHistoryLog.iHomeDetailID = clsHomeHistoryLog.iHomeDetailID;

            clsHomeHistoryLogsManager.saveHomeHistoryLog(clsExistingHomeHistoryLog);

            //Add successful / notification
            TempData["bIsHomeHistoryLogUpdated"] = true;

            return RedirectToAction("HomeHistoryLogsView", "HomeHistoryLogs");
        }

        //Delete Home History Log
        [HttpPost]
        public ActionResult HomeHistoryLogDelete(int iHomeHistoryLogID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomeHistoryLogID == 0)
            {
                return RedirectToAction("HomeHistoryLogsView", "HomeHistoryLogs");
            }

            clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
            clsHomeHistoryLogsManager.removeHomeHistoryLogByID(iHomeHistoryLogID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomeHistoryLogDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        //[HttpPost]
        //public JsonResult checkIfHomeHistoryLogExists(string strTitle)
        //{
        //    bool bCanUseTitle = false;
        //    bool bExists = db.tblHomeHistoryLogs.Any(HomeHistoryLog => HomeHistoryLog.strTitle.ToLower() == strTitle.ToLower() && HomeHistoryLog.bIsDeleted == false);

        //    if (bExists == false)
        //        bCanUseTitle = true;

        //    return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        //}
    }
}