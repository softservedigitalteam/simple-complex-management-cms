﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.ComplexTypes;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class ComplexTypesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: ComplexTypes
        public ActionResult ComplexTypesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexTypesView clsComplexTypesView = new clsComplexTypesView();
            clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();
            clsComplexTypesView.lstComplexTypes = clsComplexTypesManager.getAllComplexTypesOnlyList();

            return View(clsComplexTypesView);
        }

        public ActionResult ComplexTypeAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexTypes clsComplexType = new clsComplexTypes();

            return View(clsComplexType);
        }

        //Add ComplexType
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ComplexTypeAdd(clsComplexTypes clsComplexType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();
            clsComplexTypesManager.saveComplexType(clsComplexType);

            //Add successful / notification
            TempData["bIsComplexTypeAdded"] = true;

            return RedirectToAction("ComplexTypesView", "ComplexTypes");
        }

        //Edit ComplexType
        public ActionResult ComplexTypeEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("ComplexTypesView", "ComplexTypes");
            }

            clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();
            clsComplexTypes clsComplexType = clsComplexTypesManager.getComplexTypeByID(id);

            return View(clsComplexType);
        }

        //Edit ComplexType
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ComplexTypeEdit(clsComplexTypes clsComplexType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();
            clsComplexTypes clsExistingComplexType = clsComplexTypesManager.getComplexTypeByID(clsComplexType.iComplexTypeID);

            clsExistingComplexType.strTitle = clsComplexType.strTitle;
            clsComplexTypesManager.saveComplexType(clsExistingComplexType);

            //Add successful / notification
            TempData["bIsComplexTypeUpdated"] = true;

            return RedirectToAction("ComplexTypesView", "ComplexTypes");
        }

        //Delete ComplexType
        [HttpPost]
        public ActionResult ComplexTypeDelete(int iComplexTypeID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iComplexTypeID == 0)
            {
                return RedirectToAction("ComplexTypesView", "ComplexTypes");
            }

            clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();
            clsComplexTypesManager.removeComplexTypeByID(iComplexTypeID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsComplexTypeDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfComplexTypeExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblComplexTypes.Any(ComplexType => ComplexType.strTitle.ToLower() == strTitle.ToLower() && ComplexType.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}