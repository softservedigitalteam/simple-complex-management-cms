﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomeVehicles;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomeVehiclesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomeVehicles
        public ActionResult HomeVehiclesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeVehiclesView clsHomeVehiclesView = new clsHomeVehiclesView();
            clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
            clsHomeVehiclesView.lstHomeVehicles = clsHomeVehiclesManager.getAllHomeVehiclesOnlyList();

            return View(clsHomeVehiclesView);
        }

        public ActionResult HomeVehicleAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeVehicles clsHomeVehicle = new clsHomeVehicles();

            return View(clsHomeVehicle);
        }

        //Add Home Vehicle
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeVehicleAdd(clsHomeVehicles clsHomeVehicle)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
            clsHomeVehiclesManager.saveHomeVehicle(clsHomeVehicle);

            //Add successful / notification
            TempData["bIsHomeVehicleAdded"] = true;

            return RedirectToAction("HomeVehiclesView", "HomeVehicles");
        }

        //Edit Home Vehicle
        public ActionResult HomeVehicleEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomeVehiclesView", "HomeVehicles");
            }

            clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
            clsHomeVehicles clsHomeVehicle = clsHomeVehiclesManager.getHomeVehicleByID(id);

            return View(clsHomeVehicle);
        }

        //Edit Home Vehicle
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeVehicleEdit(clsHomeVehicles clsHomeVehicle)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
            clsHomeVehicles clsExistingHomeVehicle = clsHomeVehiclesManager.getHomeVehicleByID(clsHomeVehicle.iHomeVehicleID);

            clsExistingHomeVehicle.iHomeDetailID = clsHomeVehicle.iHomeDetailID;
            clsExistingHomeVehicle.iMakeID = clsHomeVehicle.iMakeID;
            clsExistingHomeVehicle.strModel = clsHomeVehicle.strModel;
            clsExistingHomeVehicle.iColourID = clsHomeVehicle.iColourID;
            clsExistingHomeVehicle.strRegistrationNumber = clsHomeVehicle.strRegistrationNumber;

            clsExistingHomeVehicle.strPathToImages = clsHomeVehicle.strPathToImages;
            clsExistingHomeVehicle.strMasterImage = clsHomeVehicle.strMasterImage;

            clsHomeVehiclesManager.saveHomeVehicle(clsExistingHomeVehicle);

            //Add successful / notification
            TempData["bIsHomeVehicleUpdated"] = true;

            return RedirectToAction("HomeVehiclesView", "HomeVehicles");
        }

        //Delete Home Vehicle
        [HttpPost]
        public ActionResult HomeVehicleDelete(int iHomeVehicleID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomeVehicleID == 0)
            {
                return RedirectToAction("HomeVehiclesView", "HomeVehicles");
            }

            clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
            clsHomeVehiclesManager.removeHomeVehicleByID(iHomeVehicleID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomeVehicleDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        //[HttpPost]
        //public JsonResult checkIfHomeVehicleExists(string strTitle)
        //{
        //    bool bCanUseTitle = false;
        //    bool bExists = db.tblHomeVehicles.Any(HomeVehicle => HomeVehicle.strTitle.ToLower() == strTitle.ToLower() && HomeVehicle.bIsDeleted == false);

        //    if (bExists == false)
        //        bCanUseTitle = true;

        //    return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        //}
    }
}