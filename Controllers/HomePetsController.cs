﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomePets;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomePetsController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomePets
        public ActionResult HomePetsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomePetsView clsHomePetsView = new clsHomePetsView();
            clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
            clsHomePetsView.lstHomePets = clsHomePetsManager.getAllHomePetsOnlyList();

            return View(clsHomePetsView);
        }

        public ActionResult HomePetAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomePets clsHomePet = new clsHomePets();

            return View(clsHomePet);
        }

        //Add Home Pet
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomePetAdd(clsHomePets clsHomePet)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
            clsHomePetsManager.saveHomePet(clsHomePet);

            //Add successful / notification
            TempData["bIsHomePetAdded"] = true;

            return RedirectToAction("HomePetsView", "HomePets");
        }

        //Edit Home Pet
        public ActionResult HomePetEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomePetsView", "HomePets");
            }

            clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
            clsHomePets clsHomePet = clsHomePetsManager.getHomePetByID(id);

            return View(clsHomePet);
        }

        //Edit Home Pet
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomePetEdit(clsHomePets clsHomePet)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
            clsHomePets clsExistingHomePet = clsHomePetsManager.getHomePetByID(clsHomePet.iHomePetID);

            clsExistingHomePet.strPetName = clsHomePet.strPetName;
            clsExistingHomePet.iHomeDetailID = clsHomePet.iHomeDetailID;
            clsExistingHomePet.iAnimalTypeID = clsHomePet.iAnimalTypeID;
            clsExistingHomePet.iPetColourID = clsHomePet.iPetColourID;
            clsExistingHomePet.iPetSizeID = clsHomePet.iPetSizeID;

            clsExistingHomePet.strPathToImages = clsHomePet.strPathToImages;
            clsExistingHomePet.strMasterImage = clsHomePet.strMasterImage;

            clsHomePetsManager.saveHomePet(clsExistingHomePet);

            //Add successful / notification
            TempData["bIsHomePetUpdated"] = true;

            return RedirectToAction("HomePetsView", "HomePets");
        }

        //Delete Home Pet
        [HttpPost]
        public ActionResult HomePetDelete(int iHomePetID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomePetID == 0)
            {
                return RedirectToAction("HomePetsView", "HomePets");
            }

            clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
            clsHomePetsManager.removeHomePetByID(iHomePetID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomePetDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        ////Check if exists
        //[HttpPost]
        //public JsonResult checkIfHomePetExists(string strTitle)
        //{
        //    bool bCanUseTitle = false;
        //    bool bExists = db.tblHomePets.Any(HomePet => HomePet.strTitle.ToLower() == strTitle.ToLower() && HomePet.bIsDeleted == false);

        //    if (bExists == false)
        //        bCanUseTitle = true;

        //    return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        //}
    }
}