﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.PetSizes;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class PetSizesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: PetSizes
        public ActionResult PetSizesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetSizesView clsPetSizesView = new clsPetSizesView();
            clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();
            clsPetSizesView.lstPetSizes = clsPetSizesManager.getAllPetSizesOnlyList();

            return View(clsPetSizesView);
        }

        public ActionResult PetSizeAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetSizes clsPetSize = new clsPetSizes();

            return View(clsPetSize);
        }

        //Add Pet Size
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PetSizeAdd(clsPetSizes clsPetSize)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();
            clsPetSizesManager.savePetSize(clsPetSize);

            //Add successful / notification
            TempData["bIsPetSizeAdded"] = true;

            return RedirectToAction("PetSizesView", "PetSizes");
        }

        //Edit Pet Size
        public ActionResult PetSizeEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("PetSizesView", "PetSizes");
            }

            clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();
            clsPetSizes clsPetSize = clsPetSizesManager.getPetSizeByID(id);

            return View(clsPetSize);
        }

        //Edit Pet Size
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PetSizeEdit(clsPetSizes clsPetSize)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();
            clsPetSizes clsExistingPetSize = clsPetSizesManager.getPetSizeByID(clsPetSize.iPetSizeID);

            clsExistingPetSize.strTitle = clsPetSize.strTitle;
            clsPetSizesManager.savePetSize(clsExistingPetSize);

            //Add successful / notification
            TempData["bIsPetSizeUpdated"] = true;

            return RedirectToAction("PetSizesView", "PetSizes");
        }

        //Delete Pet Size
        [HttpPost]
        public ActionResult PetSizeDelete(int iPetSizeID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iPetSizeID == 0)
            {
                return RedirectToAction("PetSizesView", "PetSizes");
            }

            clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();
            clsPetSizesManager.removePetSizeByID(iPetSizeID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsPetSizeDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfPetSizeExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblPetSizes.Any(PetSize => PetSize.strTitle.ToLower() == strTitle.ToLower() && PetSize.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}