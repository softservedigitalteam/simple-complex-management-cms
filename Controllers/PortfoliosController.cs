﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.Portfolios;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class PortfoliosController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: Portfolios
        public ActionResult PortfoliosView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPortfoliosView clsPortfoliosView = new clsPortfoliosView();
            clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
            clsPortfoliosView.lstPortfolios = clsPortfoliosManager.getAllPortfoliosOnlyList();

            return View(clsPortfoliosView);
        }

        public ActionResult PortfolioAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPortfolios clsPortfolio = new clsPortfolios();

            return View(clsPortfolio);
        }

        //Add Portfolio
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PortfolioAdd(clsPortfolios clsPortfolio)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
            clsPortfoliosManager.savePortfolio(clsPortfolio);

            //Add successful / notification
            TempData["bIsPortfolioAdded"] = true;

            return RedirectToAction("PortfoliosView", "Portfolios");
        }

        //Edit Portfolio
        public ActionResult PortfolioEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("PortfoliosView", "Portfolios");
            }

            clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
            clsPortfolios clsPortfolio = clsPortfoliosManager.getPortfolioByID(id);

            return View(clsPortfolio);
        }

        //Edit Portfolio
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PortfolioEdit(clsPortfolios clsPortfolio)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
            clsPortfolios clsExistingPortfolio = clsPortfoliosManager.getPortfolioByID(clsPortfolio.iPortfolioID);

            clsExistingPortfolio.strTitle = clsPortfolio.strTitle;
            clsExistingPortfolio.strEmailAddress1 = clsPortfolio.strEmailAddress1;
            clsExistingPortfolio.strEmailAddress2 = clsPortfolio.strEmailAddress2;
            clsExistingPortfolio.strEmailAddress3 = clsPortfolio.strEmailAddress3;
            clsExistingPortfolio.strEmailAddress4 = clsPortfolio.strEmailAddress4;

            clsExistingPortfolio.strEmailAddress5 = clsPortfolio.strEmailAddress5;
            clsExistingPortfolio.strEmailAddress6 = clsPortfolio.strEmailAddress6;
            clsExistingPortfolio.strEmailAddress7 = clsPortfolio.strEmailAddress7;
            clsExistingPortfolio.strEmailAddress8 = clsPortfolio.strEmailAddress8;

            clsPortfoliosManager.savePortfolio(clsExistingPortfolio);

            //Add successful / notification
            TempData["bIsPortfolioUpdated"] = true;

            return RedirectToAction("PortfoliosView", "Portfolios");
        }

        //Delete Portfolio
        [HttpPost]
        public ActionResult PortfolioDelete(int iPortfolioID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iPortfolioID == 0)
            {
                return RedirectToAction("PortfoliosView", "Portfolios");
            }

            clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
            clsPortfoliosManager.removePortfolioByID(iPortfolioID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsPortfolioDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfPortfolioExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblPortfolios.Any(Portfolio => Portfolio.strTitle.ToLower() == strTitle.ToLower() && Portfolio.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}