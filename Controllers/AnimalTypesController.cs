﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.AnimalTypes;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class AnimalTypesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: AnimalTypes
        public ActionResult AnimalTypesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsAnimalTypesView clsAnimalTypesView = new clsAnimalTypesView();
            clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
            clsAnimalTypesView.lstAnimalTypes = clsAnimalTypesManager.getAllAnimalTypesOnlyList();

            return View(clsAnimalTypesView);
        }

        public ActionResult AnimalTypeAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsAnimalTypes clsAnimalType = new clsAnimalTypes();

            return View(clsAnimalType);
        }

        //Add Animal Type
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AnimalTypeAdd(clsAnimalTypes clsAnimalType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
            clsAnimalTypesManager.saveAnimalType(clsAnimalType);

            //Add successful / notification
            TempData["bIsAnimalTypeAdded"] = true;

            return RedirectToAction("AnimalTypesView", "AnimalTypes");
        }

        //Edit Animal Type
        public ActionResult AnimalTypeEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("AnimalTypesView", "AnimalTypes");
            }

            clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
            clsAnimalTypes clsAnimalType = clsAnimalTypesManager.getAnimalTypeByID(id);

            return View(clsAnimalType);
        }

        //Edit Animal Type
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AnimalTypeEdit(clsAnimalTypes clsAnimalType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
            clsAnimalTypes clsExistingAnimalType = clsAnimalTypesManager.getAnimalTypeByID(clsAnimalType.iAnimalTypeID);

            clsExistingAnimalType.strTitle = clsAnimalType.strTitle;
            clsAnimalTypesManager.saveAnimalType(clsExistingAnimalType);

            //Add successful / notification
            TempData["bIsAnimalTypeUpdated"] = true;

            return RedirectToAction("AnimalTypesView", "AnimalTypes");
        }

        //Delete Animal Type
        [HttpPost]
        public ActionResult AnimalTypeDelete(int iAnimalTypeID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iAnimalTypeID == 0)
            {
                return RedirectToAction("AnimalTypesView", "AnimalTypes");
            }

            clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
            clsAnimalTypesManager.removeAnimalTypeByID(iAnimalTypeID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsAnimalTypeDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfAnimalTypeExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblAnimalTypes.Any(AnimalType => AnimalType.strTitle.ToLower() == strTitle.ToLower() && AnimalType.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}