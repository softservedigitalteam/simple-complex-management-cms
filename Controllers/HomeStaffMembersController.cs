﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomeStaffMembers;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomeStaffMembersController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomeStaffMembers
        public ActionResult HomeStaffMembersView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeStaffMembersView clsHomeStaffMembersView = new clsHomeStaffMembersView();
            clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
            clsHomeStaffMembersView.lstHomeStaffMembers = clsHomeStaffMembersManager.getAllHomeStaffMembersOnlyList();

            return View(clsHomeStaffMembersView);
        }

        public ActionResult HomeStaffMemberAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeStaffMembers clsHomeStaffMember = new clsHomeStaffMembers();

            return View(clsHomeStaffMember);
        }

        //Add Home Staff Member
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeStaffMemberAdd(clsHomeStaffMembers clsHomeStaffMember)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
            clsHomeStaffMembersManager.saveHomeStaffMember(clsHomeStaffMember);

            //Add successful / notification
            TempData["bIsHomeStaffMemberAdded"] = true;

            return RedirectToAction("HomeStaffMembersView", "HomeStaffMembers");
        }

        //Edit Home Staff Member
        public ActionResult HomeStaffMemberEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomeStaffMembersView", "HomeStaffMembers");
            }

            clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
            clsHomeStaffMembers clsHomeStaffMember = clsHomeStaffMembersManager.getHomeStaffMemberByID(id);

            return View(clsHomeStaffMember);
        }

        //Edit Home Staff Member
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeStaffMemberEdit(clsHomeStaffMembers clsHomeStaffMember)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
            clsHomeStaffMembers clsExistingHomeStaffMember = clsHomeStaffMembersManager.getHomeStaffMemberByID(clsHomeStaffMember.iHomeStaffMemberID);

            clsExistingHomeStaffMember.iHomeDetailID = clsHomeStaffMember.iHomeDetailID;
            clsExistingHomeStaffMember.strFirstNames = clsHomeStaffMember.strFirstNames;
            clsExistingHomeStaffMember.strSurname = clsHomeStaffMember.strSurname;
            clsExistingHomeStaffMember.strIDNumber = clsHomeStaffMember.strIDNumber;
            clsExistingHomeStaffMember.strPassportNumber = clsHomeStaffMember.strPassportNumber;

            clsExistingHomeStaffMember.strMobileNumber = clsHomeStaffMember.strMobileNumber;
            clsExistingHomeStaffMember.strPathToImages = clsHomeStaffMember.strPathToImages;
            clsExistingHomeStaffMember.strMasterImage = clsHomeStaffMember.strMasterImage;
            clsExistingHomeStaffMember.bIsLivingOnSite = clsHomeStaffMember.bIsLivingOnSite;

            clsHomeStaffMembersManager.saveHomeStaffMember(clsExistingHomeStaffMember);

            //Add successful / notification
            TempData["bIsHomeStaffMemberUpdated"] = true;

            return RedirectToAction("HomeStaffMembersView", "HomeStaffMembers");
        }

        //Delete Home Staff Member
        [HttpPost]
        public ActionResult HomeStaffMemberDelete(int iHomeStaffMemberID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomeStaffMemberID == 0)
            {
                return RedirectToAction("HomeStaffMembersView", "HomeStaffMembers");
            }

            clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
            clsHomeStaffMembersManager.removeHomeStaffMemberByID(iHomeStaffMemberID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomeStaffMemberDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        //[HttpPost]
        //public JsonResult checkIfHomeStaffMemberExists(string strTitle)
        //{
        //    bool bCanUseTitle = false;
        //    bool bExists = db.tblHomeStaffMembers.Any(HomeStaffMember => HomeStaffMember.strTitle.ToLower() == strTitle.ToLower() && HomeStaffMember.bIsDeleted == false);

        //    if (bExists == false)
        //        bCanUseTitle = true;

        //    return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        //}
    }
}