﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.FormTickets;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class FormTicketsController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: FormTickets
        public ActionResult FormTicketsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTicketsView clsFormTicketsView = new clsFormTicketsView();
            clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
            clsFormTicketsView.lstFormTickets = clsFormTicketsManager.getAllFormTicketsOnlyList();

            return View(clsFormTicketsView);
        }

        public ActionResult FormTicketAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTickets clsFormTicket = new clsFormTickets();

            return View(clsFormTicket);
        }

        //Add FormTicket
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FormTicketAdd(clsFormTickets clsFormTicket)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
            clsFormTicketsManager.saveFormTicket(clsFormTicket);

            //Add successful / notification
            TempData["bIsFormTicketAdded"] = true;

            return RedirectToAction("FormTicketsView", "FormTickets");
        }

        //Edit FormTicket
        public ActionResult FormTicketEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("FormTicketsView", "FormTickets");
            }

            clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
            clsFormTickets clsFormTicket = clsFormTicketsManager.getFormTicketByID(id);

            return View(clsFormTicket);
        }

        //Edit FormTicket
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FormTicketEdit(clsFormTickets clsFormTicket)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
            clsFormTickets clsExistingFormTicket = clsFormTicketsManager.getFormTicketByID(clsFormTicket.iFormTicketID);

            clsExistingFormTicket.strNameAndSurname = clsFormTicket.strNameAndSurname;
            clsExistingFormTicket.strContactNumber = clsFormTicket.strContactNumber;
            clsExistingFormTicket.strAlternateContactNumber = clsFormTicket.strAlternateContactNumber;
            clsExistingFormTicket.strEmailAddress = clsFormTicket.strEmailAddress;
            clsExistingFormTicket.iBlockNumber = clsFormTicket.iBlockNumber;

            clsExistingFormTicket.iUnitNumber = clsFormTicket.iUnitNumber;
            clsExistingFormTicket.strDetailsOfQueryComplaintOrCompliment = clsFormTicket.strDetailsOfQueryComplaintOrCompliment;
            clsExistingFormTicket.iQueryTypeID = clsFormTicket.iQueryTypeID;
            clsExistingFormTicket.iPortfolioID = clsFormTicket.iPortfolioID;
            clsExistingFormTicket.strPathToDocuments = clsFormTicket.strPathToDocuments;

            clsExistingFormTicket.strMasterDocument = clsFormTicket.strMasterDocument;
            clsExistingFormTicket.iHomeDetailID = clsFormTicket.iHomeDetailID;
            clsExistingFormTicket.iFormTicketStatusID = clsFormTicket.iFormTicketStatusID;
            clsExistingFormTicket.bIsResolved = clsFormTicket.bIsResolved;

            clsFormTicketsManager.saveFormTicket(clsExistingFormTicket);

            //Add successful / notification
            TempData["bIsFormTicketUpdated"] = true;

            return RedirectToAction("FormTicketsView", "FormTickets");
        }

        //Delete FormTicket
        [HttpPost]
        public ActionResult FormTicketDelete(int iFormTicketID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iFormTicketID == 0)
            {
                return RedirectToAction("FormTicketsView", "FormTickets");
            }

            clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
            clsFormTicketsManager.removeFormTicketByID(iFormTicketID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsFormTicketDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfFormTicketExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblFormTickets.Any(FormTicket => FormTicket.strNameAndSurname.ToLower() == strTitle.ToLower() && FormTicket.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}