﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.Colours;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class ColoursController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: Colours
        public ActionResult ColoursView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsColoursView clsColoursView = new clsColoursView();
            clsColoursManager clsColoursManager = new clsColoursManager();
            clsColoursView.lstColours = clsColoursManager.getAllColoursOnlyList();

            return View(clsColoursView);
        }

        public ActionResult ColourAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsColours clsColour = new clsColours();

            return View(clsColour);
        }

        //Add Colour
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ColourAdd(clsColours clsColour)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsColoursManager clsColoursManager = new clsColoursManager();
            clsColoursManager.saveColour(clsColour);

            //Add successful / notification
            TempData["bIsColourAdded"] = true;

            return RedirectToAction("ColoursView", "Colours");
        }

        //Edit Colour
        public ActionResult ColourEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("ColoursView", "Colours");
            }

            clsColoursManager clsColoursManager = new clsColoursManager();
            clsColours clsColour = clsColoursManager.getColourByID(id);

            return View(clsColour);
        }

        //Edit Colour
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ColourEdit(clsColours clsColour)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsColoursManager clsColoursManager = new clsColoursManager();
            clsColours clsExistingColour = clsColoursManager.getColourByID(clsColour.iColourID);

            clsExistingColour.strTitle = clsColour.strTitle;
            clsColoursManager.saveColour(clsExistingColour);

            //Add successful / notification
            TempData["bIsColourUpdated"] = true;

            return RedirectToAction("ColoursView", "Colours");
        }

        //Delete Colour
        [HttpPost]
        public ActionResult ColourDelete(int iColourID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iColourID == 0)
            {
                return RedirectToAction("ColoursView", "Colours");
            }

            clsColoursManager clsColoursManager = new clsColoursManager();
            clsColoursManager.removeColourByID(iColourID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsColourDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfColourExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblColours.Any(Colour => Colour.strTitle.ToLower() == strTitle.ToLower() && Colour.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}