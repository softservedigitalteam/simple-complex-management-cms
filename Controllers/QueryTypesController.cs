﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.QueryTypes;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class QueryTypesController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: QueryTypes
        public ActionResult QueryTypesView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsQueryTypesView clsQueryTypesView = new clsQueryTypesView();
            clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();
            clsQueryTypesView.lstQueryTypes = clsQueryTypesManager.getAllQueryTypesOnlyList();

            return View(clsQueryTypesView);
        }

        public ActionResult QueryTypeAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsQueryTypes clsQueryType = new clsQueryTypes();

            return View(clsQueryType);
        }

        //Add Query Type
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult QueryTypeAdd(clsQueryTypes clsQueryType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();
            clsQueryTypesManager.saveQueryType(clsQueryType);

            //Add successful / notification
            TempData["bIsQueryTypeAdded"] = true;

            return RedirectToAction("QueryTypesView", "QueryTypes");
        }

        //Edit Query Type
        public ActionResult QueryTypeEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("QueryTypesView", "QueryTypes");
            }

            clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();
            clsQueryTypes clsQueryType = clsQueryTypesManager.getQueryTypeByID(id);

            return View(clsQueryType);
        }

        //Edit Query Type
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult QueryTypeEdit(clsQueryTypes clsQueryType)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();
            clsQueryTypes clsExistingQueryType = clsQueryTypesManager.getQueryTypeByID(clsQueryType.iQueryTypeID);

            clsExistingQueryType.strQueryTitle = clsQueryType.strQueryTitle;
            clsQueryTypesManager.saveQueryType(clsExistingQueryType);

            //Add successful / notification
            TempData["bIsQueryTypeUpdated"] = true;

            return RedirectToAction("QueryTypesView", "QueryTypes");
        }

        //Delete Query Type
        [HttpPost]
        public ActionResult QueryTypeDelete(int iQueryTypeID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iQueryTypeID == 0)
            {
                return RedirectToAction("QueryTypesView", "QueryTypes");
            }

            clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();
            clsQueryTypesManager.removeQueryTypeByID(iQueryTypeID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsQueryTypeDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfQueryTypeExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblQueryTypes.Any(QueryType => QueryType.strQueryTitle.ToLower() == strTitle.ToLower() && QueryType.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}