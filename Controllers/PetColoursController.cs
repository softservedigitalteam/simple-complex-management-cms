﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.PetColours;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class PetColoursController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: PetColours
        public ActionResult PetColoursView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetColoursView clsPetColoursView = new clsPetColoursView();
            clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
            clsPetColoursView.lstPetColours = clsPetColoursManager.getAllPetColoursOnlyList();

            return View(clsPetColoursView);
        }

        public ActionResult PetColourAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetColours clsPetColour = new clsPetColours();

            return View(clsPetColour);
        }

        //Add Pet Colour
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PetColourAdd(clsPetColours clsPetColour)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
            clsPetColoursManager.savePetColour(clsPetColour);

            //Add successful / notification
            TempData["bIsPetColourAdded"] = true;

            return RedirectToAction("PetColoursView", "PetColours");
        }

        //Edit Pet Colour
        public ActionResult PetColourEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("PetColoursView", "PetColours");
            }

            clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
            clsPetColours clsPetColour = clsPetColoursManager.getPetColourByID(id);

            return View(clsPetColour);
        }

        //Edit Pet Colour
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PetColourEdit(clsPetColours clsPetColour)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
            clsPetColours clsExistingPetColour = clsPetColoursManager.getPetColourByID(clsPetColour.iPetColourID);

            clsExistingPetColour.strTitle = clsPetColour.strTitle;
            clsPetColoursManager.savePetColour(clsExistingPetColour);

            //Add successful / notification
            TempData["bIsPetColourUpdated"] = true;

            return RedirectToAction("PetColoursView", "PetColours");
        }

        //Delete Pet Colour
        [HttpPost]
        public ActionResult PetColourDelete(int iPetColourID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iPetColourID == 0)
            {
                return RedirectToAction("PetColoursView", "PetColours");
            }

            clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
            clsPetColoursManager.removePetColourByID(iPetColourID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsPetColourDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfPetColourExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblPetColours.Any(PetColour => PetColour.strTitle.ToLower() == strTitle.ToLower() && PetColour.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}