﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.LeasingPeriods;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class LeasingPeriodsController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: LeasingPeriods
        public ActionResult LeasingPeriodsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsLeasingPeriodsView clsLeasingPeriodsView = new clsLeasingPeriodsView();
            clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();
            clsLeasingPeriodsView.lstLeasingPeriods = clsLeasingPeriodsManager.getAllLeasingPeriodsOnlyList();

            return View(clsLeasingPeriodsView);
        }

        public ActionResult LeasingPeriodAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsLeasingPeriods clsLeasingPeriod = new clsLeasingPeriods();

            return View(clsLeasingPeriod);
        }

        //Add Leasing Period
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LeasingPeriodAdd(clsLeasingPeriods clsLeasingPeriod)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();
            clsLeasingPeriodsManager.saveLeasingPeriod(clsLeasingPeriod);

            //Add successful / notification
            TempData["bIsLeasingPeriodAdded"] = true;

            return RedirectToAction("LeasingPeriodsView", "LeasingPeriods");
        }

        //Edit Leasing Period
        public ActionResult LeasingPeriodEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("LeasingPeriodsView", "LeasingPeriods");
            }

            clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();
            clsLeasingPeriods clsLeasingPeriod = clsLeasingPeriodsManager.getLeasingPeriodByID(id);

            return View(clsLeasingPeriod);
        }

        //Edit Leasing Period
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LeasingPeriodEdit(clsLeasingPeriods clsLeasingPeriod)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();
            clsLeasingPeriods clsExistingLeasingPeriod = clsLeasingPeriodsManager.getLeasingPeriodByID(clsLeasingPeriod.iLeasingPeriodID);

            clsExistingLeasingPeriod.strTitle = clsLeasingPeriod.strTitle;
            clsLeasingPeriodsManager.saveLeasingPeriod(clsExistingLeasingPeriod);

            //Add successful / notification
            TempData["bIsLeasingPeriodUpdated"] = true;

            return RedirectToAction("LeasingPeriodsView", "LeasingPeriods");
        }

        //Delete Leasing Period
        [HttpPost]
        public ActionResult LeasingPeriodDelete(int iLeasingPeriodID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iLeasingPeriodID == 0)
            {
                return RedirectToAction("LeasingPeriodsView", "LeasingPeriods");
            }

            clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();
            clsLeasingPeriodsManager.removeLeasingPeriodByID(iLeasingPeriodID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsLeasingPeriodDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfLeasingPeriodExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblLeasingPeriods.Any(LeasingPeriod => LeasingPeriod.strTitle.ToLower() == strTitle.ToLower() && LeasingPeriod.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}