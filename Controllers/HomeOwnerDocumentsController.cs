﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomeOwnerDocuments;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomeOwnerDocumentsController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomeOwnerDocuments
        public ActionResult HomeOwnerDocumentsView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwnerDocumentsView clsHomeOwnerDocumentsView = new clsHomeOwnerDocumentsView();
            clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
            clsHomeOwnerDocumentsView.lstHomeOwnerDocuments = clsHomeOwnerDocumentsManager.getAllHomeOwnerDocumentsOnlyList();

            return View(clsHomeOwnerDocumentsView);
        }

        public ActionResult HomeOwnerDocumentAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwnerDocuments clsHomeOwnerDocument = new clsHomeOwnerDocuments();

            return View(clsHomeOwnerDocument);
        }

        //Add Home Owner Document
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeOwnerDocumentAdd(clsHomeOwnerDocuments clsHomeOwnerDocument)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
            clsHomeOwnerDocumentsManager.saveHomeOwnerDocument(clsHomeOwnerDocument);

            //Add successful / notification
            TempData["bIsHomeOwnerDocumentAdded"] = true;

            return RedirectToAction("HomeOwnerDocumentsView", "HomeOwnerDocuments");
        }

        //Edit Home Owner Document
        public ActionResult HomeOwnerDocumentEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomeOwnerDocumentsView", "HomeOwnerDocuments");
            }

            clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
            clsHomeOwnerDocuments clsHomeOwnerDocument = clsHomeOwnerDocumentsManager.getHomeOwnerDocumentByID(id);

            return View(clsHomeOwnerDocument);
        }

        //Edit Home Owner Document
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeOwnerDocumentEdit(clsHomeOwnerDocuments clsHomeOwnerDocument)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
            clsHomeOwnerDocuments clsExistingHomeOwnerDocument = clsHomeOwnerDocumentsManager.getHomeOwnerDocumentByID(clsHomeOwnerDocument.iHomeOwnerDocumentID);

            clsExistingHomeOwnerDocument.strTitle = clsHomeOwnerDocument.strTitle;
            clsExistingHomeOwnerDocument.iHomeOwnerID = clsHomeOwnerDocument.iHomeOwnerID;
            clsExistingHomeOwnerDocument.strDescription = clsHomeOwnerDocument.strDescription;
            clsExistingHomeOwnerDocument.strPathToDocuments = clsHomeOwnerDocument.strPathToDocuments;

            clsHomeOwnerDocumentsManager.saveHomeOwnerDocument(clsExistingHomeOwnerDocument);

            //Add successful / notification
            TempData["bIsHomeOwnerDocumentUpdated"] = true;

            return RedirectToAction("HomeOwnerDocumentsView", "HomeOwnerDocuments");
        }

        //Delete Home Owner Document
        [HttpPost]
        public ActionResult HomeOwnerDocumentDelete(int iHomeOwnerDocumentID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomeOwnerDocumentID == 0)
            {
                return RedirectToAction("HomeOwnerDocumentsView", "HomeOwnerDocuments");
            }

            clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
            clsHomeOwnerDocumentsManager.removeHomeOwnerDocumentByID(iHomeOwnerDocumentID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomeOwnerDocumentDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfHomeOwnerDocumentExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblHomeOwnerDocuments.Any(HomeOwnerDocument => HomeOwnerDocument.strTitle.ToLower() == strTitle.ToLower() && HomeOwnerDocument.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}