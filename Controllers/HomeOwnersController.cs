﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models.HomeOwners;
using SimpleComplexManagementCMS.Model_Manager;

namespace SimpleComplexManagementCMS.Controllers
{
    public class HomeOwnersController : Controller
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        // GET: HomeOwners
        public ActionResult HomeOwnersView()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwnersView clsHomeOwnersView = new clsHomeOwnersView();
            clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
            clsHomeOwnersView.lstHomeOwners = clsHomeOwnersManager.getAllHomeOwnersOnlyList();

            return View(clsHomeOwnersView);
        }

        public ActionResult HomeOwnerAdd()
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwners clsHomeOwner = new clsHomeOwners();

            return View(clsHomeOwner);
        }

        //Add Home Owner
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeOwnerAdd(clsHomeOwners clsHomeOwner)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
            clsHomeOwnersManager.saveHomeOwner(clsHomeOwner);

            //Add successful / notification
            TempData["bIsHomeOwnerAdded"] = true;

            return RedirectToAction("HomeOwnersView", "HomeOwners");
        }

        //Edit Home Owner
        public ActionResult HomeOwnerEdit(int id)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            if (id == 0)
            {
                return RedirectToAction("HomeOwnersView", "HomeOwners");
            }

            clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
            clsHomeOwners clsHomeOwner = clsHomeOwnersManager.getHomeOwnerByID(id);

            return View(clsHomeOwner);
        }

        //Edit Home Owner
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult HomeOwnerEdit(clsHomeOwners clsHomeOwner)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
            clsHomeOwners clsExistingHomeOwner = clsHomeOwnersManager.getHomeOwnerByID(clsHomeOwner.iHomeOwnerID);

            clsExistingHomeOwner.strLatitude = clsHomeOwner.strLatitude;
            clsExistingHomeOwner.strLongitude = clsHomeOwner.strLongitude;
            clsExistingHomeOwner.strRole = clsHomeOwner.strRole;
            clsExistingHomeOwner.strFirstName = clsHomeOwner.strFirstName;
            clsExistingHomeOwner.strSurname = clsHomeOwner.strSurname;

            clsExistingHomeOwner.strIDNumber = clsHomeOwner.strIDNumber;
            clsExistingHomeOwner.strPassportNumber = clsHomeOwner.strPassportNumber;
            clsExistingHomeOwner.strPathToImages = clsHomeOwner.strPathToImages;
            clsExistingHomeOwner.strMasterImage = clsHomeOwner.strMasterImage;
            clsExistingHomeOwner.strMobileNumber = clsHomeOwner.strMobileNumber;

            clsExistingHomeOwner.strPassword = clsHomeOwner.strPassword;
            clsExistingHomeOwner.iValidationNumber = clsHomeOwner.iValidationNumber;
            clsExistingHomeOwner.strEmailAddress = clsHomeOwner.strEmailAddress;
            clsExistingHomeOwner.bIsActive = clsHomeOwner.bIsActive;

            clsHomeOwnersManager.saveHomeOwner(clsExistingHomeOwner);

            //Add successful / notification
            TempData["bIsHomeOwnerUpdated"] = true;

            return RedirectToAction("HomeOwnersView", "HomeOwners");
        }

        //Delete Home Owner
        [HttpPost]
        public ActionResult HomeOwnerDelete(int iHomeOwnerID)
        {
            //Redirect to login if null session exists
            if (Session["clsCMSUser"] == null)
                return RedirectToAction("Login", "Account");

            bool bIsSuccess = false;

            if (iHomeOwnerID == 0)
            {
                return RedirectToAction("HomeOwnersView", "HomeOwners");
            }

            clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
            clsHomeOwnersManager.removeHomeOwnerByID(iHomeOwnerID);

            bIsSuccess = true;

            //Delete successful / notification
            TempData["bIsHomeOwnerDeleted"] = true;

            return Json(new { bIsSuccess = bIsSuccess }, JsonRequestBehavior.AllowGet);
        }

        //Check if exists
        [HttpPost]
        public JsonResult checkIfHomeOwnerExists(string strTitle)
        {
            bool bCanUseTitle = false;
            bool bExists = db.tblHomeOwners.Any(HomeOwner => HomeOwner.strEmailAddress.ToLower() == strTitle.ToLower() && HomeOwner.bIsDeleted == false);

            if (bExists == false)
                bCanUseTitle = true;

            return Json(bCanUseTitle, JsonRequestBehavior.AllowGet);
        }
    }
}