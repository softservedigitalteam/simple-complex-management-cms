﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SimpleComplexManagementCMS.Startup))]
namespace SimpleComplexManagementCMS
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
