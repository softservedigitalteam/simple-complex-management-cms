﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.RoleTypes
{
    public class clsRoleTypesView
    {
        public List<clsRoleTypes> lstRoleTypes { get; set; }
        public int iRoleTypeID { get; set; }
    }
}
