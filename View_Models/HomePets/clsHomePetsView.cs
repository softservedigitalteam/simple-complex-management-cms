﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomePets
{
    public class clsHomePetsView
    {
        public List<clsHomePets> lstHomePets { get; set; }
        public int iHomePetID { get; set; }
    }
}
