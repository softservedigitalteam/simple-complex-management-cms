﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.Makes
{
    public class clsMakesView
    {
        public List<clsMakes> lstMakes { get; set; }
        public int iMakeID { get; set; }
    }
}
