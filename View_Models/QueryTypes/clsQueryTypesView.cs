﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.QueryTypes
{
    public class clsQueryTypesView
    {
        public List<clsQueryTypes> lstQueryTypes { get; set; }
        public int iQueryTypeID { get; set; }
    }
}
