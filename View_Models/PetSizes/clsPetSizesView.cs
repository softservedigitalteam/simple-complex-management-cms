﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.PetSizes
{
    public class clsPetSizesView
    {
        public List<clsPetSizes> lstPetSizes { get; set; }
        public int iPetSizeID { get; set; }
    }
}
