﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomeOwners
{
    public class clsHomeOwnersView
    {
        public List<clsHomeOwners> lstHomeOwners { get; set; }
        public int iHomeOwnerID { get; set; }
    }
}
