﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.Portfolios
{
    public class clsPortfoliosView
    {
        public List<clsPortfolios> lstPortfolios { get; set; }
        public int iPortfolioID { get; set; }
    }
}
