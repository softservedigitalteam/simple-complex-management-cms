﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomeVehicles
{
    public class clsHomeVehiclesView
    {
        public List<clsHomeVehicles> lstHomeVehicles { get; set; }
        public int iHomeVehicleID { get; set; }
    }
}
