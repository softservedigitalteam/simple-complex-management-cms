﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomeOwnerDocuments
{
    public class clsHomeOwnerDocumentsView
    {
        public List<clsHomeOwnerDocuments> lstHomeOwnerDocuments { get; set; }
        public int iHomeOwnerDocumentID { get; set; }
    }
}
