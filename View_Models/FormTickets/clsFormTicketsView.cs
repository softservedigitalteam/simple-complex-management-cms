﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.FormTickets
{
    public class clsFormTicketsView
    {
        public List<clsFormTickets> lstFormTickets { get; set; }
        public int iFormTicketID { get; set; }
    }
}
