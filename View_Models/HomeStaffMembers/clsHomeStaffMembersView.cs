﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomeStaffMembers
{
    public class clsHomeStaffMembersView
    {
        public List<clsHomeStaffMembers> lstHomeStaffMembers { get; set; }
        public int iHomeStaffMemberID { get; set; }
    }
}
