﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Model_Manager;
using SimpleComplexManagementCMS.Models;
using SimpleComplexManagementCMS.View_Models;
using SimpleComplexManagementCMS.View_Models.Users;

namespace SimpleComplexManagementCMS.View_Models.CMSUsers
{
    public class clsCMSUsersView
    {
        public List<clsCMSUsers> lstCMSUsers { get; set; }
        public int iCMSUserID { get; set; }
    }
}
