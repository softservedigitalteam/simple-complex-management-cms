﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.AnimalTypes
{
    public class clsAnimalTypesView
    {
        public List<clsAnimalTypes> lstAnimalTypes { get; set; }
        public int iAnimalTypeID { get; set; }
    }
}
