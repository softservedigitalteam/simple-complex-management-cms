﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.Colours
{
    public class clsColoursView
    {
        public List<clsColours> lstColours { get; set; }
        public int iColourID { get; set; }
    }
}
