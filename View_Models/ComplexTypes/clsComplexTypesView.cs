﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.ComplexTypes
{
    public class clsComplexTypesView
    {
        public List<clsComplexTypes> lstComplexTypes { get; set; }
        public int iComplexTypeID { get; set; }
    }
}
