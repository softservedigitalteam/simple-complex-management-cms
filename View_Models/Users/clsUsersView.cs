﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using SimpleComplexManagementCMS.Model_Manager;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.Users
{
    public class clsUsersView
    {
        public List<clsUsers> lstUsers { get; set; }
        public int iUserID { get; set; }
    }
}
