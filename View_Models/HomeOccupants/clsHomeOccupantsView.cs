﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomeOccupants
{
    public class clsHomeOccupantsView
    {
        public List<clsHomeOccupants> lstHomeOccupants { get; set; }
        public int iHomeOccupantID { get; set; }
    }
}
