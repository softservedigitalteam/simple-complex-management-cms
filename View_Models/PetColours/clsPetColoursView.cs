﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.PetColours
{
    public class clsPetColoursView
    {
        public List<clsPetColours> lstPetColours { get; set; }
        public int iPetColourID { get; set; }
    }
}
