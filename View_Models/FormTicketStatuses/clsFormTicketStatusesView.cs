﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.FormTicketStatuses
{
    public class clsFormTicketStatusesView
    {
        public List<clsFormTicketStatuses> lstFormTicketStatuses { get; set; }
        public int iFormTicketStatusID { get; set; }
    }
}
