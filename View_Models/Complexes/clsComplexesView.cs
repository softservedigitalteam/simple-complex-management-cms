﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.Complexes
{
    public class clsComplexesView
    {
        public List<clsComplexes> lstComplexes { get; set; }
        public int iComplexID { get; set; }
    }
}
