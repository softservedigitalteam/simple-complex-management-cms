﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomeDetails
{
    public class clsHomeDetailsView
    {
        public List<clsHomeDetails> lstHomeDetails { get; set; }
        public int iHomeDetailID { get; set; }
    }
}
