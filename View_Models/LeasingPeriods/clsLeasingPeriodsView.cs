﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.LeasingPeriods
{
    public class clsLeasingPeriodsView
    {
        public List<clsLeasingPeriods> lstLeasingPeriods { get; set; }
        public int iLeasingPeriodID { get; set; }
    }
}
