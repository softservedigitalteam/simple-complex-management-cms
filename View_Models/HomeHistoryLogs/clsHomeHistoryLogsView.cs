﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.View_Models.HomeHistoryLogs
{
    public class clsHomeHistoryLogsView
    {
        public List<clsHomeHistoryLogs> lstHomeHistoryLogs { get; set; }
        public int iHomeHistoryLogID { get; set; }
    }
}
