﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomeOwnersManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomeOwners> getAllHomeOwnersList()
        {
            List<clsHomeOwners> lstHomeOwners = new List<clsHomeOwners>();
            var lstGetHomeOwnersList = db.tblHomeOwners.Where(HomeOwner => HomeOwner.bIsDeleted == false).ToList();

            if (lstGetHomeOwnersList.Count > 0)
            {
                //Managers
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
                clsUsersManager clsUsersManager = new clsUsersManager();

                foreach (var item in lstGetHomeOwnersList)
                {
                    clsHomeOwners clsHomeOwner = new clsHomeOwners();

                    clsHomeOwner.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeOwner.dtAdded = item.dtAdded;
                    clsHomeOwner.iAddedBy = item.iAddedBy;
                    clsHomeOwner.dtEdited = item.dtEdited;
                    clsHomeOwner.iEditedBy = item.iEditedBy;

                    clsHomeOwner.strLatitude = item.strLatitude;
                    clsHomeOwner.strLongitude = item.strLongitude;
                    clsHomeOwner.strRole = item.strRole;
                    clsHomeOwner.strFirstName = item.strFirstName;
                    clsHomeOwner.strSurname = item.strSurname;

                    clsHomeOwner.strIDNumber = item.strIDNumber;
                    clsHomeOwner.strPassportNumber = item.strPassportNumber;
                    clsHomeOwner.strPathToImages = item.strPathToImages;
                    clsHomeOwner.strMasterImage = item.strMasterImage;
                    clsHomeOwner.strMobileNumber = item.strMobileNumber;

                    clsHomeOwner.strPassword = item.strPassword;
                    clsHomeOwner.iValidationNumber = item.iValidationNumber;
                    clsHomeOwner.strEmailAddress = item.strEmailAddress;
                    clsHomeOwner.bIsActive = item.bIsActive;
                    clsHomeOwner.bIsDeleted = item.bIsDeleted;

                    clsHomeOwner.lstHomeDetails = new List<clsHomeDetails>();
                    clsHomeOwner.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
                    clsHomeOwner.lstHomeOwnerDocuments = new List<clsHomeOwnerDocuments>();
                    clsHomeOwner.lstUsers = new List<clsUsers>();

                    if (item.tblHomeDetails.Count > 0)
                    {
                        foreach (var HomeDetailsItem in item.tblHomeDetails)
                        {
                            clsHomeDetails clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(HomeDetailsItem);
                            clsHomeOwner.lstHomeDetails.Add(clsHomeDetail);
                        }
                    }
                    if (item.tblHomeHistoryLogs.Count > 0)
                    {
                        foreach (var HomeHistoryLogsItem in item.tblHomeHistoryLogs)
                        {
                            clsHomeHistoryLogs clsHomeHistoryLog = clsHomeHistoryLogsManager.convertHomeHistoryLogsTableToClass(HomeHistoryLogsItem);
                            clsHomeOwner.lstHomeHistoryLogs.Add(clsHomeHistoryLog);
                        }
                    }
                    if (item.tblHomeOwnerDocuments.Count > 0)
                    {
                        foreach (var HomeOwnerDocumentsItem in item.tblHomeOwnerDocuments)
                        {
                            clsHomeOwnerDocuments clsHomeOwnerDocument = clsHomeOwnerDocumentsManager.convertHomeOwnerDocumentsTableToClass(HomeOwnerDocumentsItem);
                            clsHomeOwner.lstHomeOwnerDocuments.Add(clsHomeOwnerDocument);
                        }
                    }
                    if (item.tblUsers.Count > 0)
                    {
                        foreach (var UsersItem in item.tblUsers)
                        {
                            clsUsers clsUser = clsUsersManager.convertUsersTableToClass(UsersItem);
                            clsHomeOwner.lstUsers.Add(clsUser);
                        }
                    }


                    lstHomeOwners.Add(clsHomeOwner);
                }
            }

            return lstHomeOwners;
        }

        //Get All Only
        public List<clsHomeOwners> getAllHomeOwnersOnlyList()
        {
            List<clsHomeOwners> lstHomeOwners = new List<clsHomeOwners>();
            var lstGetHomeOwnersList = db.tblHomeOwners.Where(HomeOwner => HomeOwner.bIsDeleted == false).ToList();

            if (lstGetHomeOwnersList.Count > 0)
            {
                //Managers
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
                clsUsersManager clsUsersManager = new clsUsersManager();

                foreach (var item in lstGetHomeOwnersList)
                {
                    clsHomeOwners clsHomeOwner = new clsHomeOwners();

                    clsHomeOwner.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeOwner.dtAdded = item.dtAdded;
                    clsHomeOwner.iAddedBy = item.iAddedBy;
                    clsHomeOwner.dtEdited = item.dtEdited;
                    clsHomeOwner.iEditedBy = item.iEditedBy;

                    clsHomeOwner.strLatitude = item.strLatitude;
                    clsHomeOwner.strLongitude = item.strLongitude;
                    clsHomeOwner.strRole = item.strRole;
                    clsHomeOwner.strFirstName = item.strFirstName;
                    clsHomeOwner.strSurname = item.strSurname;

                    clsHomeOwner.strIDNumber = item.strIDNumber;
                    clsHomeOwner.strPassportNumber = item.strPassportNumber;
                    clsHomeOwner.strPathToImages = item.strPathToImages;
                    clsHomeOwner.strMasterImage = item.strMasterImage;
                    clsHomeOwner.strMobileNumber = item.strMobileNumber;

                    clsHomeOwner.strPassword = item.strPassword;
                    clsHomeOwner.iValidationNumber = item.iValidationNumber;
                    clsHomeOwner.strEmailAddress = item.strEmailAddress;
                    clsHomeOwner.bIsActive = item.bIsActive;
                    clsHomeOwner.bIsDeleted = item.bIsDeleted;

                    clsHomeOwner.lstHomeDetails = new List<clsHomeDetails>();
                    clsHomeOwner.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
                    clsHomeOwner.lstHomeOwnerDocuments = new List<clsHomeOwnerDocuments>();
                    clsHomeOwner.lstUsers = new List<clsUsers>();

                    lstHomeOwners.Add(clsHomeOwner);
                }
            }

            return lstHomeOwners;
        }

        //Get
        public clsHomeOwners getHomeOwnerByID(int iHomeOwnerID)
        {
            clsHomeOwners clsHomeOwner = null;
            tblHomeOwners tblHomeOwners = db.tblHomeOwners.FirstOrDefault(HomeOwner => HomeOwner.iHomeOwnerID == iHomeOwnerID && HomeOwner.bIsDeleted == false);

            if (tblHomeOwners != null)
            {
                //Managers
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeOwnerDocumentsManager clsHomeOwnerDocumentsManager = new clsHomeOwnerDocumentsManager();
                clsUsersManager clsUsersManager = new clsUsersManager();

                clsHomeOwner = new clsHomeOwners();

                clsHomeOwner.iHomeOwnerID = tblHomeOwners.iHomeOwnerID;
                clsHomeOwner.dtAdded = tblHomeOwners.dtAdded;
                clsHomeOwner.iAddedBy = tblHomeOwners.iAddedBy;
                clsHomeOwner.dtEdited = tblHomeOwners.dtEdited;
                clsHomeOwner.iEditedBy = tblHomeOwners.iEditedBy;

                clsHomeOwner.strLatitude = tblHomeOwners.strLatitude;
                clsHomeOwner.strLongitude = tblHomeOwners.strLongitude;
                clsHomeOwner.strRole = tblHomeOwners.strRole;
                clsHomeOwner.strFirstName = tblHomeOwners.strFirstName;
                clsHomeOwner.strSurname = tblHomeOwners.strSurname;

                clsHomeOwner.strIDNumber = tblHomeOwners.strIDNumber;
                clsHomeOwner.strPassportNumber = tblHomeOwners.strPassportNumber;
                clsHomeOwner.strPathToImages = tblHomeOwners.strPathToImages;
                clsHomeOwner.strMasterImage = tblHomeOwners.strMasterImage;
                clsHomeOwner.strMobileNumber = tblHomeOwners.strMobileNumber;

                clsHomeOwner.strPassword = tblHomeOwners.strPassword;
                clsHomeOwner.iValidationNumber = tblHomeOwners.iValidationNumber;
                clsHomeOwner.strEmailAddress = tblHomeOwners.strEmailAddress;
                clsHomeOwner.bIsActive = tblHomeOwners.bIsActive;
                clsHomeOwner.bIsDeleted = tblHomeOwners.bIsDeleted;

                clsHomeOwner.lstHomeDetails = new List<clsHomeDetails>();
                clsHomeOwner.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
                clsHomeOwner.lstHomeOwnerDocuments = new List<clsHomeOwnerDocuments>();
                clsHomeOwner.lstUsers = new List<clsUsers>();

                if (tblHomeOwners.tblHomeDetails.Count > 0)
                {
                    foreach (var HomeDetailstblHomeOwners in tblHomeOwners.tblHomeDetails)
                    {
                        clsHomeDetails clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(HomeDetailstblHomeOwners);
                        clsHomeOwner.lstHomeDetails.Add(clsHomeDetail);
                    }
                }
                if (tblHomeOwners.tblHomeHistoryLogs.Count > 0)
                {
                    foreach (var HomeHistoryLogstblHomeOwners in tblHomeOwners.tblHomeHistoryLogs)
                    {
                        clsHomeHistoryLogs clsHomeHistoryLog = clsHomeHistoryLogsManager.convertHomeHistoryLogsTableToClass(HomeHistoryLogstblHomeOwners);
                        clsHomeOwner.lstHomeHistoryLogs.Add(clsHomeHistoryLog);
                    }
                }
                if (tblHomeOwners.tblHomeOwnerDocuments.Count > 0)
                {
                    foreach (var HomeOwnerDocumentstblHomeOwners in tblHomeOwners.tblHomeOwnerDocuments)
                    {
                        clsHomeOwnerDocuments clsHomeOwnerDocument = clsHomeOwnerDocumentsManager.convertHomeOwnerDocumentsTableToClass(HomeOwnerDocumentstblHomeOwners);
                        clsHomeOwner.lstHomeOwnerDocuments.Add(clsHomeOwnerDocument);
                    }
                }
                if (tblHomeOwners.tblUsers.Count > 0)
                {
                    foreach (var UserstblHomeOwners in tblHomeOwners.tblUsers)
                    {
                        clsUsers clsUser = clsUsersManager.convertUsersTableToClass(UserstblHomeOwners);
                        clsHomeOwner.lstUsers.Add(clsUser);
                    }
                }
            }

            return clsHomeOwner;
        }

        //Save
        public void saveHomeOwner(clsHomeOwners clsHomeOwner)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomeOwners tblHomeOwners = new tblHomeOwners();

                tblHomeOwners.iHomeOwnerID = clsHomeOwner.iHomeOwnerID;

                tblHomeOwners.strLatitude = clsHomeOwner.strLatitude;
                tblHomeOwners.strLongitude = clsHomeOwner.strLongitude;
                tblHomeOwners.strRole = clsHomeOwner.strRole;
                tblHomeOwners.strFirstName = clsHomeOwner.strFirstName;
                tblHomeOwners.strSurname = clsHomeOwner.strSurname;

                tblHomeOwners.strIDNumber = clsHomeOwner.strIDNumber;
                tblHomeOwners.strPassportNumber = clsHomeOwner.strPassportNumber;
                tblHomeOwners.strPathToImages = clsHomeOwner.strPathToImages;
                tblHomeOwners.strMasterImage = clsHomeOwner.strMasterImage;
                tblHomeOwners.strMobileNumber = clsHomeOwner.strMobileNumber;

                tblHomeOwners.strPassword = clsHomeOwner.strPassword;
                tblHomeOwners.iValidationNumber = clsHomeOwner.iValidationNumber;
                tblHomeOwners.strEmailAddress = clsHomeOwner.strEmailAddress;
                tblHomeOwners.bIsActive = clsHomeOwner.bIsActive;
                tblHomeOwners.bIsDeleted = clsHomeOwner.bIsDeleted;

                //Add
                if (tblHomeOwners.iHomeOwnerID == 0)
                {
                    tblHomeOwners.dtAdded = DateTime.Now;
                    tblHomeOwners.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomeOwners.dtEdited = DateTime.Now;
                    tblHomeOwners.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomeOwners.Add(tblHomeOwners);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomeOwners.dtAdded = clsHomeOwner.dtAdded;
                    tblHomeOwners.iAddedBy = clsHomeOwner.iAddedBy;
                    tblHomeOwners.dtEdited = DateTime.Now;
                    tblHomeOwners.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomeOwners>().AddOrUpdate(tblHomeOwners);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomeOwnerByID(int iHomeOwnerID)
        {
            tblHomeOwners tblHomeOwner = db.tblHomeOwners.Find(iHomeOwnerID);
            if (tblHomeOwner != null)
            {
                tblHomeOwner.bIsDeleted = true;
                db.Entry(tblHomeOwner).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomeOwnerExists(int iHomeOwnerID)
        {
            bool bHomeOwnerExists = db.tblHomeOwners.Any(HomeOwner => HomeOwner.iHomeOwnerID == iHomeOwnerID && HomeOwner.bIsDeleted == false);
            return bHomeOwnerExists;
        }

        //Convert database table to class
        public clsHomeOwners convertHomeOwnersTableToClass(tblHomeOwners tblHomeOwner)
        {
            clsHomeOwners clsHomeOwner = new clsHomeOwners();

            clsHomeOwner.iHomeOwnerID = tblHomeOwner.iHomeOwnerID;
            clsHomeOwner.dtAdded = tblHomeOwner.dtAdded;
            clsHomeOwner.iAddedBy = tblHomeOwner.iAddedBy;
            clsHomeOwner.dtEdited = tblHomeOwner.dtEdited;
            clsHomeOwner.iEditedBy = tblHomeOwner.iEditedBy;

            clsHomeOwner.strLatitude = tblHomeOwner.strLatitude;
            clsHomeOwner.strLongitude = tblHomeOwner.strLongitude;
            clsHomeOwner.strRole = tblHomeOwner.strRole;
            clsHomeOwner.strFirstName = tblHomeOwner.strFirstName;
            clsHomeOwner.strSurname = tblHomeOwner.strSurname;

            clsHomeOwner.strIDNumber = tblHomeOwner.strIDNumber;
            clsHomeOwner.strPassportNumber = tblHomeOwner.strPassportNumber;
            clsHomeOwner.strPathToImages = tblHomeOwner.strPathToImages;
            clsHomeOwner.strMasterImage = tblHomeOwner.strMasterImage;
            clsHomeOwner.strMobileNumber = tblHomeOwner.strMobileNumber;

            clsHomeOwner.strPassword = tblHomeOwner.strPassword;
            clsHomeOwner.iValidationNumber = tblHomeOwner.iValidationNumber;
            clsHomeOwner.strEmailAddress = tblHomeOwner.strEmailAddress;
            clsHomeOwner.bIsActive = tblHomeOwner.bIsActive;
            clsHomeOwner.bIsDeleted = tblHomeOwner.bIsDeleted;

            return clsHomeOwner;
        }
    }
}
