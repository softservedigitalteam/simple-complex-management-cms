﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomeStaffMembersManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomeStaffMembers> getAllHomeStaffMembersList()
        {
            List<clsHomeStaffMembers> lstHomeStaffMembers = new List<clsHomeStaffMembers>();
            var lstGetHomeStaffMembersList = db.tblHomeStaffMembers.Where(HomeStaffMember => HomeStaffMember.bIsDeleted == false).ToList();

            if (lstGetHomeStaffMembersList.Count > 0)
            {
                //Home Details Manager
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetHomeStaffMembersList)
                {
                    clsHomeStaffMembers clsHomeStaffMember = new clsHomeStaffMembers();

                    clsHomeStaffMember.iHomeStaffMemberID = item.iHomeStaffMemberID;
                    clsHomeStaffMember.dtAdded = item.dtAdded;
                    clsHomeStaffMember.iAddedBy = item.iAddedBy;
                    clsHomeStaffMember.dtEdited = item.dtEdited;
                    clsHomeStaffMember.iEditedBy = item.iEditedBy;

                    clsHomeStaffMember.iHomeDetailID = item.iHomeDetailID;
                    clsHomeStaffMember.strFirstNames = item.strFirstNames;
                    clsHomeStaffMember.strSurname = item.strSurname;
                    clsHomeStaffMember.strIDNumber = item.strIDNumber;
                    clsHomeStaffMember.strPassportNumber = item.strPassportNumber;

                    clsHomeStaffMember.strMobileNumber = item.strMobileNumber;
                    clsHomeStaffMember.strPathToImages = item.strPathToImages;
                    clsHomeStaffMember.strMasterImage = item.strMasterImage;
                    clsHomeStaffMember.bIsLivingOnSite = item.bIsLivingOnSite;
                    clsHomeStaffMember.bIsDeleted = item.bIsDeleted;

                    if (item.tblHomeDetails != null)
                        clsHomeStaffMember.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(item.tblHomeDetails);

                    lstHomeStaffMembers.Add(clsHomeStaffMember);
                }
            }

            return lstHomeStaffMembers;
        }

        //Get All Only
        public List<clsHomeStaffMembers> getAllHomeStaffMembersOnlyList()
        {
            List<clsHomeStaffMembers> lstHomeStaffMembers = new List<clsHomeStaffMembers>();
            var lstGetHomeStaffMembersList = db.tblHomeStaffMembers.Where(HomeStaffMember => HomeStaffMember.bIsDeleted == false).ToList();

            if (lstGetHomeStaffMembersList.Count > 0)
            {
                //Home Details Manager
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetHomeStaffMembersList)
                {
                    clsHomeStaffMembers clsHomeStaffMember = new clsHomeStaffMembers();

                    clsHomeStaffMember.iHomeStaffMemberID = item.iHomeStaffMemberID;
                    clsHomeStaffMember.dtAdded = item.dtAdded;
                    clsHomeStaffMember.iAddedBy = item.iAddedBy;
                    clsHomeStaffMember.dtEdited = item.dtEdited;
                    clsHomeStaffMember.iEditedBy = item.iEditedBy;

                    clsHomeStaffMember.iHomeDetailID = item.iHomeDetailID;
                    clsHomeStaffMember.strFirstNames = item.strFirstNames;
                    clsHomeStaffMember.strSurname = item.strSurname;
                    clsHomeStaffMember.strIDNumber = item.strIDNumber;
                    clsHomeStaffMember.strPassportNumber = item.strPassportNumber;

                    clsHomeStaffMember.strMobileNumber = item.strMobileNumber;
                    clsHomeStaffMember.strPathToImages = item.strPathToImages;
                    clsHomeStaffMember.strMasterImage = item.strMasterImage;
                    clsHomeStaffMember.bIsLivingOnSite = item.bIsLivingOnSite;
                    clsHomeStaffMember.bIsDeleted = item.bIsDeleted;

                    lstHomeStaffMembers.Add(clsHomeStaffMember);
                }
            }

            return lstHomeStaffMembers;
        }

        //Get
        public clsHomeStaffMembers getHomeStaffMemberByID(int iHomeStaffMemberID)
        {
            clsHomeStaffMembers clsHomeStaffMember = null;
            tblHomeStaffMembers tblHomeStaffMembers = db.tblHomeStaffMembers.FirstOrDefault(HomeStaffMember => HomeStaffMember.iHomeStaffMemberID == iHomeStaffMemberID && HomeStaffMember.bIsDeleted == false);

            if (tblHomeStaffMembers != null)
            {
                //Home Details Manager
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                clsHomeStaffMember = new clsHomeStaffMembers();

                clsHomeStaffMember.iHomeStaffMemberID = tblHomeStaffMembers.iHomeStaffMemberID;
                clsHomeStaffMember.dtAdded = tblHomeStaffMembers.dtAdded;
                clsHomeStaffMember.iAddedBy = tblHomeStaffMembers.iAddedBy;
                clsHomeStaffMember.dtEdited = tblHomeStaffMembers.dtEdited;
                clsHomeStaffMember.iEditedBy = tblHomeStaffMembers.iEditedBy;

                clsHomeStaffMember.iHomeDetailID = tblHomeStaffMembers.iHomeDetailID;
                clsHomeStaffMember.strFirstNames = tblHomeStaffMembers.strFirstNames;
                clsHomeStaffMember.strSurname = tblHomeStaffMembers.strSurname;
                clsHomeStaffMember.strIDNumber = tblHomeStaffMembers.strIDNumber;
                clsHomeStaffMember.strPassportNumber = tblHomeStaffMembers.strPassportNumber;

                clsHomeStaffMember.strMobileNumber = tblHomeStaffMembers.strMobileNumber;
                clsHomeStaffMember.strPathToImages = tblHomeStaffMembers.strPathToImages;
                clsHomeStaffMember.strMasterImage = tblHomeStaffMembers.strMasterImage;
                clsHomeStaffMember.bIsLivingOnSite = tblHomeStaffMembers.bIsLivingOnSite;
                clsHomeStaffMember.bIsDeleted = tblHomeStaffMembers.bIsDeleted;

                if (tblHomeStaffMembers.tblHomeDetails != null)
                    clsHomeStaffMember.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(tblHomeStaffMembers.tblHomeDetails);
            }

            return clsHomeStaffMember;
        }

        //Save
        public void saveHomeStaffMember(clsHomeStaffMembers clsHomeStaffMember)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomeStaffMembers tblHomeStaffMembers = new tblHomeStaffMembers();

                tblHomeStaffMembers.iHomeStaffMemberID = clsHomeStaffMember.iHomeStaffMemberID;

                tblHomeStaffMembers.iHomeDetailID = clsHomeStaffMember.iHomeDetailID;
                tblHomeStaffMembers.strFirstNames = clsHomeStaffMember.strFirstNames;
                tblHomeStaffMembers.strSurname = clsHomeStaffMember.strSurname;
                tblHomeStaffMembers.strIDNumber = clsHomeStaffMember.strIDNumber;
                tblHomeStaffMembers.strPassportNumber = clsHomeStaffMember.strPassportNumber;

                tblHomeStaffMembers.strMobileNumber = clsHomeStaffMember.strMobileNumber;
                tblHomeStaffMembers.strPathToImages = clsHomeStaffMember.strPathToImages;
                tblHomeStaffMembers.strMasterImage = clsHomeStaffMember.strMasterImage;
                tblHomeStaffMembers.bIsLivingOnSite = clsHomeStaffMember.bIsLivingOnSite;
                tblHomeStaffMembers.bIsDeleted = clsHomeStaffMember.bIsDeleted;

                //Add
                if (tblHomeStaffMembers.iHomeStaffMemberID == 0)
                {
                    tblHomeStaffMembers.dtAdded = DateTime.Now;
                    tblHomeStaffMembers.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomeStaffMembers.dtEdited = DateTime.Now;
                    tblHomeStaffMembers.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomeStaffMembers.Add(tblHomeStaffMembers);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomeStaffMembers.dtAdded = clsHomeStaffMember.dtAdded;
                    tblHomeStaffMembers.iAddedBy = clsHomeStaffMember.iAddedBy;
                    tblHomeStaffMembers.dtEdited = DateTime.Now;
                    tblHomeStaffMembers.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomeStaffMembers>().AddOrUpdate(tblHomeStaffMembers);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomeStaffMemberByID(int iHomeStaffMemberID)
        {
            tblHomeStaffMembers tblHomeStaffMember = db.tblHomeStaffMembers.Find(iHomeStaffMemberID);
            if (tblHomeStaffMember != null)
            {
                tblHomeStaffMember.bIsDeleted = true;
                db.Entry(tblHomeStaffMember).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomeStaffMemberExists(int iHomeStaffMemberID)
        {
            bool bHomeStaffMemberExists = db.tblHomeStaffMembers.Any(HomeStaffMember => HomeStaffMember.iHomeStaffMemberID == iHomeStaffMemberID && HomeStaffMember.bIsDeleted == false);
            return bHomeStaffMemberExists;
        }

        //Convert database table to class
        public clsHomeStaffMembers convertHomeStaffMembersTableToClass(tblHomeStaffMembers tblHomeStaffMember)
        {
            clsHomeStaffMembers clsHomeStaffMember = new clsHomeStaffMembers();

            clsHomeStaffMember.iHomeStaffMemberID = tblHomeStaffMember.iHomeStaffMemberID;
            clsHomeStaffMember.dtAdded = tblHomeStaffMember.dtAdded;
            clsHomeStaffMember.iAddedBy = tblHomeStaffMember.iAddedBy;
            clsHomeStaffMember.dtEdited = tblHomeStaffMember.dtEdited;
            clsHomeStaffMember.iEditedBy = tblHomeStaffMember.iEditedBy;

            clsHomeStaffMember.iHomeDetailID = tblHomeStaffMember.iHomeDetailID;
            clsHomeStaffMember.strFirstNames = tblHomeStaffMember.strFirstNames;
            clsHomeStaffMember.strSurname = tblHomeStaffMember.strSurname;
            clsHomeStaffMember.strIDNumber = tblHomeStaffMember.strIDNumber;
            clsHomeStaffMember.strPassportNumber = tblHomeStaffMember.strPassportNumber;

            clsHomeStaffMember.strMobileNumber = tblHomeStaffMember.strMobileNumber;
            clsHomeStaffMember.strPathToImages = tblHomeStaffMember.strPathToImages;
            clsHomeStaffMember.strMasterImage = tblHomeStaffMember.strMasterImage;
            clsHomeStaffMember.bIsLivingOnSite = tblHomeStaffMember.bIsLivingOnSite;
            clsHomeStaffMember.bIsDeleted = tblHomeStaffMember.bIsDeleted;

            return clsHomeStaffMember;
        }
    }
}
