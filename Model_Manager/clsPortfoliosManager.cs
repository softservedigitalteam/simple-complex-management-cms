﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsPortfoliosManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsPortfolios> getAllPortfoliosList()
        {
            List<clsPortfolios> lstPortfolios = new List<clsPortfolios>();
            var lstGetPortfoliosList = db.tblPortfolios.Where(Portfolio => Portfolio.bIsDeleted == false).ToList();

            if (lstGetPortfoliosList.Count > 0)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                foreach (var item in lstGetPortfoliosList)
                {
                    clsPortfolios clsPortfolio = new clsPortfolios();

                    clsPortfolio.iPortfolioID = item.iPortfolioID;
                    clsPortfolio.dtAdded = item.dtAdded;
                    clsPortfolio.iAddedBy = item.iAddedBy;
                    clsPortfolio.dtEdited = item.dtEdited;
                    clsPortfolio.iEditedBy = item.iEditedBy;

                    clsPortfolio.strTitle = item.strTitle;
                    clsPortfolio.strEmailAddress1 = item.strEmailAddress1;
                    clsPortfolio.strEmailAddress2 = item.strEmailAddress2;
                    clsPortfolio.strEmailAddress3 = item.strEmailAddress3;
                    clsPortfolio.strEmailAddress4 = item.strEmailAddress4;

                    clsPortfolio.strEmailAddress5 = item.strEmailAddress5;
                    clsPortfolio.strEmailAddress6 = item.strEmailAddress6;
                    clsPortfolio.strEmailAddress7 = item.strEmailAddress7;
                    clsPortfolio.strEmailAddress8 = item.strEmailAddress8;
                    clsPortfolio.bIsDeleted = item.bIsDeleted;

                    clsPortfolio.lstFormTickets = new List<clsFormTickets>();

                    if (item.tblFormTickets.Count > 0)
                    {
                        foreach (var FormTicketsItem in item.tblFormTickets)
                        {
                            clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketsItem);
                            clsPortfolio.lstFormTickets.Add(clsFormTicket);
                        }
                    }

                    lstPortfolios.Add(clsPortfolio);
                }
            }

            return lstPortfolios;
        }

        //Get All Only
        public List<clsPortfolios> getAllPortfoliosOnlyList()
        {
            List<clsPortfolios> lstPortfolios = new List<clsPortfolios>();
            var lstGetPortfoliosList = db.tblPortfolios.Where(Portfolio => Portfolio.bIsDeleted == false).ToList();

            if (lstGetPortfoliosList.Count > 0)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                foreach (var item in lstGetPortfoliosList)
                {
                    clsPortfolios clsPortfolio = new clsPortfolios();

                    clsPortfolio.iPortfolioID = item.iPortfolioID;
                    clsPortfolio.dtAdded = item.dtAdded;
                    clsPortfolio.iAddedBy = item.iAddedBy;
                    clsPortfolio.dtEdited = item.dtEdited;
                    clsPortfolio.iEditedBy = item.iEditedBy;

                    clsPortfolio.strTitle = item.strTitle;
                    clsPortfolio.strEmailAddress1 = item.strEmailAddress1;
                    clsPortfolio.strEmailAddress2 = item.strEmailAddress2;
                    clsPortfolio.strEmailAddress3 = item.strEmailAddress3;
                    clsPortfolio.strEmailAddress4 = item.strEmailAddress4;

                    clsPortfolio.strEmailAddress5 = item.strEmailAddress5;
                    clsPortfolio.strEmailAddress6 = item.strEmailAddress6;
                    clsPortfolio.strEmailAddress7 = item.strEmailAddress7;
                    clsPortfolio.strEmailAddress8 = item.strEmailAddress8;
                    clsPortfolio.bIsDeleted = item.bIsDeleted;

                    clsPortfolio.lstFormTickets = new List<clsFormTickets>();

                    lstPortfolios.Add(clsPortfolio);
                }
            }

            return lstPortfolios;
        }

        //Get
        public clsPortfolios getPortfolioByID(int iPortfolioID)
        {
            clsPortfolios clsPortfolio = null;
            tblPortfolios tblPortfolios = db.tblPortfolios.FirstOrDefault(Portfolio => Portfolio.iPortfolioID == iPortfolioID && Portfolio.bIsDeleted == false);

            if (tblPortfolios != null)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                clsPortfolio = new clsPortfolios();

                clsPortfolio.iPortfolioID = tblPortfolios.iPortfolioID;
                clsPortfolio.dtAdded = tblPortfolios.dtAdded;
                clsPortfolio.iAddedBy = tblPortfolios.iAddedBy;
                clsPortfolio.dtEdited = tblPortfolios.dtEdited;
                clsPortfolio.iEditedBy = tblPortfolios.iEditedBy;

                clsPortfolio.strTitle = tblPortfolios.strTitle;
                clsPortfolio.strEmailAddress1 = tblPortfolios.strEmailAddress1;
                clsPortfolio.strEmailAddress2 = tblPortfolios.strEmailAddress2;
                clsPortfolio.strEmailAddress3 = tblPortfolios.strEmailAddress3;
                clsPortfolio.strEmailAddress4 = tblPortfolios.strEmailAddress4;

                clsPortfolio.strEmailAddress5 = tblPortfolios.strEmailAddress5;
                clsPortfolio.strEmailAddress6 = tblPortfolios.strEmailAddress6;
                clsPortfolio.strEmailAddress7 = tblPortfolios.strEmailAddress7;
                clsPortfolio.strEmailAddress8 = tblPortfolios.strEmailAddress8;
                clsPortfolio.bIsDeleted = tblPortfolios.bIsDeleted;

                clsPortfolio.lstFormTickets = new List<clsFormTickets>();

                if (tblPortfolios.tblFormTickets.Count > 0)
                {
                    foreach (var FormTicketsItem in tblPortfolios.tblFormTickets)
                    {
                        clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketsItem);
                        clsPortfolio.lstFormTickets.Add(clsFormTicket);
                    }
                }
            }

            return clsPortfolio;
        }

        //Save
        public void savePortfolio(clsPortfolios clsPortfolio)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblPortfolios tblPortfolios = new tblPortfolios();

                tblPortfolios.iPortfolioID = clsPortfolio.iPortfolioID;

                tblPortfolios.strTitle = clsPortfolio.strTitle;
                tblPortfolios.strEmailAddress1 = clsPortfolio.strEmailAddress1;
                tblPortfolios.strEmailAddress2 = clsPortfolio.strEmailAddress2;
                tblPortfolios.strEmailAddress3 = clsPortfolio.strEmailAddress3;
                tblPortfolios.strEmailAddress4 = clsPortfolio.strEmailAddress4;

                tblPortfolios.strEmailAddress5 = clsPortfolio.strEmailAddress5;
                tblPortfolios.strEmailAddress6 = clsPortfolio.strEmailAddress6;
                tblPortfolios.strEmailAddress7 = clsPortfolio.strEmailAddress7;
                tblPortfolios.strEmailAddress8 = clsPortfolio.strEmailAddress8;
                tblPortfolios.bIsDeleted = clsPortfolio.bIsDeleted;

                //Add
                if (tblPortfolios.iPortfolioID == 0)
                {
                    tblPortfolios.dtAdded = DateTime.Now;
                    tblPortfolios.iAddedBy = clsCMSUser.iCMSUserID;
                    tblPortfolios.dtEdited = DateTime.Now;
                    tblPortfolios.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblPortfolios.Add(tblPortfolios);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblPortfolios.dtAdded = clsPortfolio.dtAdded;
                    tblPortfolios.iAddedBy = clsPortfolio.iAddedBy;
                    tblPortfolios.dtEdited = DateTime.Now;
                    tblPortfolios.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblPortfolios>().AddOrUpdate(tblPortfolios);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removePortfolioByID(int iPortfolioID)
        {
            tblPortfolios tblPortfolio = db.tblPortfolios.Find(iPortfolioID);
            if (tblPortfolio != null)
            {
                tblPortfolio.bIsDeleted = true;
                db.Entry(tblPortfolio).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfPortfolioExists(int iPortfolioID)
        {
            bool bPortfolioExists = db.tblPortfolios.Any(Portfolio => Portfolio.iPortfolioID == iPortfolioID && Portfolio.bIsDeleted == false);
            return bPortfolioExists;
        }

        //Convert database table to class
        public clsPortfolios convertPortfoliosTableToClass(tblPortfolios tblPortfolio)
        {
            clsPortfolios clsPortfolio = new clsPortfolios();

            clsPortfolio.iPortfolioID = tblPortfolio.iPortfolioID;
            clsPortfolio.dtAdded = tblPortfolio.dtAdded;
            clsPortfolio.iAddedBy = tblPortfolio.iAddedBy;
            clsPortfolio.dtEdited = tblPortfolio.dtEdited;
            clsPortfolio.iEditedBy = tblPortfolio.iEditedBy;

            clsPortfolio.strTitle = tblPortfolio.strTitle;
            clsPortfolio.strEmailAddress1 = tblPortfolio.strEmailAddress1;
            clsPortfolio.strEmailAddress2 = tblPortfolio.strEmailAddress2;
            clsPortfolio.strEmailAddress3 = tblPortfolio.strEmailAddress3;
            clsPortfolio.strEmailAddress4 = tblPortfolio.strEmailAddress4;

            clsPortfolio.strEmailAddress5 = tblPortfolio.strEmailAddress5;
            clsPortfolio.strEmailAddress6 = tblPortfolio.strEmailAddress6;
            clsPortfolio.strEmailAddress7 = tblPortfolio.strEmailAddress7;
            clsPortfolio.strEmailAddress8 = tblPortfolio.strEmailAddress8;
            clsPortfolio.bIsDeleted = tblPortfolio.bIsDeleted;

            return clsPortfolio;
        }
    }
}
