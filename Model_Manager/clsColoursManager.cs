﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsColoursManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsColours> getAllColoursList()
        {
            List<clsColours> lstColours = new List<clsColours>();
            var lstGetColoursList = db.tblColours.Where(Colour => Colour.bIsDeleted == false).ToList();

            if (lstGetColoursList.Count > 0)
            {
                //Home Vehicle Manager
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();

                foreach (var item in lstGetColoursList)
                {
                    clsColours clsColour = new clsColours();

                    clsColour.iColourID = item.iColourID;
                    clsColour.dtAdded = item.dtAdded;
                    clsColour.iAddedBy = item.iAddedBy;
                    clsColour.dtEdited = item.dtEdited;
                    clsColour.iEditedBy = item.iEditedBy;

                    clsColour.strTitle = item.strTitle;
                    clsColour.bIsDeleted = item.bIsDeleted;

                    clsColour.lstHomeVehicles = new List<clsHomeVehicles>();

                    if (item.tblHomeVehicles.Count > 0)
                    {
                        foreach (var HomeVehiclesItem in item.tblHomeVehicles)
                        {
                            clsHomeVehicles clsHomeVehicle = clsHomeVehiclesManager.convertHomeVehiclesTableToClass(HomeVehiclesItem);
                            clsColour.lstHomeVehicles.Add(clsHomeVehicle);
                        }
                    }

                    lstColours.Add(clsColour);
                }
            }

            return lstColours;
        }

        //Get All Only
        public List<clsColours> getAllColoursOnlyList()
        {
            List<clsColours> lstColours = new List<clsColours>();
            var lstGetColoursList = db.tblColours.Where(Colour => Colour.bIsDeleted == false).ToList();

            if (lstGetColoursList.Count > 0)
            {
                //Home Vehicle Manager
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();

                foreach (var item in lstGetColoursList)
                {
                    clsColours clsColour = new clsColours();

                    clsColour.iColourID = item.iColourID;
                    clsColour.dtAdded = item.dtAdded;
                    clsColour.iAddedBy = item.iAddedBy;
                    clsColour.dtEdited = item.dtEdited;
                    clsColour.iEditedBy = item.iEditedBy;

                    clsColour.strTitle = item.strTitle;
                    clsColour.bIsDeleted = item.bIsDeleted;

                    clsColour.lstHomeVehicles = new List<clsHomeVehicles>();

                    lstColours.Add(clsColour);
                }
            }

            return lstColours;
        }

        //Get
        public clsColours getColourByID(int iColourID)
        {
            clsColours clsColour = null;
            tblColours tblColours = db.tblColours.FirstOrDefault(Colour => Colour.iColourID == iColourID && Colour.bIsDeleted == false);

            if (tblColours != null)
            {
                //Home Vehicle Manager
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();

                clsColour = new clsColours();

                clsColour.iColourID = tblColours.iColourID;
                clsColour.dtAdded = tblColours.dtAdded;
                clsColour.iAddedBy = tblColours.iAddedBy;
                clsColour.dtEdited = tblColours.dtEdited;
                clsColour.iEditedBy = tblColours.iEditedBy;

                clsColour.strTitle = tblColours.strTitle;
                clsColour.bIsDeleted = tblColours.bIsDeleted;

                clsColour.lstHomeVehicles = new List<clsHomeVehicles>();

                if (tblColours.tblHomeVehicles.Count > 0)
                {
                    foreach (var HomeVehiclesItem in tblColours.tblHomeVehicles)
                    {
                        clsHomeVehicles clsHomeVehicle = clsHomeVehiclesManager.convertHomeVehiclesTableToClass(HomeVehiclesItem);
                        clsColour.lstHomeVehicles.Add(clsHomeVehicle);
                    }
                }
            }

            return clsColour;
        }

        //Save
        public void saveColour(clsColours clsColour)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblColours tblColours = new tblColours();

                tblColours.iColourID = clsColour.iColourID;

                tblColours.strTitle = clsColour.strTitle;
                tblColours.bIsDeleted = clsColour.bIsDeleted;

                //Add
                if (tblColours.iColourID == 0)
                {
                    tblColours.dtAdded = DateTime.Now;
                    tblColours.iAddedBy = clsCMSUser.iCMSUserID;
                    tblColours.dtEdited = DateTime.Now;
                    tblColours.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblColours.Add(tblColours);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblColours.dtAdded = clsColour.dtAdded;
                    tblColours.iAddedBy = clsColour.iAddedBy;
                    tblColours.dtEdited = DateTime.Now;
                    tblColours.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblColours>().AddOrUpdate(tblColours);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeColourByID(int iColourID)
        {
            tblColours tblColour = db.tblColours.Find(iColourID);
            if (tblColour != null)
            {
                tblColour.bIsDeleted = true;
                db.Entry(tblColour).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfColourExists(int iColourID)
        {
            bool bColourExists = db.tblColours.Any(Colour => Colour.iColourID == iColourID && Colour.bIsDeleted == false);
            return bColourExists;
        }

        //Convert database table to class
        public clsColours convertColoursTableToClass(tblColours tblColour)
        {
            clsColours clsColour = new clsColours();

            clsColour.iColourID = tblColour.iColourID;
            clsColour.dtAdded = tblColour.dtAdded;
            clsColour.iAddedBy = tblColour.iAddedBy;
            clsColour.dtEdited = tblColour.dtEdited;
            clsColour.iEditedBy = tblColour.iEditedBy;

            clsColour.strTitle = tblColour.strTitle;
            clsColour.bIsDeleted = tblColour.bIsDeleted;

            return clsColour;
        }
    }
}
