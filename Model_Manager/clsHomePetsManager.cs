﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomePetsManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomePets> getAllHomePetsList()
        {
            List<clsHomePets> lstHomePets = new List<clsHomePets>();
            var lstGetHomePetsList = db.tblHomePets.Where(HomePet => HomePet.bIsDeleted == false).ToList();

            if (lstGetHomePetsList.Count > 0)
            {
                //Managers
                clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
                clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();

                foreach (var item in lstGetHomePetsList)
                {
                    clsHomePets clsHomePet = new clsHomePets();

                    clsHomePet.iHomePetID = item.iHomePetID;
                    clsHomePet.dtAdded = item.dtAdded;
                    clsHomePet.iAddedBy = item.iAddedBy;
                    clsHomePet.dtEdited = item.dtEdited;
                    clsHomePet.iEditedBy = item.iEditedBy;

                    clsHomePet.strPetName = item.strPetName;
                    clsHomePet.iHomeDetailID = item.iHomeDetailID;
                    clsHomePet.iAnimalTypeID = item.iAnimalTypeID;
                    clsHomePet.iPetColourID = item.iPetColourID;
                    clsHomePet.iPetSizeID = item.iPetSizeID;

                    clsHomePet.strPathToImages = item.strPathToImages;
                    clsHomePet.strMasterImage = item.strMasterImage;
                    clsHomePet.bIsDeleted = item.bIsDeleted;

                    if (item.tblAnimalTypes != null)
                        clsHomePet.clsAnimalType = clsAnimalTypesManager.convertAnimalTypesTableToClass(item.tblAnimalTypes);
                    if (item.tblHomeDetails != null)
                        clsHomePet.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(item.tblHomeDetails);
                    if (item.tblPetColours != null)
                        clsHomePet.clsPetColour = clsPetColoursManager.convertPetColoursTableToClass(item.tblPetColours);
                    if (item.tblPetSizes != null)
                        clsHomePet.clsPetSize = clsPetSizesManager.convertPetSizesTableToClass(item.tblPetSizes);

                    lstHomePets.Add(clsHomePet);
                }
            }

            return lstHomePets;
        }

        //Get All Only
        public List<clsHomePets> getAllHomePetsOnlyList()
        {
            List<clsHomePets> lstHomePets = new List<clsHomePets>();
            var lstGetHomePetsList = db.tblHomePets.Where(HomePet => HomePet.bIsDeleted == false).ToList();

            if (lstGetHomePetsList.Count > 0)
            {
                //Managers
                clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
                clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();

                foreach (var item in lstGetHomePetsList)
                {
                    clsHomePets clsHomePet = new clsHomePets();

                    clsHomePet.iHomePetID = item.iHomePetID;
                    clsHomePet.dtAdded = item.dtAdded;
                    clsHomePet.iAddedBy = item.iAddedBy;
                    clsHomePet.dtEdited = item.dtEdited;
                    clsHomePet.iEditedBy = item.iEditedBy;

                    clsHomePet.strPetName = item.strPetName;
                    clsHomePet.iHomeDetailID = item.iHomeDetailID;
                    clsHomePet.iAnimalTypeID = item.iAnimalTypeID;
                    clsHomePet.iPetColourID = item.iPetColourID;
                    clsHomePet.iPetSizeID = item.iPetSizeID;

                    clsHomePet.strPathToImages = item.strPathToImages;
                    clsHomePet.strMasterImage = item.strMasterImage;
                    clsHomePet.bIsDeleted = item.bIsDeleted;

                    lstHomePets.Add(clsHomePet);
                }
            }

            return lstHomePets;
        }

        //Get
        public clsHomePets getHomePetByID(int iHomePetID)
        {
            clsHomePets clsHomePet = null;
            tblHomePets tblHomePets = db.tblHomePets.FirstOrDefault(HomePet => HomePet.iHomePetID == iHomePetID && HomePet.bIsDeleted == false);

            if (tblHomePets != null)
            {
                //Managers
                clsAnimalTypesManager clsAnimalTypesManager = new clsAnimalTypesManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsPetColoursManager clsPetColoursManager = new clsPetColoursManager();
                clsPetSizesManager clsPetSizesManager = new clsPetSizesManager();

                clsHomePet = new clsHomePets();

                clsHomePet.iHomePetID = tblHomePets.iHomePetID;
                clsHomePet.dtAdded = tblHomePets.dtAdded;
                clsHomePet.iAddedBy = tblHomePets.iAddedBy;
                clsHomePet.dtEdited = tblHomePets.dtEdited;
                clsHomePet.iEditedBy = tblHomePets.iEditedBy;

                clsHomePet.strPetName = tblHomePets.strPetName;
                clsHomePet.iHomeDetailID = tblHomePets.iHomeDetailID;
                clsHomePet.iAnimalTypeID = tblHomePets.iAnimalTypeID;
                clsHomePet.iPetColourID = tblHomePets.iPetColourID;
                clsHomePet.iPetSizeID = tblHomePets.iPetSizeID;

                clsHomePet.strPathToImages = tblHomePets.strPathToImages;
                clsHomePet.strMasterImage = tblHomePets.strMasterImage;
                clsHomePet.bIsDeleted = tblHomePets.bIsDeleted;

                if (tblHomePets.tblAnimalTypes != null)
                    clsHomePet.clsAnimalType = clsAnimalTypesManager.convertAnimalTypesTableToClass(tblHomePets.tblAnimalTypes);
                if (tblHomePets.tblHomeDetails != null)
                    clsHomePet.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(tblHomePets.tblHomeDetails);
                if (tblHomePets.tblPetColours != null)
                    clsHomePet.clsPetColour = clsPetColoursManager.convertPetColoursTableToClass(tblHomePets.tblPetColours);
                if (tblHomePets.tblPetSizes != null)
                    clsHomePet.clsPetSize = clsPetSizesManager.convertPetSizesTableToClass(tblHomePets.tblPetSizes);
            }

            return clsHomePet;
        }

        //Save
        public void saveHomePet(clsHomePets clsHomePet)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomePets tblHomePets = new tblHomePets();

                tblHomePets.iHomePetID = clsHomePet.iHomePetID;

                tblHomePets.strPetName = clsHomePet.strPetName;
                tblHomePets.iHomeDetailID = clsHomePet.iHomeDetailID;
                tblHomePets.iAnimalTypeID = clsHomePet.iAnimalTypeID;
                tblHomePets.iPetColourID = clsHomePet.iPetColourID;
                tblHomePets.iPetSizeID = clsHomePet.iPetSizeID;

                tblHomePets.strPathToImages = clsHomePet.strPathToImages;
                tblHomePets.strMasterImage = clsHomePet.strMasterImage;
                tblHomePets.bIsDeleted = clsHomePet.bIsDeleted;

                //Add
                if (tblHomePets.iHomePetID == 0)
                {
                    tblHomePets.dtAdded = DateTime.Now;
                    tblHomePets.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomePets.dtEdited = DateTime.Now;
                    tblHomePets.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomePets.Add(tblHomePets);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomePets.dtAdded = clsHomePet.dtAdded;
                    tblHomePets.iAddedBy = clsHomePet.iAddedBy;
                    tblHomePets.dtEdited = DateTime.Now;
                    tblHomePets.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomePets>().AddOrUpdate(tblHomePets);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomePetByID(int iHomePetID)
        {
            tblHomePets tblHomePet = db.tblHomePets.Find(iHomePetID);
            if (tblHomePet != null)
            {
                tblHomePet.bIsDeleted = true;
                db.Entry(tblHomePet).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomePetExists(int iHomePetID)
        {
            bool bHomePetExists = db.tblHomePets.Any(HomePet => HomePet.iHomePetID == iHomePetID && HomePet.bIsDeleted == false);
            return bHomePetExists;
        }

        //Convert database table to class
        public clsHomePets convertHomePetsTableToClass(tblHomePets tblHomePet)
        {
            clsHomePets clsHomePet = new clsHomePets();

            clsHomePet.iHomePetID = tblHomePet.iHomePetID;
            clsHomePet.dtAdded = tblHomePet.dtAdded;
            clsHomePet.iAddedBy = tblHomePet.iAddedBy;
            clsHomePet.dtEdited = tblHomePet.dtEdited;
            clsHomePet.iEditedBy = tblHomePet.iEditedBy;

            clsHomePet.strPetName = tblHomePet.strPetName;
            clsHomePet.iHomeDetailID = tblHomePet.iHomeDetailID;
            clsHomePet.iAnimalTypeID = tblHomePet.iAnimalTypeID;
            clsHomePet.iPetColourID = tblHomePet.iPetColourID;
            clsHomePet.iPetSizeID = tblHomePet.iPetSizeID;

            clsHomePet.strPathToImages = tblHomePet.strPathToImages;
            clsHomePet.strMasterImage = tblHomePet.strMasterImage;
            clsHomePet.bIsDeleted = tblHomePet.bIsDeleted;

            return clsHomePet;
        }
    }
}
