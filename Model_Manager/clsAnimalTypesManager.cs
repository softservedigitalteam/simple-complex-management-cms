﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsAnimalTypesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsAnimalTypes> getAllAnimalTypesList()
        {
            List<clsAnimalTypes> lstAnimalTypes = new List<clsAnimalTypes>();
            var lstGetAnimalTypesList = db.tblAnimalTypes.Where(AnimalType => AnimalType.bIsDeleted == false).ToList();

            if (lstGetAnimalTypesList.Count > 0)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                foreach (var item in lstGetAnimalTypesList)
                {
                    clsAnimalTypes clsAnimalType = new clsAnimalTypes();

                    clsAnimalType.iAnimalTypeID = item.iAnimalTypeID;
                    clsAnimalType.dtAdded = item.dtAdded;
                    clsAnimalType.iAddedBy = item.iAddedBy;
                    clsAnimalType.dtEdited = item.dtEdited;
                    clsAnimalType.iEditedBy = item.iEditedBy;

                    clsAnimalType.strTitle = item.strTitle;
                    clsAnimalType.bIsDeleted = item.bIsDeleted;

                    clsAnimalType.lstHomePets = new List<clsHomePets>();

                    if (item.tblHomePets.Count > 0)
                    {
                        foreach (var HomePetsItem in item.tblHomePets)
                        {
                            clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetsItem);
                            clsAnimalType.lstHomePets.Add(clsHomePet);
                        }
                    }

                    lstAnimalTypes.Add(clsAnimalType);
                }
            }

            return lstAnimalTypes;
        }

        //Get All Only
        public List<clsAnimalTypes> getAllAnimalTypesOnlyList()
        {
            List<clsAnimalTypes> lstAnimalTypes = new List<clsAnimalTypes>();
            var lstGetAnimalTypesList = db.tblAnimalTypes.Where(AnimalType => AnimalType.bIsDeleted == false).ToList();

            if (lstGetAnimalTypesList.Count > 0)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                foreach (var item in lstGetAnimalTypesList)
                {
                    clsAnimalTypes clsAnimalType = new clsAnimalTypes();

                    clsAnimalType.iAnimalTypeID = item.iAnimalTypeID;
                    clsAnimalType.dtAdded = item.dtAdded;
                    clsAnimalType.iAddedBy = item.iAddedBy;
                    clsAnimalType.dtEdited = item.dtEdited;
                    clsAnimalType.iEditedBy = item.iEditedBy;

                    clsAnimalType.strTitle = item.strTitle;
                    clsAnimalType.bIsDeleted = item.bIsDeleted;

                    clsAnimalType.lstHomePets = new List<clsHomePets>();

                    lstAnimalTypes.Add(clsAnimalType);
                }
            }

            return lstAnimalTypes;
        }

        //Get
        public clsAnimalTypes getAnimalTypeByID(int iAnimalTypeID)
        {
            clsAnimalTypes clsAnimalType = null;
            tblAnimalTypes tblAnimalTypes = db.tblAnimalTypes.FirstOrDefault(AnimalType => AnimalType.iAnimalTypeID == iAnimalTypeID && AnimalType.bIsDeleted == false);

            if (tblAnimalTypes != null)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                clsAnimalType = new clsAnimalTypes();

                clsAnimalType.iAnimalTypeID = tblAnimalTypes.iAnimalTypeID;
                clsAnimalType.dtAdded = tblAnimalTypes.dtAdded;
                clsAnimalType.iAddedBy = tblAnimalTypes.iAddedBy;
                clsAnimalType.dtEdited = tblAnimalTypes.dtEdited;
                clsAnimalType.iEditedBy = tblAnimalTypes.iEditedBy;

                clsAnimalType.strTitle = tblAnimalTypes.strTitle;
                clsAnimalType.bIsDeleted = tblAnimalTypes.bIsDeleted;

                clsAnimalType.lstHomePets = new List<clsHomePets>();

                if (tblAnimalTypes.tblHomePets.Count > 0)
                {
                    foreach (var HomePetsItem in tblAnimalTypes.tblHomePets)
                    {
                        clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetsItem);
                        clsAnimalType.lstHomePets.Add(clsHomePet);
                    }
                }
            }

            return clsAnimalType;
        }

        //Save
        public void saveAnimalType(clsAnimalTypes clsAnimalType)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblAnimalTypes tblAnimalTypes = new tblAnimalTypes();

                tblAnimalTypes.iAnimalTypeID = clsAnimalType.iAnimalTypeID;

                tblAnimalTypes.strTitle = clsAnimalType.strTitle;
                tblAnimalTypes.bIsDeleted = clsAnimalType.bIsDeleted;

                //Add
                if (tblAnimalTypes.iAnimalTypeID == 0)
                {
                    tblAnimalTypes.dtAdded = DateTime.Now;
                    tblAnimalTypes.iAddedBy = clsCMSUser.iCMSUserID;
                    tblAnimalTypes.dtEdited = DateTime.Now;
                    tblAnimalTypes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblAnimalTypes.Add(tblAnimalTypes);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblAnimalTypes.dtAdded = clsAnimalType.dtAdded;
                    tblAnimalTypes.iAddedBy = clsAnimalType.iAddedBy;
                    tblAnimalTypes.dtEdited = DateTime.Now;
                    tblAnimalTypes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblAnimalTypes>().AddOrUpdate(tblAnimalTypes);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeAnimalTypeByID(int iAnimalTypeID)
        {
            tblAnimalTypes tblAnimalType = db.tblAnimalTypes.Find(iAnimalTypeID);
            if (tblAnimalType != null)
            {
                tblAnimalType.bIsDeleted = true;
                db.Entry(tblAnimalType).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfAnimalTypeExists(int iAnimalTypeID)
        {
            bool bAnimalTypeExists = db.tblAnimalTypes.Any(AnimalType => AnimalType.iAnimalTypeID == iAnimalTypeID && AnimalType.bIsDeleted == false);
            return bAnimalTypeExists;
        }

        //Convert database table to class
        public clsAnimalTypes convertAnimalTypesTableToClass(tblAnimalTypes tblAnimalType)
        {
            clsAnimalTypes clsAnimalType = new clsAnimalTypes();

            clsAnimalType.iAnimalTypeID = tblAnimalType.iAnimalTypeID;
            clsAnimalType.dtAdded = tblAnimalType.dtAdded;
            clsAnimalType.iAddedBy = tblAnimalType.iAddedBy;
            clsAnimalType.dtEdited = tblAnimalType.dtEdited;
            clsAnimalType.iEditedBy = tblAnimalType.iEditedBy;

            clsAnimalType.strTitle = tblAnimalType.strTitle;
            clsAnimalType.bIsDeleted = tblAnimalType.bIsDeleted;

            return clsAnimalType;
        }
    }
}
