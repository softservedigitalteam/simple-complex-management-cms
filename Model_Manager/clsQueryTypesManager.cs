﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsQueryTypesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsQueryTypes> getAllQueryTypesList()
        {
            List<clsQueryTypes> lstQueryTypes = new List<clsQueryTypes>();
            var lstGetQueryTypesList = db.tblQueryTypes.Where(QueryType => QueryType.bIsDeleted == false).ToList();

            if (lstGetQueryTypesList.Count > 0)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                foreach (var item in lstGetQueryTypesList)
                {
                    clsQueryTypes clsQueryType = new clsQueryTypes();

                    clsQueryType.iQueryTypeID = item.iQueryTypeID;
                    clsQueryType.dtAdded = item.dtAdded;
                    clsQueryType.iAddedBy = item.iAddedBy;
                    clsQueryType.dtEdited = item.dtEdited;
                    clsQueryType.iEditedBy = item.iEditedBy;

                    clsQueryType.strQueryTitle = item.strQueryTitle;
                    clsQueryType.bIsDeleted = item.bIsDeleted;

                    clsQueryType.lstFormTickets = new List<clsFormTickets>();

                    if (item.tblFormTickets.Count > 0)
                    {
                        foreach (var FormTicketsItem in item.tblFormTickets)
                        {
                            clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketsItem);
                            clsQueryType.lstFormTickets.Add(clsFormTicket);
                        }
                    }

                    lstQueryTypes.Add(clsQueryType);
                }
            }

            return lstQueryTypes;
        }

        //Get All Only
        public List<clsQueryTypes> getAllQueryTypesOnlyList()
        {
            List<clsQueryTypes> lstQueryTypes = new List<clsQueryTypes>();
            var lstGetQueryTypesList = db.tblQueryTypes.Where(QueryType => QueryType.bIsDeleted == false).ToList();

            if (lstGetQueryTypesList.Count > 0)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                foreach (var item in lstGetQueryTypesList)
                {
                    clsQueryTypes clsQueryType = new clsQueryTypes();

                    clsQueryType.iQueryTypeID = item.iQueryTypeID;
                    clsQueryType.dtAdded = item.dtAdded;
                    clsQueryType.iAddedBy = item.iAddedBy;
                    clsQueryType.dtEdited = item.dtEdited;
                    clsQueryType.iEditedBy = item.iEditedBy;

                    clsQueryType.strQueryTitle = item.strQueryTitle;
                    clsQueryType.bIsDeleted = item.bIsDeleted;

                    clsQueryType.lstFormTickets = new List<clsFormTickets>();

                    lstQueryTypes.Add(clsQueryType);
                }
            }

            return lstQueryTypes;
        }

        //Get
        public clsQueryTypes getQueryTypeByID(int iQueryTypeID)
        {
            clsQueryTypes clsQueryType = null;
            tblQueryTypes tblQueryTypes = db.tblQueryTypes.FirstOrDefault(QueryType => QueryType.iQueryTypeID == iQueryTypeID && QueryType.bIsDeleted == false);

            if (tblQueryTypes != null)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                clsQueryType = new clsQueryTypes();

                clsQueryType.iQueryTypeID = tblQueryTypes.iQueryTypeID;
                clsQueryType.dtAdded = tblQueryTypes.dtAdded;
                clsQueryType.iAddedBy = tblQueryTypes.iAddedBy;
                clsQueryType.dtEdited = tblQueryTypes.dtEdited;
                clsQueryType.iEditedBy = tblQueryTypes.iEditedBy;

                clsQueryType.strQueryTitle = tblQueryTypes.strQueryTitle;
                clsQueryType.bIsDeleted = tblQueryTypes.bIsDeleted;

                clsQueryType.lstFormTickets = new List<clsFormTickets>();

                if (tblQueryTypes.tblFormTickets.Count > 0)
                {
                    foreach (var FormTicketsItem in tblQueryTypes.tblFormTickets)
                    {
                        clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketsItem);
                        clsQueryType.lstFormTickets.Add(clsFormTicket);
                    }
                }
            }

            return clsQueryType;
        }

        //Save
        public void saveQueryType(clsQueryTypes clsQueryType)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblQueryTypes tblQueryTypes = new tblQueryTypes();

                tblQueryTypes.iQueryTypeID = clsQueryType.iQueryTypeID;

                tblQueryTypes.strQueryTitle = clsQueryType.strQueryTitle;
                tblQueryTypes.bIsDeleted = clsQueryType.bIsDeleted;

                //Add
                if (tblQueryTypes.iQueryTypeID == 0)
                {
                    tblQueryTypes.dtAdded = DateTime.Now;
                    tblQueryTypes.iAddedBy = clsCMSUser.iCMSUserID;
                    tblQueryTypes.dtEdited = DateTime.Now;
                    tblQueryTypes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblQueryTypes.Add(tblQueryTypes);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblQueryTypes.dtAdded = clsQueryType.dtAdded;
                    tblQueryTypes.iAddedBy = clsQueryType.iAddedBy;
                    tblQueryTypes.dtEdited = DateTime.Now;
                    tblQueryTypes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblQueryTypes>().AddOrUpdate(tblQueryTypes);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeQueryTypeByID(int iQueryTypeID)
        {
            tblQueryTypes tblQueryType = db.tblQueryTypes.Find(iQueryTypeID);
            if (tblQueryType != null)
            {
                tblQueryType.bIsDeleted = true;
                db.Entry(tblQueryType).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfQueryTypeExists(int iQueryTypeID)
        {
            bool bQueryTypeExists = db.tblQueryTypes.Any(QueryType => QueryType.iQueryTypeID == iQueryTypeID && QueryType.bIsDeleted == false);
            return bQueryTypeExists;
        }

        //Convert database table to class
        public clsQueryTypes convertQueryTypesTableToClass(tblQueryTypes tblQueryType)
        {
            clsQueryTypes clsQueryType = new clsQueryTypes();

            clsQueryType.iQueryTypeID = tblQueryType.iQueryTypeID;
            clsQueryType.dtAdded = tblQueryType.dtAdded;
            clsQueryType.iAddedBy = tblQueryType.iAddedBy;
            clsQueryType.dtEdited = tblQueryType.dtEdited;
            clsQueryType.iEditedBy = tblQueryType.iEditedBy;

            clsQueryType.strQueryTitle = tblQueryType.strQueryTitle;
            clsQueryType.bIsDeleted = tblQueryType.bIsDeleted;

            return clsQueryType;
        }
    }
}
