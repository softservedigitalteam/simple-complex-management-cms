﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomeHistoryLogsManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomeHistoryLogs> getAllHomeHistoryLogsList()
        {
            List<clsHomeHistoryLogs> lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
            var lstGetHomeHistoryLogsList = db.tblHomeHistoryLogs.Where(HomeHistoryLog => HomeHistoryLog.bIsDeleted == false).ToList();

            if (lstGetHomeHistoryLogsList.Count > 0)
            {
                //Managers
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
                clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetHomeHistoryLogsList)
                {
                    clsHomeHistoryLogs clsHomeHistoryLog = new clsHomeHistoryLogs();

                    clsHomeHistoryLog.iHomeHistoryLogID = item.iHomeHistoryLogID;
                    clsHomeHistoryLog.dtAdded = item.dtAdded;
                    clsHomeHistoryLog.iAddedBy = item.iAddedBy;
                    clsHomeHistoryLog.dtEdited = item.dtEdited;
                    clsHomeHistoryLog.iEditedBy = item.iEditedBy;

                    clsHomeHistoryLog.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeHistoryLog.iHomeOccupantID = item.iHomeOccupantID;
                    clsHomeHistoryLog.iHomeDetailID = item.iHomeDetailID;
                    clsHomeHistoryLog.bIsDeleted = item.bIsDeleted;

                    if (item.tblHomeOwners != null)
                        clsHomeHistoryLog.clsHomeOwner = clsHomeOwnersManager.convertHomeOwnersTableToClass(item.tblHomeOwners);
                    if (item.tblHomeOccupants != null)
                        clsHomeHistoryLog.clsHomeOccupant = clsHomeOccupantsManager.convertHomeOccupantsTableToClass(item.tblHomeOccupants);
                    if (item.tblHomeDetails != null)
                        clsHomeHistoryLog.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(item.tblHomeDetails);

                    lstHomeHistoryLogs.Add(clsHomeHistoryLog);
                }
            }

            return lstHomeHistoryLogs;
        }

        //Get All Only
        public List<clsHomeHistoryLogs> getAllHomeHistoryLogsOnlyList()
        {
            List<clsHomeHistoryLogs> lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
            var lstGetHomeHistoryLogsList = db.tblHomeHistoryLogs.Where(HomeHistoryLog => HomeHistoryLog.bIsDeleted == false).ToList();

            if (lstGetHomeHistoryLogsList.Count > 0)
            {
                //Managers
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
                clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetHomeHistoryLogsList)
                {
                    clsHomeHistoryLogs clsHomeHistoryLog = new clsHomeHistoryLogs();

                    clsHomeHistoryLog.iHomeHistoryLogID = item.iHomeHistoryLogID;
                    clsHomeHistoryLog.dtAdded = item.dtAdded;
                    clsHomeHistoryLog.iAddedBy = item.iAddedBy;
                    clsHomeHistoryLog.dtEdited = item.dtEdited;
                    clsHomeHistoryLog.iEditedBy = item.iEditedBy;

                    clsHomeHistoryLog.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeHistoryLog.iHomeOccupantID = item.iHomeOccupantID;
                    clsHomeHistoryLog.iHomeDetailID = item.iHomeDetailID;
                    clsHomeHistoryLog.bIsDeleted = item.bIsDeleted;

                    lstHomeHistoryLogs.Add(clsHomeHistoryLog);
                }
            }

            return lstHomeHistoryLogs;
        }

        //Get
        public clsHomeHistoryLogs getHomeHistoryLogByID(int iHomeHistoryLogID)
        {
            clsHomeHistoryLogs clsHomeHistoryLog = null;
            tblHomeHistoryLogs tblHomeHistoryLogs = db.tblHomeHistoryLogs.FirstOrDefault(HomeHistoryLog => HomeHistoryLog.iHomeHistoryLogID == iHomeHistoryLogID && HomeHistoryLog.bIsDeleted == false);

            if (tblHomeHistoryLogs != null)
            {
                //Managers
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
                clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                clsHomeHistoryLog = new clsHomeHistoryLogs();

                clsHomeHistoryLog.iHomeHistoryLogID = tblHomeHistoryLogs.iHomeHistoryLogID;
                clsHomeHistoryLog.dtAdded = tblHomeHistoryLogs.dtAdded;
                clsHomeHistoryLog.iAddedBy = tblHomeHistoryLogs.iAddedBy;
                clsHomeHistoryLog.dtEdited = tblHomeHistoryLogs.dtEdited;
                clsHomeHistoryLog.iEditedBy = tblHomeHistoryLogs.iEditedBy;

                clsHomeHistoryLog.iHomeOwnerID = tblHomeHistoryLogs.iHomeOwnerID;
                clsHomeHistoryLog.iHomeOccupantID = tblHomeHistoryLogs.iHomeOccupantID;
                clsHomeHistoryLog.iHomeDetailID = tblHomeHistoryLogs.iHomeDetailID;
                clsHomeHistoryLog.bIsDeleted = tblHomeHistoryLogs.bIsDeleted;

                if (tblHomeHistoryLogs.tblHomeOwners != null)
                    clsHomeHistoryLog.clsHomeOwner = clsHomeOwnersManager.convertHomeOwnersTableToClass(tblHomeHistoryLogs.tblHomeOwners);
                if (tblHomeHistoryLogs.tblHomeOccupants != null)
                    clsHomeHistoryLog.clsHomeOccupant = clsHomeOccupantsManager.convertHomeOccupantsTableToClass(tblHomeHistoryLogs.tblHomeOccupants);
                if (tblHomeHistoryLogs.tblHomeDetails != null)
                    clsHomeHistoryLog.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(tblHomeHistoryLogs.tblHomeDetails);
            }

            return clsHomeHistoryLog;
        }

        //Save
        public void saveHomeHistoryLog(clsHomeHistoryLogs clsHomeHistoryLog)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomeHistoryLogs tblHomeHistoryLogs = new tblHomeHistoryLogs();

                tblHomeHistoryLogs.iHomeHistoryLogID = clsHomeHistoryLog.iHomeHistoryLogID;

                clsHomeHistoryLog.iHomeOwnerID = clsHomeHistoryLog.iHomeOwnerID;
                clsHomeHistoryLog.iHomeOccupantID = clsHomeHistoryLog.iHomeOccupantID;
                clsHomeHistoryLog.iHomeDetailID = clsHomeHistoryLog.iHomeDetailID;
                clsHomeHistoryLog.bIsDeleted = clsHomeHistoryLog.bIsDeleted;

                //Add
                if (tblHomeHistoryLogs.iHomeHistoryLogID == 0)
                {
                    tblHomeHistoryLogs.dtAdded = DateTime.Now;
                    tblHomeHistoryLogs.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomeHistoryLogs.dtEdited = DateTime.Now;
                    tblHomeHistoryLogs.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomeHistoryLogs.Add(tblHomeHistoryLogs);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomeHistoryLogs.dtAdded = clsHomeHistoryLog.dtAdded;
                    tblHomeHistoryLogs.iAddedBy = clsHomeHistoryLog.iAddedBy;
                    tblHomeHistoryLogs.dtEdited = DateTime.Now;
                    tblHomeHistoryLogs.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomeHistoryLogs>().AddOrUpdate(tblHomeHistoryLogs);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomeHistoryLogByID(int iHomeHistoryLogID)
        {
            tblHomeHistoryLogs tblHomeHistoryLog = db.tblHomeHistoryLogs.Find(iHomeHistoryLogID);
            if (tblHomeHistoryLog != null)
            {
                tblHomeHistoryLog.bIsDeleted = true;
                db.Entry(tblHomeHistoryLog).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomeHistoryLogExists(int iHomeHistoryLogID)
        {
            bool bHomeHistoryLogExists = db.tblHomeHistoryLogs.Any(HomeHistoryLog => HomeHistoryLog.iHomeHistoryLogID == iHomeHistoryLogID && HomeHistoryLog.bIsDeleted == false);
            return bHomeHistoryLogExists;
        }

        //Convert database table to class
        public clsHomeHistoryLogs convertHomeHistoryLogsTableToClass(tblHomeHistoryLogs tblHomeHistoryLog)
        {
            clsHomeHistoryLogs clsHomeHistoryLog = new clsHomeHistoryLogs();

            clsHomeHistoryLog.iHomeHistoryLogID = tblHomeHistoryLog.iHomeHistoryLogID;
            clsHomeHistoryLog.dtAdded = tblHomeHistoryLog.dtAdded;
            clsHomeHistoryLog.iAddedBy = tblHomeHistoryLog.iAddedBy;
            clsHomeHistoryLog.dtEdited = tblHomeHistoryLog.dtEdited;
            clsHomeHistoryLog.iEditedBy = tblHomeHistoryLog.iEditedBy;

            clsHomeHistoryLog.iHomeOwnerID = tblHomeHistoryLog.iHomeOwnerID;
            clsHomeHistoryLog.iHomeOccupantID = tblHomeHistoryLog.iHomeOccupantID;
            clsHomeHistoryLog.iHomeDetailID = tblHomeHistoryLog.iHomeDetailID;
            clsHomeHistoryLog.bIsDeleted = tblHomeHistoryLog.bIsDeleted;

            return clsHomeHistoryLog;
        }
    }
}
