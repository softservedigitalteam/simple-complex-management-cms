﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsComplexTypesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsComplexTypes> getAllComplexTypesList()
        {
            List<clsComplexTypes> lstComplexTypes = new List<clsComplexTypes>();
            var lstGetComplexTypesList = db.tblComplexTypes.Where(ComplexType => ComplexType.bIsDeleted == false).ToList();

            if (lstGetComplexTypesList.Count > 0)
            {
                //Complex Manager
                clsComplexesManager clsComplexesManager = new clsComplexesManager();

                foreach (var item in lstGetComplexTypesList)
                {
                    clsComplexTypes clsComplexType = new clsComplexTypes();

                    clsComplexType.iComplexTypeID = item.iComplexTypeID;
                    clsComplexType.dtAdded = item.dtAdded;
                    clsComplexType.iAddedBy = item.iAddedBy;
                    clsComplexType.dtEdited = item.dtEdited;
                    clsComplexType.iEditedBy = item.iEditedBy;

                    clsComplexType.strTitle = item.strTitle;
                    clsComplexType.bIsDeleted = item.bIsDeleted;

                    clsComplexType.lstComplexes = new List<clsComplexes>();

                    if (item.tblComplexes.Count > 0)
                    {
                        foreach (var ComplexesItem in item.tblComplexes)
                        {
                            clsComplexes clsComplex = clsComplexesManager.convertComplexesTableToClass(ComplexesItem);
                            clsComplexType.lstComplexes.Add(clsComplex);
                        }
                    }

                    lstComplexTypes.Add(clsComplexType);
                }
            }

            return lstComplexTypes;
        }

        //Get All Only
        public List<clsComplexTypes> getAllComplexTypesOnlyList()
        {
            List<clsComplexTypes> lstComplexTypes = new List<clsComplexTypes>();
            var lstGetComplexTypesList = db.tblComplexTypes.Where(ComplexType => ComplexType.bIsDeleted == false).ToList();

            if (lstGetComplexTypesList.Count > 0)
            {
                //Complex Manager
                clsComplexesManager clsComplexesManager = new clsComplexesManager();

                foreach (var item in lstGetComplexTypesList)
                {
                    clsComplexTypes clsComplexType = new clsComplexTypes();

                    clsComplexType.iComplexTypeID = item.iComplexTypeID;
                    clsComplexType.dtAdded = item.dtAdded;
                    clsComplexType.iAddedBy = item.iAddedBy;
                    clsComplexType.dtEdited = item.dtEdited;
                    clsComplexType.iEditedBy = item.iEditedBy;

                    clsComplexType.strTitle = item.strTitle;
                    clsComplexType.bIsDeleted = item.bIsDeleted;

                    clsComplexType.lstComplexes = new List<clsComplexes>();

                    lstComplexTypes.Add(clsComplexType);
                }
            }

            return lstComplexTypes;
        }

        //Get
        public clsComplexTypes getComplexTypeByID(int iComplexTypeID)
        {
            clsComplexTypes clsComplexType = null;
            tblComplexTypes tblComplexTypes = db.tblComplexTypes.FirstOrDefault(ComplexType => ComplexType.iComplexTypeID == iComplexTypeID && ComplexType.bIsDeleted == false);

            if (tblComplexTypes != null)
            {
                //Complex Manager
                clsComplexesManager clsComplexesManager = new clsComplexesManager();

                clsComplexType = new clsComplexTypes();

                clsComplexType.iComplexTypeID = tblComplexTypes.iComplexTypeID;
                clsComplexType.dtAdded = tblComplexTypes.dtAdded;
                clsComplexType.iAddedBy = tblComplexTypes.iAddedBy;
                clsComplexType.dtEdited = tblComplexTypes.dtEdited;
                clsComplexType.iEditedBy = tblComplexTypes.iEditedBy;

                clsComplexType.strTitle = tblComplexTypes.strTitle;
                clsComplexType.bIsDeleted = tblComplexTypes.bIsDeleted;

                clsComplexType.lstComplexes = new List<clsComplexes>();

                if (tblComplexTypes.tblComplexes.Count > 0)
                {
                    foreach (var ComplexesItem in tblComplexTypes.tblComplexes)
                    {
                        clsComplexes clsComplex = clsComplexesManager.convertComplexesTableToClass(ComplexesItem);
                        clsComplexType.lstComplexes.Add(clsComplex);
                    }
                }
            }

            return clsComplexType;
        }

        //Save
        public void saveComplexType(clsComplexTypes clsComplexType)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblComplexTypes tblComplexTypes = new tblComplexTypes();

                tblComplexTypes.iComplexTypeID = clsComplexType.iComplexTypeID;

                tblComplexTypes.strTitle = clsComplexType.strTitle;
                tblComplexTypes.bIsDeleted = clsComplexType.bIsDeleted;

                //Add
                if (tblComplexTypes.iComplexTypeID == 0)
                {
                    tblComplexTypes.dtAdded = DateTime.Now;
                    tblComplexTypes.iAddedBy = clsCMSUser.iCMSUserID;
                    tblComplexTypes.dtEdited = DateTime.Now;
                    tblComplexTypes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblComplexTypes.Add(tblComplexTypes);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblComplexTypes.dtAdded = clsComplexType.dtAdded;
                    tblComplexTypes.iAddedBy = clsComplexType.iAddedBy;
                    tblComplexTypes.dtEdited = DateTime.Now;
                    tblComplexTypes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblComplexTypes>().AddOrUpdate(tblComplexTypes);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeComplexTypeByID(int iComplexTypeID)
        {
            tblComplexTypes tblComplexType = db.tblComplexTypes.Find(iComplexTypeID);
            if (tblComplexType != null)
            {
                tblComplexType.bIsDeleted = true;
                db.Entry(tblComplexType).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfComplexTypeExists(int iComplexTypeID)
        {
            bool bComplexTypeExists = db.tblComplexTypes.Any(ComplexType => ComplexType.iComplexTypeID == iComplexTypeID && ComplexType.bIsDeleted == false);
            return bComplexTypeExists;
        }

        //Convert database table to class
        public clsComplexTypes convertComplexTypesTableToClass(tblComplexTypes tblComplexType)
        {
            clsComplexTypes clsComplexType = new clsComplexTypes();

            clsComplexType.iComplexTypeID = tblComplexType.iComplexTypeID;
            clsComplexType.dtAdded = tblComplexType.dtAdded;
            clsComplexType.iAddedBy = tblComplexType.iAddedBy;
            clsComplexType.dtEdited = tblComplexType.dtEdited;
            clsComplexType.iEditedBy = tblComplexType.iEditedBy;

            clsComplexType.strTitle = tblComplexType.strTitle;
            clsComplexType.bIsDeleted = tblComplexType.bIsDeleted;

            return clsComplexType;
        }
    }
}
