﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsFormTicketsManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsFormTickets> getAllFormTicketsList()
        {
            List<clsFormTickets> lstFormTickets = new List<clsFormTickets>();
            var lstGetFormTicketsList = db.tblFormTickets.Where(FormTicket => FormTicket.bIsDeleted == false).ToList();

            if (lstGetFormTicketsList.Count > 0)
            {
                //Managers
                clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
                clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();

                foreach (var item in lstGetFormTicketsList)
                {
                    clsFormTickets clsFormTicket = new clsFormTickets();

                    clsFormTicket.iFormTicketID = item.iFormTicketID;
                    clsFormTicket.dtAdded = item.dtAdded;
                    clsFormTicket.iAddedBy = item.iAddedBy;
                    clsFormTicket.dtEdited = item.dtEdited;
                    clsFormTicket.iEditedBy = item.iEditedBy;

                    clsFormTicket.strNameAndSurname = item.strNameAndSurname;
                    clsFormTicket.strContactNumber = item.strContactNumber;
                    clsFormTicket.strAlternateContactNumber = item.strAlternateContactNumber;
                    clsFormTicket.strEmailAddress = item.strEmailAddress;
                    clsFormTicket.iBlockNumber = item.iBlockNumber;

                    clsFormTicket.iUnitNumber = item.iUnitNumber;
                    clsFormTicket.strDetailsOfQueryComplaintOrCompliment = item.strDetailsOfQueryComplaintOrCompliment;
                    clsFormTicket.iQueryTypeID = item.iQueryTypeID;
                    clsFormTicket.iPortfolioID = item.iPortfolioID;
                    clsFormTicket.strPathToDocuments = item.strPathToDocuments;

                    clsFormTicket.strMasterDocument = item.strMasterDocument;
                    clsFormTicket.iHomeDetailID = item.iHomeDetailID;
                    clsFormTicket.iFormTicketStatusID = item.iFormTicketStatusID;
                    clsFormTicket.bIsResolved = item.bIsResolved;
                    clsFormTicket.bIsDeleted = item.bIsDeleted;

                    if (item.tblFormTicketStatuses != null)
                        clsFormTicket.clsFormTicketStatus = clsFormTicketStatusesManager.convertFormTicketStatusesTableToClass(item.tblFormTicketStatuses);
                    if (item.tblHomeDetails != null)
                        clsFormTicket.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(item.tblHomeDetails);
                    if (item.tblPortfolios != null)
                        clsFormTicket.clsPortfolio = clsPortfoliosManager.convertPortfoliosTableToClass(item.tblPortfolios);
                    if (item.tblQueryTypes != null)
                        clsFormTicket.clsQueryType = clsQueryTypesManager.convertQueryTypesTableToClass(item.tblQueryTypes);

                    lstFormTickets.Add(clsFormTicket);
                }
            }

            return lstFormTickets;
        }

        //Get All Only
        public List<clsFormTickets> getAllFormTicketsOnlyList()
        {
            List<clsFormTickets> lstFormTickets = new List<clsFormTickets>();
            var lstGetFormTicketsList = db.tblFormTickets.Where(FormTicket => FormTicket.bIsDeleted == false).ToList();

            if (lstGetFormTicketsList.Count > 0)
            {
                //Managers
                clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
                clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();

                foreach (var item in lstGetFormTicketsList)
                {
                    clsFormTickets clsFormTicket = new clsFormTickets();

                    clsFormTicket.iFormTicketID = item.iFormTicketID;
                    clsFormTicket.dtAdded = item.dtAdded;
                    clsFormTicket.iAddedBy = item.iAddedBy;
                    clsFormTicket.dtEdited = item.dtEdited;
                    clsFormTicket.iEditedBy = item.iEditedBy;

                    clsFormTicket.strNameAndSurname = item.strNameAndSurname;
                    clsFormTicket.strContactNumber = item.strContactNumber;
                    clsFormTicket.strAlternateContactNumber = item.strAlternateContactNumber;
                    clsFormTicket.strEmailAddress = item.strEmailAddress;
                    clsFormTicket.iBlockNumber = item.iBlockNumber;

                    clsFormTicket.iUnitNumber = item.iUnitNumber;
                    clsFormTicket.strDetailsOfQueryComplaintOrCompliment = item.strDetailsOfQueryComplaintOrCompliment;
                    clsFormTicket.iQueryTypeID = item.iQueryTypeID;
                    clsFormTicket.iPortfolioID = item.iPortfolioID;
                    clsFormTicket.strPathToDocuments = item.strPathToDocuments;

                    clsFormTicket.strMasterDocument = item.strMasterDocument;
                    clsFormTicket.iHomeDetailID = item.iHomeDetailID;
                    clsFormTicket.iFormTicketStatusID = item.iFormTicketStatusID;
                    clsFormTicket.bIsResolved = item.bIsResolved;
                    clsFormTicket.bIsDeleted = item.bIsDeleted;

                    lstFormTickets.Add(clsFormTicket);
                }
            }

            return lstFormTickets;
        }

        //Get
        public clsFormTickets getFormTicketByID(int iFormTicketID)
        {
            clsFormTickets clsFormTicket = null;
            tblFormTickets tblFormTickets = db.tblFormTickets.FirstOrDefault(FormTicket => FormTicket.iFormTicketID == iFormTicketID && FormTicket.bIsDeleted == false);

            if (tblFormTickets != null)
            {
                //Managers
                clsFormTicketStatusesManager clsFormTicketStatusesManager = new clsFormTicketStatusesManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsPortfoliosManager clsPortfoliosManager = new clsPortfoliosManager();
                clsQueryTypesManager clsQueryTypesManager = new clsQueryTypesManager();

                clsFormTicket = new clsFormTickets();

                clsFormTicket.iFormTicketID = tblFormTickets.iFormTicketID;
                clsFormTicket.dtAdded = tblFormTickets.dtAdded;
                clsFormTicket.iAddedBy = tblFormTickets.iAddedBy;
                clsFormTicket.dtEdited = tblFormTickets.dtEdited;
                clsFormTicket.iEditedBy = tblFormTickets.iEditedBy;

                clsFormTicket.strNameAndSurname = tblFormTickets.strNameAndSurname;
                clsFormTicket.strContactNumber = tblFormTickets.strContactNumber;
                clsFormTicket.strAlternateContactNumber = tblFormTickets.strAlternateContactNumber;
                clsFormTicket.strEmailAddress = tblFormTickets.strEmailAddress;
                clsFormTicket.iBlockNumber = tblFormTickets.iBlockNumber;

                clsFormTicket.iUnitNumber = tblFormTickets.iUnitNumber;
                clsFormTicket.strDetailsOfQueryComplaintOrCompliment = tblFormTickets.strDetailsOfQueryComplaintOrCompliment;
                clsFormTicket.iQueryTypeID = tblFormTickets.iQueryTypeID;
                clsFormTicket.iPortfolioID = tblFormTickets.iPortfolioID;
                clsFormTicket.strPathToDocuments = tblFormTickets.strPathToDocuments;

                clsFormTicket.strMasterDocument = tblFormTickets.strMasterDocument;
                clsFormTicket.iHomeDetailID = tblFormTickets.iHomeDetailID;
                clsFormTicket.iFormTicketStatusID = tblFormTickets.iFormTicketStatusID;
                clsFormTicket.bIsResolved = tblFormTickets.bIsResolved;
                clsFormTicket.bIsDeleted = tblFormTickets.bIsDeleted;

                if (tblFormTickets.tblFormTicketStatuses != null)
                    clsFormTicket.clsFormTicketStatus = clsFormTicketStatusesManager.convertFormTicketStatusesTableToClass(tblFormTickets.tblFormTicketStatuses);
                if (tblFormTickets.tblHomeDetails != null)
                    clsFormTicket.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(tblFormTickets.tblHomeDetails);
                if (tblFormTickets.tblPortfolios != null)
                    clsFormTicket.clsPortfolio = clsPortfoliosManager.convertPortfoliosTableToClass(tblFormTickets.tblPortfolios);
                if (tblFormTickets.tblQueryTypes != null)
                    clsFormTicket.clsQueryType = clsQueryTypesManager.convertQueryTypesTableToClass(tblFormTickets.tblQueryTypes);
            }

            return clsFormTicket;
        }

        //Save
        public void saveFormTicket(clsFormTickets clsFormTicket)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblFormTickets tblFormTickets = new tblFormTickets();

                tblFormTickets.iFormTicketID = clsFormTicket.iFormTicketID;

                tblFormTickets.strNameAndSurname = clsFormTicket.strNameAndSurname;
                tblFormTickets.strContactNumber = clsFormTicket.strContactNumber;
                tblFormTickets.strAlternateContactNumber = clsFormTicket.strAlternateContactNumber;
                tblFormTickets.strEmailAddress = clsFormTicket.strEmailAddress;
                tblFormTickets.iBlockNumber = clsFormTicket.iBlockNumber;

                tblFormTickets.iUnitNumber = clsFormTicket.iUnitNumber;
                tblFormTickets.strDetailsOfQueryComplaintOrCompliment = clsFormTicket.strDetailsOfQueryComplaintOrCompliment;
                tblFormTickets.iQueryTypeID = clsFormTicket.iQueryTypeID;
                tblFormTickets.iPortfolioID = clsFormTicket.iPortfolioID;
                tblFormTickets.strPathToDocuments = clsFormTicket.strPathToDocuments;

                tblFormTickets.strMasterDocument = clsFormTicket.strMasterDocument;
                tblFormTickets.iHomeDetailID = clsFormTicket.iHomeDetailID;
                tblFormTickets.iFormTicketStatusID = clsFormTicket.iFormTicketStatusID;
                tblFormTickets.bIsResolved = clsFormTicket.bIsResolved;
                tblFormTickets.bIsDeleted = clsFormTicket.bIsDeleted;

                //Add
                if (tblFormTickets.iFormTicketID == 0)
                {
                    tblFormTickets.dtAdded = DateTime.Now;
                    tblFormTickets.iAddedBy = clsCMSUser.iCMSUserID;
                    tblFormTickets.dtEdited = DateTime.Now;
                    tblFormTickets.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblFormTickets.Add(tblFormTickets);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblFormTickets.dtAdded = clsFormTicket.dtAdded;
                    tblFormTickets.iAddedBy = clsFormTicket.iAddedBy;
                    tblFormTickets.dtEdited = DateTime.Now;
                    tblFormTickets.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblFormTickets>().AddOrUpdate(tblFormTickets);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeFormTicketByID(int iFormTicketID)
        {
            tblFormTickets tblFormTicket = db.tblFormTickets.Find(iFormTicketID);
            if (tblFormTicket != null)
            {
                tblFormTicket.bIsDeleted = true;
                db.Entry(tblFormTicket).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfFormTicketExists(int iFormTicketID)
        {
            bool bFormTicketExists = db.tblFormTickets.Any(FormTicket => FormTicket.iFormTicketID == iFormTicketID && FormTicket.bIsDeleted == false);
            return bFormTicketExists;
        }

        //Convert database table to class
        public clsFormTickets convertFormTicketsTableToClass(tblFormTickets tblFormTicket)
        {
            clsFormTickets clsFormTicket = new clsFormTickets();

            clsFormTicket.iFormTicketID = tblFormTicket.iFormTicketID;
            clsFormTicket.dtAdded = tblFormTicket.dtAdded;
            clsFormTicket.iAddedBy = tblFormTicket.iAddedBy;
            clsFormTicket.dtEdited = tblFormTicket.dtEdited;
            clsFormTicket.iEditedBy = tblFormTicket.iEditedBy;

            clsFormTicket.strNameAndSurname = tblFormTicket.strNameAndSurname;
            clsFormTicket.strContactNumber = tblFormTicket.strContactNumber;
            clsFormTicket.strAlternateContactNumber = tblFormTicket.strAlternateContactNumber;
            clsFormTicket.strEmailAddress = tblFormTicket.strEmailAddress;
            clsFormTicket.iBlockNumber = tblFormTicket.iBlockNumber;

            clsFormTicket.iUnitNumber = tblFormTicket.iUnitNumber;
            clsFormTicket.strDetailsOfQueryComplaintOrCompliment = tblFormTicket.strDetailsOfQueryComplaintOrCompliment;
            clsFormTicket.iQueryTypeID = tblFormTicket.iQueryTypeID;
            clsFormTicket.iPortfolioID = tblFormTicket.iPortfolioID;
            clsFormTicket.strPathToDocuments = tblFormTicket.strPathToDocuments;

            clsFormTicket.strMasterDocument = tblFormTicket.strMasterDocument;
            clsFormTicket.iHomeDetailID = tblFormTicket.iHomeDetailID;
            clsFormTicket.iFormTicketStatusID = tblFormTicket.iFormTicketStatusID;
            clsFormTicket.bIsResolved = tblFormTicket.bIsResolved;
            clsFormTicket.bIsDeleted = tblFormTicket.bIsDeleted;

            return clsFormTicket;
        }
    }
}
