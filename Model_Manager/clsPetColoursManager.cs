﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsPetColoursManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsPetColours> getAllPetColoursList()
        {
            List<clsPetColours> lstPetColours = new List<clsPetColours>();
            var lstGetPetColoursList = db.tblPetColours.Where(PetColour => PetColour.bIsDeleted == false).ToList();

            if (lstGetPetColoursList.Count > 0)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                foreach (var item in lstGetPetColoursList)
                {
                    clsPetColours clsPetColour = new clsPetColours();

                    clsPetColour.iPetColourID = item.iPetColourID;
                    clsPetColour.dtAdded = item.dtAdded;
                    clsPetColour.iAddedBy = item.iAddedBy;
                    clsPetColour.dtEdited = item.dtEdited;
                    clsPetColour.iEditedBy = item.iEditedBy;

                    clsPetColour.strTitle = item.strTitle;
                    clsPetColour.bIsDeleted = item.bIsDeleted;

                    clsPetColour.lstHomePets = new List<clsHomePets>();

                    if (item.tblHomePets.Count > 0)
                    {
                        foreach (var HomePetsItem in item.tblHomePets)
                        {
                            clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetsItem);
                            clsPetColour.lstHomePets.Add(clsHomePet);
                        }
                    }

                    lstPetColours.Add(clsPetColour);
                }
            }

            return lstPetColours;
        }

        //Get All Only
        public List<clsPetColours> getAllPetColoursOnlyList()
        {
            List<clsPetColours> lstPetColours = new List<clsPetColours>();
            var lstGetPetColoursList = db.tblPetColours.Where(PetColour => PetColour.bIsDeleted == false).ToList();

            if (lstGetPetColoursList.Count > 0)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                foreach (var item in lstGetPetColoursList)
                {
                    clsPetColours clsPetColour = new clsPetColours();

                    clsPetColour.iPetColourID = item.iPetColourID;
                    clsPetColour.dtAdded = item.dtAdded;
                    clsPetColour.iAddedBy = item.iAddedBy;
                    clsPetColour.dtEdited = item.dtEdited;
                    clsPetColour.iEditedBy = item.iEditedBy;

                    clsPetColour.strTitle = item.strTitle;
                    clsPetColour.bIsDeleted = item.bIsDeleted;

                    clsPetColour.lstHomePets = new List<clsHomePets>();

                    lstPetColours.Add(clsPetColour);
                }
            }

            return lstPetColours;
        }

        //Get
        public clsPetColours getPetColourByID(int iPetColourID)
        {
            clsPetColours clsPetColour = null;
            tblPetColours tblPetColours = db.tblPetColours.FirstOrDefault(PetColour => PetColour.iPetColourID == iPetColourID && PetColour.bIsDeleted == false);

            if (tblPetColours != null)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                clsPetColour = new clsPetColours();

                clsPetColour.iPetColourID = tblPetColours.iPetColourID;
                clsPetColour.dtAdded = tblPetColours.dtAdded;
                clsPetColour.iAddedBy = tblPetColours.iAddedBy;
                clsPetColour.dtEdited = tblPetColours.dtEdited;
                clsPetColour.iEditedBy = tblPetColours.iEditedBy;

                clsPetColour.strTitle = tblPetColours.strTitle;
                clsPetColour.bIsDeleted = tblPetColours.bIsDeleted;

                clsPetColour.lstHomePets = new List<clsHomePets>();

                if (tblPetColours.tblHomePets.Count > 0)
                {
                    foreach (var HomePetsItem in tblPetColours.tblHomePets)
                    {
                        clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetsItem);
                        clsPetColour.lstHomePets.Add(clsHomePet);
                    }
                }
            }

            return clsPetColour;
        }

        //Save
        public void savePetColour(clsPetColours clsPetColour)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblPetColours tblPetColours = new tblPetColours();

                tblPetColours.iPetColourID = clsPetColour.iPetColourID;

                tblPetColours.strTitle = clsPetColour.strTitle;
                tblPetColours.bIsDeleted = clsPetColour.bIsDeleted;

                //Add
                if (tblPetColours.iPetColourID == 0)
                {
                    tblPetColours.dtAdded = DateTime.Now;
                    tblPetColours.iAddedBy = clsCMSUser.iCMSUserID;
                    tblPetColours.dtEdited = DateTime.Now;
                    tblPetColours.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblPetColours.Add(tblPetColours);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblPetColours.dtAdded = clsPetColour.dtAdded;
                    tblPetColours.iAddedBy = clsPetColour.iAddedBy;
                    tblPetColours.dtEdited = DateTime.Now;
                    tblPetColours.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblPetColours>().AddOrUpdate(tblPetColours);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removePetColourByID(int iPetColourID)
        {
            tblPetColours tblPetColour = db.tblPetColours.Find(iPetColourID);
            if (tblPetColour != null)
            {
                tblPetColour.bIsDeleted = true;
                db.Entry(tblPetColour).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfPetColourExists(int iPetColourID)
        {
            bool bPetColourExists = db.tblPetColours.Any(PetColour => PetColour.iPetColourID == iPetColourID && PetColour.bIsDeleted == false);
            return bPetColourExists;
        }

        //Convert database table to class
        public clsPetColours convertPetColoursTableToClass(tblPetColours tblPetColour)
        {
            clsPetColours clsPetColour = new clsPetColours();

            clsPetColour.iPetColourID = tblPetColour.iPetColourID;
            clsPetColour.dtAdded = tblPetColour.dtAdded;
            clsPetColour.iAddedBy = tblPetColour.iAddedBy;
            clsPetColour.dtEdited = tblPetColour.dtEdited;
            clsPetColour.iEditedBy = tblPetColour.iEditedBy;

            clsPetColour.strTitle = tblPetColour.strTitle;
            clsPetColour.bIsDeleted = tblPetColour.bIsDeleted;

            return clsPetColour;
        }
    }
}
