﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsPetSizesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsPetSizes> getAllPetSizesList()
        {
            List<clsPetSizes> lstPetSizes = new List<clsPetSizes>();
            var lstGetPetSizesList = db.tblPetSizes.Where(PetSize => PetSize.bIsDeleted == false).ToList();

            if (lstGetPetSizesList.Count > 0)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                foreach (var item in lstGetPetSizesList)
                {
                    clsPetSizes clsPetSize = new clsPetSizes();

                    clsPetSize.iPetSizeID = item.iPetSizeID;
                    clsPetSize.dtAdded = item.dtAdded;
                    clsPetSize.iAddedBy = item.iAddedBy;
                    clsPetSize.dtEdited = item.dtEdited;
                    clsPetSize.iEditedBy = item.iEditedBy;

                    clsPetSize.strTitle = item.strTitle;
                    clsPetSize.bIsDeleted = item.bIsDeleted;

                    clsPetSize.lstHomePets = new List<clsHomePets>();

                    if (item.tblHomePets.Count > 0)
                    {
                        foreach (var HomePetsItem in item.tblHomePets)
                        {
                            clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetsItem);
                            clsPetSize.lstHomePets.Add(clsHomePet);
                        }
                    }

                    lstPetSizes.Add(clsPetSize);
                }
            }

            return lstPetSizes;
        }

        //Get All Only
        public List<clsPetSizes> getAllPetSizesOnlyList()
        {
            List<clsPetSizes> lstPetSizes = new List<clsPetSizes>();
            var lstGetPetSizesList = db.tblPetSizes.Where(PetSize => PetSize.bIsDeleted == false).ToList();

            if (lstGetPetSizesList.Count > 0)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                foreach (var item in lstGetPetSizesList)
                {
                    clsPetSizes clsPetSize = new clsPetSizes();

                    clsPetSize.iPetSizeID = item.iPetSizeID;
                    clsPetSize.dtAdded = item.dtAdded;
                    clsPetSize.iAddedBy = item.iAddedBy;
                    clsPetSize.dtEdited = item.dtEdited;
                    clsPetSize.iEditedBy = item.iEditedBy;

                    clsPetSize.strTitle = item.strTitle;
                    clsPetSize.bIsDeleted = item.bIsDeleted;

                    clsPetSize.lstHomePets = new List<clsHomePets>();

                    lstPetSizes.Add(clsPetSize);
                }
            }

            return lstPetSizes;
        }

        //Get
        public clsPetSizes getPetSizeByID(int iPetSizeID)
        {
            clsPetSizes clsPetSize = null;
            tblPetSizes tblPetSizes = db.tblPetSizes.FirstOrDefault(PetSize => PetSize.iPetSizeID == iPetSizeID && PetSize.bIsDeleted == false);

            if (tblPetSizes != null)
            {
                //Home Pets Manager
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();

                clsPetSize = new clsPetSizes();

                clsPetSize.iPetSizeID = tblPetSizes.iPetSizeID;
                clsPetSize.dtAdded = tblPetSizes.dtAdded;
                clsPetSize.iAddedBy = tblPetSizes.iAddedBy;
                clsPetSize.dtEdited = tblPetSizes.dtEdited;
                clsPetSize.iEditedBy = tblPetSizes.iEditedBy;

                clsPetSize.strTitle = tblPetSizes.strTitle;
                clsPetSize.bIsDeleted = tblPetSizes.bIsDeleted;

                clsPetSize.lstHomePets = new List<clsHomePets>();

                if (tblPetSizes.tblHomePets.Count > 0)
                {
                    foreach (var HomePetsItem in tblPetSizes.tblHomePets)
                    {
                        clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetsItem);
                        clsPetSize.lstHomePets.Add(clsHomePet);
                    }
                }
            }

            return clsPetSize;
        }

        //Save
        public void savePetSize(clsPetSizes clsPetSize)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblPetSizes tblPetSizes = new tblPetSizes();

                tblPetSizes.iPetSizeID = clsPetSize.iPetSizeID;

                tblPetSizes.strTitle = clsPetSize.strTitle;
                tblPetSizes.bIsDeleted = clsPetSize.bIsDeleted;

                //Add
                if (tblPetSizes.iPetSizeID == 0)
                {
                    tblPetSizes.dtAdded = DateTime.Now;
                    tblPetSizes.iAddedBy = clsCMSUser.iCMSUserID;
                    tblPetSizes.dtEdited = DateTime.Now;
                    tblPetSizes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblPetSizes.Add(tblPetSizes);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblPetSizes.dtAdded = clsPetSize.dtAdded;
                    tblPetSizes.iAddedBy = clsPetSize.iAddedBy;
                    tblPetSizes.dtEdited = DateTime.Now;
                    tblPetSizes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblPetSizes>().AddOrUpdate(tblPetSizes);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removePetSizeByID(int iPetSizeID)
        {
            tblPetSizes tblPetSize = db.tblPetSizes.Find(iPetSizeID);
            if (tblPetSize != null)
            {
                tblPetSize.bIsDeleted = true;
                db.Entry(tblPetSize).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfPetSizeExists(int iPetSizeID)
        {
            bool bPetSizeExists = db.tblPetSizes.Any(PetSize => PetSize.iPetSizeID == iPetSizeID && PetSize.bIsDeleted == false);
            return bPetSizeExists;
        }

        //Convert database table to class
        public clsPetSizes convertPetSizesTableToClass(tblPetSizes tblPetSize)
        {
            clsPetSizes clsPetSize = new clsPetSizes();

            clsPetSize.iPetSizeID = tblPetSize.iPetSizeID;
            clsPetSize.dtAdded = tblPetSize.dtAdded;
            clsPetSize.iAddedBy = tblPetSize.iAddedBy;
            clsPetSize.dtEdited = tblPetSize.dtEdited;
            clsPetSize.iEditedBy = tblPetSize.iEditedBy;

            clsPetSize.strTitle = tblPetSize.strTitle;
            clsPetSize.bIsDeleted = tblPetSize.bIsDeleted;

            return clsPetSize;
        }
    }
}
