﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsMakesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsMakes> getAllMakesList()
        {
            List<clsMakes> lstMakes = new List<clsMakes>();
            var lstGetMakesList = db.tblMakes.Where(Make => Make.bIsDeleted == false).ToList();

            if (lstGetMakesList.Count > 0)
            {
                //Home Vehicles Manager
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();

                foreach (var item in lstGetMakesList)
                {
                    clsMakes clsMake = new clsMakes();

                    clsMake.iMakeID = item.iMakeID;
                    clsMake.dtAdded = item.dtAdded;
                    clsMake.iAddedBy = item.iAddedBy;
                    clsMake.dtEdited = item.dtEdited;
                    clsMake.iEditedBy = item.iEditedBy;

                    clsMake.strTitle = item.strTitle;
                    clsMake.bIsDeleted = item.bIsDeleted;

                    clsMake.lstHomeVehicles = new List<clsHomeVehicles>();

                    if (item.tblHomeVehicles.Count > 0)
                    {
                        foreach (var HomeVehiclesItem in item.tblHomeVehicles)
                        {
                            clsHomeVehicles clsHomeVehicle = clsHomeVehiclesManager.convertHomeVehiclesTableToClass(HomeVehiclesItem);
                            clsMake.lstHomeVehicles.Add(clsHomeVehicle);
                        }
                    }

                    lstMakes.Add(clsMake);
                }
            }

            return lstMakes;
        }

        //Get All Only
        public List<clsMakes> getAllMakesOnlyList()
        {
            List<clsMakes> lstMakes = new List<clsMakes>();
            var lstGetMakesList = db.tblMakes.Where(Make => Make.bIsDeleted == false).ToList();

            if (lstGetMakesList.Count > 0)
            {
                //Home Vehicles Manager
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();

                foreach (var item in lstGetMakesList)
                {
                    clsMakes clsMake = new clsMakes();

                    clsMake.iMakeID = item.iMakeID;
                    clsMake.dtAdded = item.dtAdded;
                    clsMake.iAddedBy = item.iAddedBy;
                    clsMake.dtEdited = item.dtEdited;
                    clsMake.iEditedBy = item.iEditedBy;

                    clsMake.strTitle = item.strTitle;
                    clsMake.bIsDeleted = item.bIsDeleted;

                    clsMake.lstHomeVehicles = new List<clsHomeVehicles>();

                    lstMakes.Add(clsMake);
                }
            }

            return lstMakes;
        }

        //Get
        public clsMakes getMakeByID(int iMakeID)
        {
            clsMakes clsMake = null;
            tblMakes tblMakes = db.tblMakes.FirstOrDefault(Make => Make.iMakeID == iMakeID && Make.bIsDeleted == false);

            if (tblMakes != null)
            {
                //Home Vehicles Manager
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();

                clsMake = new clsMakes();

                clsMake.iMakeID = tblMakes.iMakeID;
                clsMake.dtAdded = tblMakes.dtAdded;
                clsMake.iAddedBy = tblMakes.iAddedBy;
                clsMake.dtEdited = tblMakes.dtEdited;
                clsMake.iEditedBy = tblMakes.iEditedBy;

                clsMake.strTitle = tblMakes.strTitle;
                clsMake.bIsDeleted = tblMakes.bIsDeleted;

                clsMake.lstHomeVehicles = new List<clsHomeVehicles>();

                if (tblMakes.tblHomeVehicles.Count > 0)
                {
                    foreach (var HomeVehiclesItem in tblMakes.tblHomeVehicles)
                    {
                        clsHomeVehicles clsHomeVehicle = clsHomeVehiclesManager.convertHomeVehiclesTableToClass(HomeVehiclesItem);
                        clsMake.lstHomeVehicles.Add(clsHomeVehicle);
                    }
                }
            }

            return clsMake;
        }

        //Save
        public void saveMake(clsMakes clsMake)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblMakes tblMakes = new tblMakes();

                tblMakes.iMakeID = clsMake.iMakeID;

                tblMakes.strTitle = clsMake.strTitle;
                tblMakes.bIsDeleted = clsMake.bIsDeleted;

                //Add
                if (tblMakes.iMakeID == 0)
                {
                    tblMakes.dtAdded = DateTime.Now;
                    tblMakes.iAddedBy = clsCMSUser.iCMSUserID;
                    tblMakes.dtEdited = DateTime.Now;
                    tblMakes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblMakes.Add(tblMakes);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblMakes.dtAdded = clsMake.dtAdded;
                    tblMakes.iAddedBy = clsMake.iAddedBy;
                    tblMakes.dtEdited = DateTime.Now;
                    tblMakes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblMakes>().AddOrUpdate(tblMakes);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeMakeByID(int iMakeID)
        {
            tblMakes tblMake = db.tblMakes.Find(iMakeID);
            if (tblMake != null)
            {
                tblMake.bIsDeleted = true;
                db.Entry(tblMake).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfMakeExists(int iMakeID)
        {
            bool bMakeExists = db.tblMakes.Any(Make => Make.iMakeID == iMakeID && Make.bIsDeleted == false);
            return bMakeExists;
        }

        //Convert database table to class
        public clsMakes convertMakesTableToClass(tblMakes tblMake)
        {
            clsMakes clsMake = new clsMakes();

            clsMake.iMakeID = tblMake.iMakeID;
            clsMake.dtAdded = tblMake.dtAdded;
            clsMake.iAddedBy = tblMake.iAddedBy;
            clsMake.dtEdited = tblMake.dtEdited;
            clsMake.iEditedBy = tblMake.iEditedBy;

            clsMake.strTitle = tblMake.strTitle;
            clsMake.bIsDeleted = tblMake.bIsDeleted;

            return clsMake;
        }
    }
}
