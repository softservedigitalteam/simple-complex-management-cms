﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsFormTicketStatusesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsFormTicketStatuses> getAllFormTicketStatusesList()
        {
            List<clsFormTicketStatuses> lstFormTicketStatuses = new List<clsFormTicketStatuses>();
            var lstGetFormTicketStatusesList = db.tblFormTicketStatuses.Where(FormTicketStatus => FormTicketStatus.bIsDeleted == false).ToList();

            if (lstGetFormTicketStatusesList.Count > 0)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                foreach (var item in lstGetFormTicketStatusesList)
                {
                    clsFormTicketStatuses clsFormTicketStatus = new clsFormTicketStatuses();

                    clsFormTicketStatus.iFormTicketStatusID = item.iFormTicketStatusID;
                    clsFormTicketStatus.dtAdded = item.dtAdded;
                    clsFormTicketStatus.iAddedBy = item.iAddedBy;
                    clsFormTicketStatus.dtEdited = item.dtEdited;
                    clsFormTicketStatus.iEditedBy = item.iEditedBy;

                    clsFormTicketStatus.strTitle = item.strTitle;
                    clsFormTicketStatus.bIsDeleted = item.bIsDeleted;

                    clsFormTicketStatus.lstFormTickets = new List<clsFormTickets>();

                    if (item.tblFormTickets.Count > 0)
                    {
                        foreach (var FormTicketsItem in item.tblFormTickets)
                        {
                            clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketsItem);
                            clsFormTicketStatus.lstFormTickets.Add(clsFormTicket);
                        }
                    }

                    lstFormTicketStatuses.Add(clsFormTicketStatus);
                }
            }

            return lstFormTicketStatuses;
        }

        //Get All Only
        public List<clsFormTicketStatuses> getAllFormTicketStatusesOnlyList()
        {
            List<clsFormTicketStatuses> lstFormTicketStatuses = new List<clsFormTicketStatuses>();
            var lstGetFormTicketStatusesList = db.tblFormTicketStatuses.Where(FormTicketStatus => FormTicketStatus.bIsDeleted == false).ToList();

            if (lstGetFormTicketStatusesList.Count > 0)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                foreach (var item in lstGetFormTicketStatusesList)
                {
                    clsFormTicketStatuses clsFormTicketStatus = new clsFormTicketStatuses();

                    clsFormTicketStatus.iFormTicketStatusID = item.iFormTicketStatusID;
                    clsFormTicketStatus.dtAdded = item.dtAdded;
                    clsFormTicketStatus.iAddedBy = item.iAddedBy;
                    clsFormTicketStatus.dtEdited = item.dtEdited;
                    clsFormTicketStatus.iEditedBy = item.iEditedBy;

                    clsFormTicketStatus.strTitle = item.strTitle;
                    clsFormTicketStatus.bIsDeleted = item.bIsDeleted;

                    clsFormTicketStatus.lstFormTickets = new List<clsFormTickets>();

                    lstFormTicketStatuses.Add(clsFormTicketStatus);
                }
            }

            return lstFormTicketStatuses;
        }

        //Get
        public clsFormTicketStatuses getFormTicketStatusByID(int iFormTicketStatusID)
        {
            clsFormTicketStatuses clsFormTicketStatus = null;
            tblFormTicketStatuses tblFormTicketStatuses = db.tblFormTicketStatuses.FirstOrDefault(FormTicketStatus => FormTicketStatus.iFormTicketStatusID == iFormTicketStatusID && FormTicketStatus.bIsDeleted == false);

            if (tblFormTicketStatuses != null)
            {
                //Form Tickets Manager
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();

                clsFormTicketStatus = new clsFormTicketStatuses();

                clsFormTicketStatus.iFormTicketStatusID = tblFormTicketStatuses.iFormTicketStatusID;
                clsFormTicketStatus.dtAdded = tblFormTicketStatuses.dtAdded;
                clsFormTicketStatus.iAddedBy = tblFormTicketStatuses.iAddedBy;
                clsFormTicketStatus.dtEdited = tblFormTicketStatuses.dtEdited;
                clsFormTicketStatus.iEditedBy = tblFormTicketStatuses.iEditedBy;

                clsFormTicketStatus.strTitle = tblFormTicketStatuses.strTitle;
                clsFormTicketStatus.bIsDeleted = tblFormTicketStatuses.bIsDeleted;

                clsFormTicketStatus.lstFormTickets = new List<clsFormTickets>();

                if (tblFormTicketStatuses.tblFormTickets.Count > 0)
                {
                    foreach (var FormTicketsItem in tblFormTicketStatuses.tblFormTickets)
                    {
                        clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketsItem);
                        clsFormTicketStatus.lstFormTickets.Add(clsFormTicket);
                    }
                }
            }

            return clsFormTicketStatus;
        }

        //Save
        public void saveFormTicketStatus(clsFormTicketStatuses clsFormTicketStatus)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblFormTicketStatuses tblFormTicketStatuses = new tblFormTicketStatuses();

                tblFormTicketStatuses.iFormTicketStatusID = clsFormTicketStatus.iFormTicketStatusID;

                tblFormTicketStatuses.strTitle = clsFormTicketStatus.strTitle;
                tblFormTicketStatuses.bIsDeleted = clsFormTicketStatus.bIsDeleted;

                //Add
                if (tblFormTicketStatuses.iFormTicketStatusID == 0)
                {
                    tblFormTicketStatuses.dtAdded = DateTime.Now;
                    tblFormTicketStatuses.iAddedBy = clsCMSUser.iCMSUserID;
                    tblFormTicketStatuses.dtEdited = DateTime.Now;
                    tblFormTicketStatuses.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblFormTicketStatuses.Add(tblFormTicketStatuses);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblFormTicketStatuses.dtAdded = clsFormTicketStatus.dtAdded;
                    tblFormTicketStatuses.iAddedBy = clsFormTicketStatus.iAddedBy;
                    tblFormTicketStatuses.dtEdited = DateTime.Now;
                    tblFormTicketStatuses.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblFormTicketStatuses>().AddOrUpdate(tblFormTicketStatuses);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeFormTicketStatusByID(int iFormTicketStatusID)
        {
            tblFormTicketStatuses tblFormTicketStatus = db.tblFormTicketStatuses.Find(iFormTicketStatusID);
            if (tblFormTicketStatus != null)
            {
                tblFormTicketStatus.bIsDeleted = true;
                db.Entry(tblFormTicketStatus).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfFormTicketStatusExists(int iFormTicketStatusID)
        {
            bool bFormTicketStatusExists = db.tblFormTicketStatuses.Any(FormTicketStatus => FormTicketStatus.iFormTicketStatusID == iFormTicketStatusID && FormTicketStatus.bIsDeleted == false);
            return bFormTicketStatusExists;
        }

        //Convert database table to class
        public clsFormTicketStatuses convertFormTicketStatusesTableToClass(tblFormTicketStatuses tblFormTicketStatus)
        {
            clsFormTicketStatuses clsFormTicketStatus = new clsFormTicketStatuses();

            clsFormTicketStatus.iFormTicketStatusID = tblFormTicketStatus.iFormTicketStatusID;
            clsFormTicketStatus.dtAdded = tblFormTicketStatus.dtAdded;
            clsFormTicketStatus.iAddedBy = tblFormTicketStatus.iAddedBy;
            clsFormTicketStatus.dtEdited = tblFormTicketStatus.dtEdited;
            clsFormTicketStatus.iEditedBy = tblFormTicketStatus.iEditedBy;

            clsFormTicketStatus.strTitle = tblFormTicketStatus.strTitle;
            clsFormTicketStatus.bIsDeleted = tblFormTicketStatus.bIsDeleted;

            return clsFormTicketStatus;
        }
    }
}
