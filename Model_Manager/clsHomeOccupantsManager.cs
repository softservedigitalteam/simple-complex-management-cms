﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomeOccupantsManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomeOccupants> getAllHomeOccupantsList()
        {
            List<clsHomeOccupants> lstHomeOccupants = new List<clsHomeOccupants>();
            var lstGetHomeOccupantsList = db.tblHomeOccupants.Where(HomeOccupant => HomeOccupant.bIsDeleted == false).ToList();

            if (lstGetHomeOccupantsList.Count > 0)
            {
                //Managers
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetHomeOccupantsList)
                {
                    clsHomeOccupants clsHomeOccupant = new clsHomeOccupants();

                    clsHomeOccupant.iHomeOccupantID = item.iHomeOccupantID;
                    clsHomeOccupant.dtAdded = item.dtAdded;
                    clsHomeOccupant.iAddedBy = item.iAddedBy;
                    clsHomeOccupant.dtEdited = item.dtEdited;
                    clsHomeOccupant.iEditedBy = item.iEditedBy;

                    clsHomeOccupant.iHomeDetailID = item.iHomeDetailID;
                    clsHomeOccupant.strFirstNames = item.strFirstNames;
                    clsHomeOccupant.strSurname = item.strSurname;
                    clsHomeOccupant.strMobileNumber = item.strMobileNumber;
                    clsHomeOccupant.strEmailAddress = item.strEmailAddress;

                    clsHomeOccupant.strIDNumber = item.strIDNumber;
                    clsHomeOccupant.strPassportNumber = item.strPassportNumber;
                    clsHomeOccupant.dtDateOfBirth = item.dtDateOfBirth;
                    clsHomeOccupant.strPathToImages = item.strPathToImages;
                    clsHomeOccupant.strMasterImage = item.strMasterImage;

                    clsHomeOccupant.strIndustryOrProfession = item.strIndustryOrProfession;
                    clsHomeOccupant.bIsDeleted = item.bIsDeleted;

                    clsHomeOccupant.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();

                    if (item.tblHomeHistoryLogs.Count > 0)
                    {
                        foreach (var HomeHistoryLogsItem in item.tblHomeHistoryLogs)
                        {
                            clsHomeHistoryLogs clsHomeHistoryLogs = clsHomeHistoryLogsManager.convertHomeHistoryLogsTableToClass(HomeHistoryLogsItem);
                            clsHomeOccupant.lstHomeHistoryLogs.Add(clsHomeHistoryLogs);
                        }
                    }
                    if (item.tblHomeDetails != null)
                        clsHomeOccupant.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(item.tblHomeDetails);

                    lstHomeOccupants.Add(clsHomeOccupant);
                }
            }

            return lstHomeOccupants;
        }

        //Get All Only
        public List<clsHomeOccupants> getAllHomeOccupantsOnlyList()
        {
            List<clsHomeOccupants> lstHomeOccupants = new List<clsHomeOccupants>();
            var lstGetHomeOccupantsList = db.tblHomeOccupants.Where(HomeOccupant => HomeOccupant.bIsDeleted == false).ToList();

            if (lstGetHomeOccupantsList.Count > 0)
            {
                //Managers
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetHomeOccupantsList)
                {
                    clsHomeOccupants clsHomeOccupant = new clsHomeOccupants();

                    clsHomeOccupant.iHomeOccupantID = item.iHomeOccupantID;
                    clsHomeOccupant.dtAdded = item.dtAdded;
                    clsHomeOccupant.iAddedBy = item.iAddedBy;
                    clsHomeOccupant.dtEdited = item.dtEdited;
                    clsHomeOccupant.iEditedBy = item.iEditedBy;

                    clsHomeOccupant.iHomeDetailID = item.iHomeDetailID;
                    clsHomeOccupant.strFirstNames = item.strFirstNames;
                    clsHomeOccupant.strSurname = item.strSurname;
                    clsHomeOccupant.strMobileNumber = item.strMobileNumber;
                    clsHomeOccupant.strEmailAddress = item.strEmailAddress;

                    clsHomeOccupant.strIDNumber = item.strIDNumber;
                    clsHomeOccupant.strPassportNumber = item.strPassportNumber;
                    clsHomeOccupant.dtDateOfBirth = item.dtDateOfBirth;
                    clsHomeOccupant.strPathToImages = item.strPathToImages;
                    clsHomeOccupant.strMasterImage = item.strMasterImage;

                    clsHomeOccupant.strIndustryOrProfession = item.strIndustryOrProfession;
                    clsHomeOccupant.bIsDeleted = item.bIsDeleted;

                    clsHomeOccupant.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();

                    lstHomeOccupants.Add(clsHomeOccupant);
                }
            }

            return lstHomeOccupants;
        }

        //Get
        public clsHomeOccupants getHomeOccupantByID(int iHomeOccupantID)
        {
            clsHomeOccupants clsHomeOccupant = null;
            tblHomeOccupants tblHomeOccupants = db.tblHomeOccupants.FirstOrDefault(HomeOccupant => HomeOccupant.iHomeOccupantID == iHomeOccupantID && HomeOccupant.bIsDeleted == false);

            if (tblHomeOccupants != null)
            {
                //Managers
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                clsHomeOccupant = new clsHomeOccupants();

                clsHomeOccupant.iHomeOccupantID = tblHomeOccupants.iHomeOccupantID;
                clsHomeOccupant.dtAdded = tblHomeOccupants.dtAdded;
                clsHomeOccupant.iAddedBy = tblHomeOccupants.iAddedBy;
                clsHomeOccupant.dtEdited = tblHomeOccupants.dtEdited;
                clsHomeOccupant.iEditedBy = tblHomeOccupants.iEditedBy;

                clsHomeOccupant.iHomeDetailID = tblHomeOccupants.iHomeDetailID;
                clsHomeOccupant.strFirstNames = tblHomeOccupants.strFirstNames;
                clsHomeOccupant.strSurname = tblHomeOccupants.strSurname;
                clsHomeOccupant.strMobileNumber = tblHomeOccupants.strMobileNumber;
                clsHomeOccupant.strEmailAddress = tblHomeOccupants.strEmailAddress;

                clsHomeOccupant.strIDNumber = tblHomeOccupants.strIDNumber;
                clsHomeOccupant.strPassportNumber = tblHomeOccupants.strPassportNumber;
                clsHomeOccupant.dtDateOfBirth = tblHomeOccupants.dtDateOfBirth;
                clsHomeOccupant.strPathToImages = tblHomeOccupants.strPathToImages;
                clsHomeOccupant.strMasterImage = tblHomeOccupants.strMasterImage;

                clsHomeOccupant.strIndustryOrProfession = tblHomeOccupants.strIndustryOrProfession;
                clsHomeOccupant.bIsDeleted = tblHomeOccupants.bIsDeleted;

                clsHomeOccupant.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();

                if (tblHomeOccupants.tblHomeHistoryLogs.Count > 0)
                {
                    foreach (var HomeHistoryLogstblHomeOccupants in tblHomeOccupants.tblHomeHistoryLogs)
                    {
                        clsHomeHistoryLogs clsHomeHistoryLogs = clsHomeHistoryLogsManager.convertHomeHistoryLogsTableToClass(HomeHistoryLogstblHomeOccupants);
                        clsHomeOccupant.lstHomeHistoryLogs.Add(clsHomeHistoryLogs);
                    }
                }
                if (tblHomeOccupants.tblHomeDetails != null)
                    clsHomeOccupant.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(tblHomeOccupants.tblHomeDetails);
            }

            return clsHomeOccupant;
        }

        //Save
        public void saveHomeOccupant(clsHomeOccupants clsHomeOccupant)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomeOccupants tblHomeOccupants = new tblHomeOccupants();

                tblHomeOccupants.iHomeOccupantID = clsHomeOccupant.iHomeOccupantID;

                tblHomeOccupants.iHomeDetailID = clsHomeOccupant.iHomeDetailID;
                tblHomeOccupants.strFirstNames = clsHomeOccupant.strFirstNames;
                tblHomeOccupants.strSurname = clsHomeOccupant.strSurname;
                tblHomeOccupants.strMobileNumber = clsHomeOccupant.strMobileNumber;
                tblHomeOccupants.strEmailAddress = clsHomeOccupant.strEmailAddress;

                tblHomeOccupants.strIDNumber = clsHomeOccupant.strIDNumber;
                tblHomeOccupants.strPassportNumber = clsHomeOccupant.strPassportNumber;
                tblHomeOccupants.dtDateOfBirth = clsHomeOccupant.dtDateOfBirth;
                tblHomeOccupants.strPathToImages = clsHomeOccupant.strPathToImages;
                tblHomeOccupants.strMasterImage = clsHomeOccupant.strMasterImage;

                tblHomeOccupants.strIndustryOrProfession = clsHomeOccupant.strIndustryOrProfession;
                tblHomeOccupants.bIsDeleted = clsHomeOccupant.bIsDeleted;

                //Add
                if (tblHomeOccupants.iHomeOccupantID == 0)
                {
                    tblHomeOccupants.dtAdded = DateTime.Now;
                    tblHomeOccupants.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomeOccupants.dtEdited = DateTime.Now;
                    tblHomeOccupants.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomeOccupants.Add(tblHomeOccupants);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomeOccupants.dtAdded = clsHomeOccupant.dtAdded;
                    tblHomeOccupants.iAddedBy = clsHomeOccupant.iAddedBy;
                    tblHomeOccupants.dtEdited = DateTime.Now;
                    tblHomeOccupants.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomeOccupants>().AddOrUpdate(tblHomeOccupants);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomeOccupantByID(int iHomeOccupantID)
        {
            tblHomeOccupants tblHomeOccupant = db.tblHomeOccupants.Find(iHomeOccupantID);
            if (tblHomeOccupant != null)
            {
                tblHomeOccupant.bIsDeleted = true;
                db.Entry(tblHomeOccupant).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomeOccupantExists(int iHomeOccupantID)
        {
            bool bHomeOccupantExists = db.tblHomeOccupants.Any(HomeOccupant => HomeOccupant.iHomeOccupantID == iHomeOccupantID && HomeOccupant.bIsDeleted == false);
            return bHomeOccupantExists;
        }

        //Convert database table to class
        public clsHomeOccupants convertHomeOccupantsTableToClass(tblHomeOccupants tblHomeOccupant)
        {
            clsHomeOccupants clsHomeOccupant = new clsHomeOccupants();

            clsHomeOccupant.iHomeOccupantID = tblHomeOccupant.iHomeOccupantID;
            clsHomeOccupant.dtAdded = tblHomeOccupant.dtAdded;
            clsHomeOccupant.iAddedBy = tblHomeOccupant.iAddedBy;
            clsHomeOccupant.dtEdited = tblHomeOccupant.dtEdited;
            clsHomeOccupant.iEditedBy = tblHomeOccupant.iEditedBy;

            clsHomeOccupant.iHomeDetailID = tblHomeOccupant.iHomeDetailID;
            clsHomeOccupant.strFirstNames = tblHomeOccupant.strFirstNames;
            clsHomeOccupant.strSurname = tblHomeOccupant.strSurname;
            clsHomeOccupant.strMobileNumber = tblHomeOccupant.strMobileNumber;
            clsHomeOccupant.strEmailAddress = tblHomeOccupant.strEmailAddress;

            clsHomeOccupant.strIDNumber = tblHomeOccupant.strIDNumber;
            clsHomeOccupant.strPassportNumber = tblHomeOccupant.strPassportNumber;
            clsHomeOccupant.dtDateOfBirth = tblHomeOccupant.dtDateOfBirth;
            clsHomeOccupant.strPathToImages = tblHomeOccupant.strPathToImages;
            clsHomeOccupant.strMasterImage = tblHomeOccupant.strMasterImage;

            clsHomeOccupant.strIndustryOrProfession = tblHomeOccupant.strIndustryOrProfession;
            clsHomeOccupant.bIsDeleted = tblHomeOccupant.bIsDeleted;

            return clsHomeOccupant;
        }
    }
}
