﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomeOwnerDocumentsManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomeOwnerDocuments> getAllHomeOwnerDocumentsList()
        {
            List<clsHomeOwnerDocuments> lstHomeOwnerDocuments = new List<clsHomeOwnerDocuments>();
            var lstGetHomeOwnerDocumentsList = db.tblHomeOwnerDocuments.Where(HomeOwnerDocument => HomeOwnerDocument.bIsDeleted == false).ToList();

            if (lstGetHomeOwnerDocumentsList.Count > 0)
            {
                //Managers
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();

                foreach (var item in lstGetHomeOwnerDocumentsList)
                {
                    clsHomeOwnerDocuments clsHomeOwnerDocument = new clsHomeOwnerDocuments();

                    clsHomeOwnerDocument.iHomeOwnerDocumentID = item.iHomeOwnerDocumentID;
                    clsHomeOwnerDocument.dtAdded = item.dtAdded;
                    clsHomeOwnerDocument.iAddedBy = item.iAddedBy;
                    clsHomeOwnerDocument.dtEdited = item.dtEdited;
                    clsHomeOwnerDocument.iEditedBy = item.iEditedBy;

                    clsHomeOwnerDocument.strTitle = item.strTitle;
                    clsHomeOwnerDocument.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeOwnerDocument.bIsDeleted = item.bIsDeleted;

                    if (item.tblHomeOwners != null)
                        clsHomeOwnerDocument.clsHomeOwner = clsHomeOwnersManager.convertHomeOwnersTableToClass(item.tblHomeOwners);

                    lstHomeOwnerDocuments.Add(clsHomeOwnerDocument);
                }
            }

            return lstHomeOwnerDocuments;
        }

        //Get All Only
        public List<clsHomeOwnerDocuments> getAllHomeOwnerDocumentsOnlyList()
        {
            List<clsHomeOwnerDocuments> lstHomeOwnerDocuments = new List<clsHomeOwnerDocuments>();
            var lstGetHomeOwnerDocumentsList = db.tblHomeOwnerDocuments.Where(HomeOwnerDocument => HomeOwnerDocument.bIsDeleted == false).ToList();

            if (lstGetHomeOwnerDocumentsList.Count > 0)
            {
                //Managers
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();

                foreach (var item in lstGetHomeOwnerDocumentsList)
                {
                    clsHomeOwnerDocuments clsHomeOwnerDocument = new clsHomeOwnerDocuments();

                    clsHomeOwnerDocument.iHomeOwnerDocumentID = item.iHomeOwnerDocumentID;
                    clsHomeOwnerDocument.dtAdded = item.dtAdded;
                    clsHomeOwnerDocument.iAddedBy = item.iAddedBy;
                    clsHomeOwnerDocument.dtEdited = item.dtEdited;
                    clsHomeOwnerDocument.iEditedBy = item.iEditedBy;

                    clsHomeOwnerDocument.strTitle = item.strTitle;
                    clsHomeOwnerDocument.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeOwnerDocument.bIsDeleted = item.bIsDeleted;

                    lstHomeOwnerDocuments.Add(clsHomeOwnerDocument);
                }
            }

            return lstHomeOwnerDocuments;
        }

        //Get
        public clsHomeOwnerDocuments getHomeOwnerDocumentByID(int iHomeOwnerDocumentID)
        {
            clsHomeOwnerDocuments clsHomeOwnerDocument = null;
            tblHomeOwnerDocuments tblHomeOwnerDocuments = db.tblHomeOwnerDocuments.FirstOrDefault(HomeOwnerDocument => HomeOwnerDocument.iHomeOwnerDocumentID == iHomeOwnerDocumentID && HomeOwnerDocument.bIsDeleted == false);

            if (tblHomeOwnerDocuments != null)
            {
                //Managers
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();

                clsHomeOwnerDocument = new clsHomeOwnerDocuments();

                clsHomeOwnerDocument.iHomeOwnerDocumentID = tblHomeOwnerDocuments.iHomeOwnerDocumentID;
                clsHomeOwnerDocument.dtAdded = tblHomeOwnerDocuments.dtAdded;
                clsHomeOwnerDocument.iAddedBy = tblHomeOwnerDocuments.iAddedBy;
                clsHomeOwnerDocument.dtEdited = tblHomeOwnerDocuments.dtEdited;
                clsHomeOwnerDocument.iEditedBy = tblHomeOwnerDocuments.iEditedBy;

                clsHomeOwnerDocument.strTitle = tblHomeOwnerDocuments.strTitle;
                clsHomeOwnerDocument.iHomeOwnerID = tblHomeOwnerDocuments.iHomeOwnerID;
                clsHomeOwnerDocument.bIsDeleted = tblHomeOwnerDocuments.bIsDeleted;

                if (tblHomeOwnerDocuments.tblHomeOwners != null)
                    clsHomeOwnerDocument.clsHomeOwner = clsHomeOwnersManager.convertHomeOwnersTableToClass(tblHomeOwnerDocuments.tblHomeOwners);
            }

            return clsHomeOwnerDocument;
        }

        //Save
        public void saveHomeOwnerDocument(clsHomeOwnerDocuments clsHomeOwnerDocument)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomeOwnerDocuments tblHomeOwnerDocuments = new tblHomeOwnerDocuments();

                tblHomeOwnerDocuments.iHomeOwnerDocumentID = clsHomeOwnerDocument.iHomeOwnerDocumentID;

                tblHomeOwnerDocuments.strTitle = clsHomeOwnerDocument.strTitle;
                tblHomeOwnerDocuments.iHomeOwnerID = clsHomeOwnerDocument.iHomeOwnerID;
                tblHomeOwnerDocuments.bIsDeleted = clsHomeOwnerDocument.bIsDeleted;

                //Add
                if (tblHomeOwnerDocuments.iHomeOwnerDocumentID == 0)
                {
                    tblHomeOwnerDocuments.dtAdded = DateTime.Now;
                    tblHomeOwnerDocuments.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomeOwnerDocuments.dtEdited = DateTime.Now;
                    tblHomeOwnerDocuments.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomeOwnerDocuments.Add(tblHomeOwnerDocuments);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomeOwnerDocuments.dtAdded = clsHomeOwnerDocument.dtAdded;
                    tblHomeOwnerDocuments.iAddedBy = clsHomeOwnerDocument.iAddedBy;
                    tblHomeOwnerDocuments.dtEdited = DateTime.Now;
                    tblHomeOwnerDocuments.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomeOwnerDocuments>().AddOrUpdate(tblHomeOwnerDocuments);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomeOwnerDocumentByID(int iHomeOwnerDocumentID)
        {
            tblHomeOwnerDocuments tblHomeOwnerDocument = db.tblHomeOwnerDocuments.Find(iHomeOwnerDocumentID);
            if (tblHomeOwnerDocument != null)
            {
                tblHomeOwnerDocument.bIsDeleted = true;
                db.Entry(tblHomeOwnerDocument).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomeOwnerDocumentExists(int iHomeOwnerDocumentID)
        {
            bool bHomeOwnerDocumentExists = db.tblHomeOwnerDocuments.Any(HomeOwnerDocument => HomeOwnerDocument.iHomeOwnerDocumentID == iHomeOwnerDocumentID && HomeOwnerDocument.bIsDeleted == false);
            return bHomeOwnerDocumentExists;
        }

        //Convert database table to class
        public clsHomeOwnerDocuments convertHomeOwnerDocumentsTableToClass(tblHomeOwnerDocuments tblHomeOwnerDocument)
        {
            clsHomeOwnerDocuments clsHomeOwnerDocument = new clsHomeOwnerDocuments();

            clsHomeOwnerDocument.iHomeOwnerDocumentID = tblHomeOwnerDocument.iHomeOwnerDocumentID;
            clsHomeOwnerDocument.dtAdded = tblHomeOwnerDocument.dtAdded;
            clsHomeOwnerDocument.iAddedBy = tblHomeOwnerDocument.iAddedBy;
            clsHomeOwnerDocument.dtEdited = tblHomeOwnerDocument.dtEdited;
            clsHomeOwnerDocument.iEditedBy = tblHomeOwnerDocument.iEditedBy;

            clsHomeOwnerDocument.strTitle = tblHomeOwnerDocument.strTitle;
            clsHomeOwnerDocument.iHomeOwnerID = tblHomeOwnerDocument.iHomeOwnerID;
            clsHomeOwnerDocument.bIsDeleted = tblHomeOwnerDocument.bIsDeleted;

            return clsHomeOwnerDocument;
        }
    }
}
