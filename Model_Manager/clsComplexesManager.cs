﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsComplexesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsComplexes> getAllComplexesList()
        {
            List<clsComplexes> lstComplexes = new List<clsComplexes>();
            var lstGetComplexesList = db.tblComplexes.Where(Complex => Complex.bIsDeleted == false).ToList();

            if (lstGetComplexesList.Count > 0)
            {
                //Complex Types Manager
                clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();

                foreach (var item in lstGetComplexesList)
                {
                    clsComplexes clsComplex = new clsComplexes();

                    clsComplex.iComplexID = item.iComplexID;
                    clsComplex.dtAdded = item.dtAdded;
                    clsComplex.iAddedBy = item.iAddedBy;
                    clsComplex.dtEdited = item.dtEdited;
                    clsComplex.iEditedBy = item.iEditedBy;

                    clsComplex.strLatitude = item.strLatitude;
                    clsComplex.strLongitude = item.strLongitude;
                    clsComplex.strComplexName = item.strComplexName;
                    clsComplex.strComplexPhysicalAddress = item.strComplexPhysicalAddress;
                    clsComplex.strMangementCompanyName = item.strMangementCompanyName;

                    clsComplex.iNumberOfHomes = item.iNumberOfHomes;
                    clsComplex.strPathToImages = item.strPathToImages;
                    clsComplex.strMasterImage = item.strMasterImage;
                    clsComplex.iComplexTypeID = item.iComplexTypeID;
                    clsComplex.bIsDeleted = item.bIsDeleted;

                    if (item.tblComplexTypes != null)
                        clsComplex.clsComplexType = clsComplexTypesManager.convertComplexTypesTableToClass(item.tblComplexTypes);

                    lstComplexes.Add(clsComplex);
                }
            }

            return lstComplexes;
        }

        //Get All Only
        public List<clsComplexes> getAllComplexesOnlyList()
        {
            List<clsComplexes> lstComplexes = new List<clsComplexes>();
            var lstGetComplexesList = db.tblComplexes.Where(Complex => Complex.bIsDeleted == false).ToList();

            if (lstGetComplexesList.Count > 0)
            {
                //Complex Types Manager
                clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();

                foreach (var item in lstGetComplexesList)
                {
                    clsComplexes clsComplex = new clsComplexes();

                    clsComplex.iComplexID = item.iComplexID;
                    clsComplex.dtAdded = item.dtAdded;
                    clsComplex.iAddedBy = item.iAddedBy;
                    clsComplex.dtEdited = item.dtEdited;
                    clsComplex.iEditedBy = item.iEditedBy;

                    clsComplex.strLatitude = item.strLatitude;
                    clsComplex.strLongitude = item.strLongitude;
                    clsComplex.strComplexName = item.strComplexName;
                    clsComplex.strComplexPhysicalAddress = item.strComplexPhysicalAddress;
                    clsComplex.strMangementCompanyName = item.strMangementCompanyName;

                    clsComplex.iNumberOfHomes = item.iNumberOfHomes;
                    clsComplex.strPathToImages = item.strPathToImages;
                    clsComplex.strMasterImage = item.strMasterImage;
                    clsComplex.iComplexTypeID = item.iComplexTypeID;
                    clsComplex.bIsDeleted = item.bIsDeleted;

                    lstComplexes.Add(clsComplex);
                }
            }

            return lstComplexes;
        }

        //Get
        public clsComplexes getComplexByID(int iComplexID)
        {
            clsComplexes clsComplex = null;
            tblComplexes tblComplexes = db.tblComplexes.FirstOrDefault(Complex => Complex.iComplexID == iComplexID && Complex.bIsDeleted == false);

            if (tblComplexes != null)
            {
                //Complex Types Manager
                clsComplexTypesManager clsComplexTypesManager = new clsComplexTypesManager();

                clsComplex = new clsComplexes();

                clsComplex.iComplexID = tblComplexes.iComplexID;
                clsComplex.dtAdded = tblComplexes.dtAdded;
                clsComplex.iAddedBy = tblComplexes.iAddedBy;
                clsComplex.dtEdited = tblComplexes.dtEdited;
                clsComplex.iEditedBy = tblComplexes.iEditedBy;

                clsComplex.strLatitude = tblComplexes.strLatitude;
                clsComplex.strLongitude = tblComplexes.strLongitude;
                clsComplex.strComplexName = tblComplexes.strComplexName;
                clsComplex.strComplexPhysicalAddress = tblComplexes.strComplexPhysicalAddress;
                clsComplex.strMangementCompanyName = tblComplexes.strMangementCompanyName;

                clsComplex.iNumberOfHomes = tblComplexes.iNumberOfHomes;
                clsComplex.strPathToImages = tblComplexes.strPathToImages;
                clsComplex.strMasterImage = tblComplexes.strMasterImage;
                clsComplex.iComplexTypeID = tblComplexes.iComplexTypeID;
                clsComplex.bIsDeleted = tblComplexes.bIsDeleted;

                if (tblComplexes.tblComplexTypes != null)
                    clsComplex.clsComplexType = clsComplexTypesManager.convertComplexTypesTableToClass(tblComplexes.tblComplexTypes);
            }

            return clsComplex;
        }

        //Save
        public void saveComplex(clsComplexes clsComplex)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblComplexes tblComplexes = new tblComplexes();

                tblComplexes.iComplexID = clsComplex.iComplexID;

                tblComplexes.strLatitude = clsComplex.strLatitude;
                tblComplexes.strLongitude = clsComplex.strLongitude;
                tblComplexes.strComplexName = clsComplex.strComplexName;
                tblComplexes.strComplexPhysicalAddress = clsComplex.strComplexPhysicalAddress;
                tblComplexes.strMangementCompanyName = clsComplex.strMangementCompanyName;

                tblComplexes.iNumberOfHomes = clsComplex.iNumberOfHomes;
                tblComplexes.strPathToImages = clsComplex.strPathToImages;
                tblComplexes.strMasterImage = clsComplex.strMasterImage;
                tblComplexes.iComplexTypeID = clsComplex.iComplexTypeID;
                tblComplexes.bIsDeleted = clsComplex.bIsDeleted;

                //Add
                if (tblComplexes.iComplexID == 0)
                {
                    tblComplexes.dtAdded = DateTime.Now;
                    tblComplexes.iAddedBy = clsCMSUser.iCMSUserID;
                    tblComplexes.dtEdited = DateTime.Now;
                    tblComplexes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblComplexes.Add(tblComplexes);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblComplexes.dtAdded = clsComplex.dtAdded;
                    tblComplexes.iAddedBy = clsComplex.iAddedBy;
                    tblComplexes.dtEdited = DateTime.Now;
                    tblComplexes.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblComplexes>().AddOrUpdate(tblComplexes);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeComplexByID(int iComplexID)
        {
            tblComplexes tblComplex = db.tblComplexes.Find(iComplexID);
            if (tblComplex != null)
            {
                tblComplex.bIsDeleted = true;
                db.Entry(tblComplex).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfComplexExists(int iComplexID)
        {
            bool bComplexExists = db.tblComplexes.Any(Complex => Complex.iComplexID == iComplexID && Complex.bIsDeleted == false);
            return bComplexExists;
        }

        //Convert database table to class
        public clsComplexes convertComplexesTableToClass(tblComplexes tblComplex)
        {
            clsComplexes clsComplex = new clsComplexes();

            clsComplex.iComplexID = tblComplex.iComplexID;
            clsComplex.dtAdded = tblComplex.dtAdded;
            clsComplex.iAddedBy = tblComplex.iAddedBy;
            clsComplex.dtEdited = tblComplex.dtEdited;
            clsComplex.iEditedBy = tblComplex.iEditedBy;

            clsComplex.strLatitude = tblComplex.strLatitude;
            clsComplex.strLongitude = tblComplex.strLongitude;
            clsComplex.strComplexName = tblComplex.strComplexName;
            clsComplex.strComplexPhysicalAddress = tblComplex.strComplexPhysicalAddress;
            clsComplex.strMangementCompanyName = tblComplex.strMangementCompanyName;

            clsComplex.iNumberOfHomes = tblComplex.iNumberOfHomes;
            clsComplex.strPathToImages = tblComplex.strPathToImages;
            clsComplex.strMasterImage = tblComplex.strMasterImage;
            clsComplex.iComplexTypeID = tblComplex.iComplexTypeID;
            clsComplex.bIsDeleted = tblComplex.bIsDeleted;

            return clsComplex;
        }
    }
}
