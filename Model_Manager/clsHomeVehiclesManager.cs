﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomeVehiclesManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomeVehicles> getAllHomeVehiclesList()
        {
            List<clsHomeVehicles> lstHomeVehicles = new List<clsHomeVehicles>();
            var lstGetHomeVehiclesList = db.tblHomeVehicles.Where(HomeVehicle => HomeVehicle.bIsDeleted == false).ToList();

            if (lstGetHomeVehiclesList.Count > 0)
            {
                //Managers
                clsColoursManager clsColoursManager = new clsColoursManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsMakesManager clsMakesManager = new clsMakesManager();

                foreach (var item in lstGetHomeVehiclesList)
                {
                    clsHomeVehicles clsHomeVehicle = new clsHomeVehicles();

                    clsHomeVehicle.iHomeVehicleID = item.iHomeVehicleID;
                    clsHomeVehicle.dtAdded = item.dtAdded;
                    clsHomeVehicle.iAddedBy = item.iAddedBy;
                    clsHomeVehicle.dtEdited = item.dtEdited;
                    clsHomeVehicle.iEditedBy = item.iEditedBy;

                    clsHomeVehicle.iHomeDetailID = item.iHomeDetailID;
                    clsHomeVehicle.iMakeID = item.iMakeID;
                    clsHomeVehicle.strModel = item.strModel;
                    clsHomeVehicle.iColourID = item.iColourID;
                    clsHomeVehicle.strRegistrationNumber = item.strRegistrationNumber;

                    clsHomeVehicle.strPathToImages = item.strPathToImages;
                    clsHomeVehicle.strMasterImage = item.strMasterImage;
                    clsHomeVehicle.bIsDeleted = item.bIsDeleted;

                    if (item.tblColours != null)
                        clsHomeVehicle.clsColour = clsColoursManager.convertColoursTableToClass(item.tblColours);
                    if (item.tblHomeDetails != null)
                        clsHomeVehicle.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(item.tblHomeDetails);
                    if (item.tblMakes != null)
                        clsHomeVehicle.clsMake = clsMakesManager.convertMakesTableToClass(item.tblMakes);

                    lstHomeVehicles.Add(clsHomeVehicle);
                }
            }

            return lstHomeVehicles;
        }

        //Get All Only
        public List<clsHomeVehicles> getAllHomeVehiclesOnlyList()
        {
            List<clsHomeVehicles> lstHomeVehicles = new List<clsHomeVehicles>();
            var lstGetHomeVehiclesList = db.tblHomeVehicles.Where(HomeVehicle => HomeVehicle.bIsDeleted == false).ToList();

            if (lstGetHomeVehiclesList.Count > 0)
            {
                //Managers
                clsColoursManager clsColoursManager = new clsColoursManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsMakesManager clsMakesManager = new clsMakesManager();

                foreach (var item in lstGetHomeVehiclesList)
                {
                    clsHomeVehicles clsHomeVehicle = new clsHomeVehicles();

                    clsHomeVehicle.iHomeVehicleID = item.iHomeVehicleID;
                    clsHomeVehicle.dtAdded = item.dtAdded;
                    clsHomeVehicle.iAddedBy = item.iAddedBy;
                    clsHomeVehicle.dtEdited = item.dtEdited;
                    clsHomeVehicle.iEditedBy = item.iEditedBy;

                    clsHomeVehicle.iHomeDetailID = item.iHomeDetailID;
                    clsHomeVehicle.iMakeID = item.iMakeID;
                    clsHomeVehicle.strModel = item.strModel;
                    clsHomeVehicle.iColourID = item.iColourID;
                    clsHomeVehicle.strRegistrationNumber = item.strRegistrationNumber;

                    clsHomeVehicle.strPathToImages = item.strPathToImages;
                    clsHomeVehicle.strMasterImage = item.strMasterImage;
                    clsHomeVehicle.bIsDeleted = item.bIsDeleted;

                    lstHomeVehicles.Add(clsHomeVehicle);
                }
            }

            return lstHomeVehicles;
        }

        //Get
        public clsHomeVehicles getHomeVehicleByID(int iHomeVehicleID)
        {
            clsHomeVehicles clsHomeVehicle = null;
            tblHomeVehicles tblHomeVehicles = db.tblHomeVehicles.FirstOrDefault(HomeVehicle => HomeVehicle.iHomeVehicleID == iHomeVehicleID && HomeVehicle.bIsDeleted == false);

            if (tblHomeVehicles != null)
            {
                //Managers
                clsColoursManager clsColoursManager = new clsColoursManager();
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();
                clsMakesManager clsMakesManager = new clsMakesManager();

                clsHomeVehicle = new clsHomeVehicles();

                clsHomeVehicle.iHomeVehicleID = tblHomeVehicles.iHomeVehicleID;
                clsHomeVehicle.dtAdded = tblHomeVehicles.dtAdded;
                clsHomeVehicle.iAddedBy = tblHomeVehicles.iAddedBy;
                clsHomeVehicle.dtEdited = tblHomeVehicles.dtEdited;
                clsHomeVehicle.iEditedBy = tblHomeVehicles.iEditedBy;

                clsHomeVehicle.iHomeDetailID = tblHomeVehicles.iHomeDetailID;
                clsHomeVehicle.iMakeID = tblHomeVehicles.iMakeID;
                clsHomeVehicle.strModel = tblHomeVehicles.strModel;
                clsHomeVehicle.iColourID = tblHomeVehicles.iColourID;
                clsHomeVehicle.strRegistrationNumber = tblHomeVehicles.strRegistrationNumber;

                clsHomeVehicle.strPathToImages = tblHomeVehicles.strPathToImages;
                clsHomeVehicle.strMasterImage = tblHomeVehicles.strMasterImage;
                clsHomeVehicle.bIsDeleted = tblHomeVehicles.bIsDeleted;

                if (tblHomeVehicles.tblColours != null)
                    clsHomeVehicle.clsColour = clsColoursManager.convertColoursTableToClass(tblHomeVehicles.tblColours);
                if (tblHomeVehicles.tblHomeDetails != null)
                    clsHomeVehicle.clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(tblHomeVehicles.tblHomeDetails);
                if (tblHomeVehicles.tblMakes != null)
                    clsHomeVehicle.clsMake = clsMakesManager.convertMakesTableToClass(tblHomeVehicles.tblMakes);
            }

            return clsHomeVehicle;
        }

        //Save
        public void saveHomeVehicle(clsHomeVehicles clsHomeVehicle)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomeVehicles tblHomeVehicles = new tblHomeVehicles();

                tblHomeVehicles.iHomeVehicleID = clsHomeVehicle.iHomeVehicleID;

                clsHomeVehicle.iHomeDetailID = tblHomeVehicles.iHomeDetailID;
                clsHomeVehicle.iMakeID = tblHomeVehicles.iMakeID;
                clsHomeVehicle.strModel = tblHomeVehicles.strModel;
                clsHomeVehicle.iColourID = tblHomeVehicles.iColourID;
                clsHomeVehicle.strRegistrationNumber = tblHomeVehicles.strRegistrationNumber;

                clsHomeVehicle.strPathToImages = tblHomeVehicles.strPathToImages;
                clsHomeVehicle.strMasterImage = tblHomeVehicles.strMasterImage;
                clsHomeVehicle.bIsDeleted = tblHomeVehicles.bIsDeleted;

                //Add
                if (tblHomeVehicles.iHomeVehicleID == 0)
                {
                    tblHomeVehicles.dtAdded = DateTime.Now;
                    tblHomeVehicles.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomeVehicles.dtEdited = DateTime.Now;
                    tblHomeVehicles.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomeVehicles.Add(tblHomeVehicles);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomeVehicles.dtAdded = clsHomeVehicle.dtAdded;
                    tblHomeVehicles.iAddedBy = clsHomeVehicle.iAddedBy;
                    tblHomeVehicles.dtEdited = DateTime.Now;
                    tblHomeVehicles.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomeVehicles>().AddOrUpdate(tblHomeVehicles);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomeVehicleByID(int iHomeVehicleID)
        {
            tblHomeVehicles tblHomeVehicle = db.tblHomeVehicles.Find(iHomeVehicleID);
            if (tblHomeVehicle != null)
            {
                tblHomeVehicle.bIsDeleted = true;
                db.Entry(tblHomeVehicle).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomeVehicleExists(int iHomeVehicleID)
        {
            bool bHomeVehicleExists = db.tblHomeVehicles.Any(HomeVehicle => HomeVehicle.iHomeVehicleID == iHomeVehicleID && HomeVehicle.bIsDeleted == false);
            return bHomeVehicleExists;
        }

        //Convert database table to class
        public clsHomeVehicles convertHomeVehiclesTableToClass(tblHomeVehicles tblHomeVehicle)
        {
            clsHomeVehicles clsHomeVehicle = new clsHomeVehicles();

            clsHomeVehicle.iHomeVehicleID = tblHomeVehicle.iHomeVehicleID;
            clsHomeVehicle.dtAdded = tblHomeVehicle.dtAdded;
            clsHomeVehicle.iAddedBy = tblHomeVehicle.iAddedBy;
            clsHomeVehicle.dtEdited = tblHomeVehicle.dtEdited;
            clsHomeVehicle.iEditedBy = tblHomeVehicle.iEditedBy;

            clsHomeVehicle.iHomeDetailID = tblHomeVehicle.iHomeDetailID;
            clsHomeVehicle.iMakeID = tblHomeVehicle.iMakeID;
            clsHomeVehicle.strModel = tblHomeVehicle.strModel;
            clsHomeVehicle.iColourID = tblHomeVehicle.iColourID;
            clsHomeVehicle.strRegistrationNumber = tblHomeVehicle.strRegistrationNumber;

            clsHomeVehicle.strPathToImages = tblHomeVehicle.strPathToImages;
            clsHomeVehicle.strMasterImage = tblHomeVehicle.strMasterImage;
            clsHomeVehicle.bIsDeleted = tblHomeVehicle.bIsDeleted;

            return clsHomeVehicle;
        }
    }
}
