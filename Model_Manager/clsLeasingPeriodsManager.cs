﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsLeasingPeriodsManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsLeasingPeriods> getAllLeasingPeriodsList()
        {
            List<clsLeasingPeriods> lstLeasingPeriods = new List<clsLeasingPeriods>();
            var lstGetLeasingPeriodsList = db.tblLeasingPeriods.Where(LeasingPeriod => LeasingPeriod.bIsDeleted == false).ToList();

            if (lstGetLeasingPeriodsList.Count > 0)
            {
                //Home Details Manager
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetLeasingPeriodsList)
                {
                    clsLeasingPeriods clsLeasingPeriod = new clsLeasingPeriods();

                    clsLeasingPeriod.iLeasingPeriodID = item.iLeasingPeriodID;
                    clsLeasingPeriod.dtAdded = item.dtAdded;
                    clsLeasingPeriod.iAddedBy = item.iAddedBy;
                    clsLeasingPeriod.dtEdited = item.dtEdited;
                    clsLeasingPeriod.iEditedBy = item.iEditedBy;

                    clsLeasingPeriod.strTitle = item.strTitle;
                    clsLeasingPeriod.bIsDeleted = item.bIsDeleted;

                    clsLeasingPeriod.lstHomeDetails = new List<clsHomeDetails>();

                    if (item.tblHomeDetails.Count > 0)
                    {
                        foreach (var HomeDetailsItem in item.tblHomeDetails)
                        {
                            clsHomeDetails clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(HomeDetailsItem);
                            clsLeasingPeriod.lstHomeDetails.Add(clsHomeDetail);
                        }
                    }

                    lstLeasingPeriods.Add(clsLeasingPeriod);
                }
            }

            return lstLeasingPeriods;
        }

        //Get All Only
        public List<clsLeasingPeriods> getAllLeasingPeriodsOnlyList()
        {
            List<clsLeasingPeriods> lstLeasingPeriods = new List<clsLeasingPeriods>();
            var lstGetLeasingPeriodsList = db.tblLeasingPeriods.Where(LeasingPeriod => LeasingPeriod.bIsDeleted == false).ToList();

            if (lstGetLeasingPeriodsList.Count > 0)
            {
                //Home Details Manager
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                foreach (var item in lstGetLeasingPeriodsList)
                {
                    clsLeasingPeriods clsLeasingPeriod = new clsLeasingPeriods();

                    clsLeasingPeriod.iLeasingPeriodID = item.iLeasingPeriodID;
                    clsLeasingPeriod.dtAdded = item.dtAdded;
                    clsLeasingPeriod.iAddedBy = item.iAddedBy;
                    clsLeasingPeriod.dtEdited = item.dtEdited;
                    clsLeasingPeriod.iEditedBy = item.iEditedBy;

                    clsLeasingPeriod.strTitle = item.strTitle;
                    clsLeasingPeriod.bIsDeleted = item.bIsDeleted;

                    clsLeasingPeriod.lstHomeDetails = new List<clsHomeDetails>();

                    lstLeasingPeriods.Add(clsLeasingPeriod);
                }
            }

            return lstLeasingPeriods;
        }

        //Get
        public clsLeasingPeriods getLeasingPeriodByID(int iLeasingPeriodID)
        {
            clsLeasingPeriods clsLeasingPeriod = null;
            tblLeasingPeriods tblLeasingPeriods = db.tblLeasingPeriods.FirstOrDefault(LeasingPeriod => LeasingPeriod.iLeasingPeriodID == iLeasingPeriodID && LeasingPeriod.bIsDeleted == false);

            if (tblLeasingPeriods != null)
            {
                //Home Details Manager
                clsHomeDetailsManager clsHomeDetailsManager = new clsHomeDetailsManager();

                clsLeasingPeriod = new clsLeasingPeriods();

                clsLeasingPeriod.iLeasingPeriodID = tblLeasingPeriods.iLeasingPeriodID;
                clsLeasingPeriod.dtAdded = tblLeasingPeriods.dtAdded;
                clsLeasingPeriod.iAddedBy = tblLeasingPeriods.iAddedBy;
                clsLeasingPeriod.dtEdited = tblLeasingPeriods.dtEdited;
                clsLeasingPeriod.iEditedBy = tblLeasingPeriods.iEditedBy;

                clsLeasingPeriod.strTitle = tblLeasingPeriods.strTitle;
                clsLeasingPeriod.bIsDeleted = tblLeasingPeriods.bIsDeleted;

                clsLeasingPeriod.lstHomeDetails = new List<clsHomeDetails>();

                if (tblLeasingPeriods.tblHomeDetails.Count > 0)
                {
                    foreach (var HomeDetailsItem in tblLeasingPeriods.tblHomeDetails)
                    {
                        clsHomeDetails clsHomeDetail = clsHomeDetailsManager.convertHomeDetailsTableToClass(HomeDetailsItem);
                        clsLeasingPeriod.lstHomeDetails.Add(clsHomeDetail);
                    }
                }
            }

            return clsLeasingPeriod;
        }

        //Save
        public void saveLeasingPeriod(clsLeasingPeriods clsLeasingPeriod)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblLeasingPeriods tblLeasingPeriods = new tblLeasingPeriods();

                tblLeasingPeriods.iLeasingPeriodID = clsLeasingPeriod.iLeasingPeriodID;

                tblLeasingPeriods.strTitle = clsLeasingPeriod.strTitle;
                tblLeasingPeriods.bIsDeleted = clsLeasingPeriod.bIsDeleted;

                //Add
                if (tblLeasingPeriods.iLeasingPeriodID == 0)
                {
                    tblLeasingPeriods.dtAdded = DateTime.Now;
                    tblLeasingPeriods.iAddedBy = clsCMSUser.iCMSUserID;
                    tblLeasingPeriods.dtEdited = DateTime.Now;
                    tblLeasingPeriods.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblLeasingPeriods.Add(tblLeasingPeriods);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblLeasingPeriods.dtAdded = clsLeasingPeriod.dtAdded;
                    tblLeasingPeriods.iAddedBy = clsLeasingPeriod.iAddedBy;
                    tblLeasingPeriods.dtEdited = DateTime.Now;
                    tblLeasingPeriods.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblLeasingPeriods>().AddOrUpdate(tblLeasingPeriods);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeLeasingPeriodByID(int iLeasingPeriodID)
        {
            tblLeasingPeriods tblLeasingPeriod = db.tblLeasingPeriods.Find(iLeasingPeriodID);
            if (tblLeasingPeriod != null)
            {
                tblLeasingPeriod.bIsDeleted = true;
                db.Entry(tblLeasingPeriod).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfLeasingPeriodExists(int iLeasingPeriodID)
        {
            bool bLeasingPeriodExists = db.tblLeasingPeriods.Any(LeasingPeriod => LeasingPeriod.iLeasingPeriodID == iLeasingPeriodID && LeasingPeriod.bIsDeleted == false);
            return bLeasingPeriodExists;
        }

        //Convert database table to class
        public clsLeasingPeriods convertLeasingPeriodsTableToClass(tblLeasingPeriods tblLeasingPeriod)
        {
            clsLeasingPeriods clsLeasingPeriod = new clsLeasingPeriods();

            clsLeasingPeriod.iLeasingPeriodID = tblLeasingPeriod.iLeasingPeriodID;
            clsLeasingPeriod.dtAdded = tblLeasingPeriod.dtAdded;
            clsLeasingPeriod.iAddedBy = tblLeasingPeriod.iAddedBy;
            clsLeasingPeriod.dtEdited = tblLeasingPeriod.dtEdited;
            clsLeasingPeriod.iEditedBy = tblLeasingPeriod.iEditedBy;

            clsLeasingPeriod.strTitle = tblLeasingPeriod.strTitle;
            clsLeasingPeriod.bIsDeleted = tblLeasingPeriod.bIsDeleted;

            return clsLeasingPeriod;
        }
    }
}
