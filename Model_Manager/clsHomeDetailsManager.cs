﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SimpleComplexManagementCMS.Models;

namespace SimpleComplexManagementCMS.Model_Manager
{
    public class clsHomeDetailsManager
    {
        SimpleComplexManagementContext db = new SimpleComplexManagementContext();

        //Get All
        public List<clsHomeDetails> getAllHomeDetailsList()
        {
            List<clsHomeDetails> lstHomeDetails = new List<clsHomeDetails>();
            var lstGetHomeDetailsList = db.tblHomeDetails.Where(HomeDetail => HomeDetail.bIsDeleted == false).ToList();

            if (lstGetHomeDetailsList.Count > 0)
            {
                //Managers
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
                clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
                clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();

                foreach (var item in lstGetHomeDetailsList)
                {
                    clsHomeDetails clsHomeDetail = new clsHomeDetails();

                    clsHomeDetail.iHomeDetailID = item.iHomeDetailID;
                    clsHomeDetail.dtAdded = item.dtAdded;
                    clsHomeDetail.iAddedBy = item.iAddedBy;
                    clsHomeDetail.dtEdited = item.dtEdited;
                    clsHomeDetail.iEditedBy = item.iEditedBy;

                    clsHomeDetail.strEmailAddress = item.strEmailAddress;
                    clsHomeDetail.strLatitude = item.strLatitude;
                    clsHomeDetail.strLongitude = item.strLongitude;
                    clsHomeDetail.strUnitNumber = item.strUnitNumber;
                    clsHomeDetail.strBlockNumber = item.strBlockNumber;

                    clsHomeDetail.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeDetail.iNumberOfPeopleInHome = item.iNumberOfPeopleInHome;
                    clsHomeDetail.iNumberOfStaffMembers = item.iNumberOfStaffMembers;
                    clsHomeDetail.iNumberOfVehicles = item.iNumberOfVehicles;
                    clsHomeDetail.iNumberOfPets = item.iNumberOfPets;

                    clsHomeDetail.bIsRented = item.bIsRented;
                    clsHomeDetail.iLeasingPeriodID = item.iLeasingPeriodID;
                    clsHomeDetail.bIsDeleted = item.bIsDeleted;

                    clsHomeDetail.lstFormTickets = new List<clsFormTickets>();
                    clsHomeDetail.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
                    clsHomeDetail.lstHomeOccupants = new List<clsHomeOccupants>();
                    clsHomeDetail.lstHomePets = new List<clsHomePets>();
                    clsHomeDetail.lstHomeStaffMembers = new List<clsHomeStaffMembers>();
                    clsHomeDetail.lstHomeVehicles = new List<clsHomeVehicles>();

                    if (item.tblFormTickets.Count > 0)
                    {
                        foreach (var FormTicketsItem in item.tblFormTickets)
                        {
                            clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketsItem);
                            clsHomeDetail.lstFormTickets.Add(clsFormTicket);
                        }
                    }
                    if (item.tblHomeHistoryLogs.Count > 0)
                    {
                        foreach (var HomeHistoryLogsItem in item.tblHomeHistoryLogs)
                        {
                            clsHomeHistoryLogs clsHomeHistoryLog = clsHomeHistoryLogsManager.convertHomeHistoryLogsTableToClass(HomeHistoryLogsItem);
                            clsHomeDetail.lstHomeHistoryLogs.Add(clsHomeHistoryLog);
                        }
                    }
                    if (item.tblHomeOccupants.Count > 0)
                    {
                        foreach (var HomeOccupantsItem in item.tblHomeOccupants)
                        {
                            clsHomeOccupants clsHomeOccupant = clsHomeOccupantsManager.convertHomeOccupantsTableToClass(HomeOccupantsItem);
                            clsHomeDetail.lstHomeOccupants.Add(clsHomeOccupant);
                        }
                    }
                    if (item.tblHomePets.Count > 0)
                    {
                        foreach (var HomePetsItem in item.tblHomePets)
                        {
                            clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetsItem);
                            clsHomeDetail.lstHomePets.Add(clsHomePet);
                        }
                    }
                    if (item.tblHomeStaffMembers.Count > 0)
                    {
                        foreach (var HomeStaffMembersItem in item.tblHomeStaffMembers)
                        {
                            clsHomeStaffMembers clsHomeStaffMember = clsHomeStaffMembersManager.convertHomeStaffMembersTableToClass(HomeStaffMembersItem);
                            clsHomeDetail.lstHomeStaffMembers.Add(clsHomeStaffMember);
                        }
                    }
                    if (item.tblHomeVehicles.Count > 0)
                    {
                        foreach (var HomeVehiclesItem in item.tblHomeVehicles)
                        {
                            clsHomeVehicles clsHomeVehicle = clsHomeVehiclesManager.convertHomeVehiclesTableToClass(HomeVehiclesItem);
                            clsHomeDetail.lstHomeVehicles.Add(clsHomeVehicle);
                        }
                    }
                    if (item.tblHomeOwners != null)
                        clsHomeDetail.clsHomeOwner = clsHomeOwnersManager.convertHomeOwnersTableToClass(item.tblHomeOwners);
                    if (item.tblLeasingPeriods != null)
                        clsHomeDetail.clsLeasingPeriod = clsLeasingPeriodsManager.convertLeasingPeriodsTableToClass(item.tblLeasingPeriods);

                    lstHomeDetails.Add(clsHomeDetail);
                }
            }

            return lstHomeDetails;
        }

        //Get All Only
        public List<clsHomeDetails> getAllHomeDetailsOnlyList()
        {
            List<clsHomeDetails> lstHomeDetails = new List<clsHomeDetails>();
            var lstGetHomeDetailsList = db.tblHomeDetails.Where(HomeDetail => HomeDetail.bIsDeleted == false).ToList();

            if (lstGetHomeDetailsList.Count > 0)
            {
                //Managers
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
                clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
                clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();

                foreach (var item in lstGetHomeDetailsList)
                {
                    clsHomeDetails clsHomeDetail = new clsHomeDetails();

                    clsHomeDetail.iHomeDetailID = item.iHomeDetailID;
                    clsHomeDetail.dtAdded = item.dtAdded;
                    clsHomeDetail.iAddedBy = item.iAddedBy;
                    clsHomeDetail.dtEdited = item.dtEdited;
                    clsHomeDetail.iEditedBy = item.iEditedBy;

                    clsHomeDetail.strEmailAddress = item.strEmailAddress;
                    clsHomeDetail.strLatitude = item.strLatitude;
                    clsHomeDetail.strLongitude = item.strLongitude;
                    clsHomeDetail.strUnitNumber = item.strUnitNumber;
                    clsHomeDetail.strBlockNumber = item.strBlockNumber;

                    clsHomeDetail.iHomeOwnerID = item.iHomeOwnerID;
                    clsHomeDetail.iNumberOfPeopleInHome = item.iNumberOfPeopleInHome;
                    clsHomeDetail.iNumberOfStaffMembers = item.iNumberOfStaffMembers;
                    clsHomeDetail.iNumberOfVehicles = item.iNumberOfVehicles;
                    clsHomeDetail.iNumberOfPets = item.iNumberOfPets;

                    clsHomeDetail.bIsRented = item.bIsRented;
                    clsHomeDetail.iLeasingPeriodID = item.iLeasingPeriodID;
                    clsHomeDetail.bIsDeleted = item.bIsDeleted;

                    clsHomeDetail.lstFormTickets = new List<clsFormTickets>();
                    clsHomeDetail.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
                    clsHomeDetail.lstHomeOccupants = new List<clsHomeOccupants>();
                    clsHomeDetail.lstHomePets = new List<clsHomePets>();
                    clsHomeDetail.lstHomeStaffMembers = new List<clsHomeStaffMembers>();
                    clsHomeDetail.lstHomeVehicles = new List<clsHomeVehicles>();

                    lstHomeDetails.Add(clsHomeDetail);
                }
            }

            return lstHomeDetails;
        }

        //Get
        public clsHomeDetails getHomeDetailByID(int iHomeDetailID)
        {
            clsHomeDetails clsHomeDetail = null;
            tblHomeDetails tblHomeDetails = db.tblHomeDetails.FirstOrDefault(HomeDetail => HomeDetail.iHomeDetailID == iHomeDetailID && HomeDetail.bIsDeleted == false);

            if (tblHomeDetails != null)
            {
                //Managers
                clsFormTicketsManager clsFormTicketsManager = new clsFormTicketsManager();
                clsHomeHistoryLogsManager clsHomeHistoryLogsManager = new clsHomeHistoryLogsManager();
                clsHomeOccupantsManager clsHomeOccupantsManager = new clsHomeOccupantsManager();
                clsHomePetsManager clsHomePetsManager = new clsHomePetsManager();
                clsHomeStaffMembersManager clsHomeStaffMembersManager = new clsHomeStaffMembersManager();
                clsHomeVehiclesManager clsHomeVehiclesManager = new clsHomeVehiclesManager();
                clsHomeOwnersManager clsHomeOwnersManager = new clsHomeOwnersManager();
                clsLeasingPeriodsManager clsLeasingPeriodsManager = new clsLeasingPeriodsManager();

                clsHomeDetail = new clsHomeDetails();

                clsHomeDetail.iHomeDetailID = tblHomeDetails.iHomeDetailID;
                clsHomeDetail.dtAdded = tblHomeDetails.dtAdded;
                clsHomeDetail.iAddedBy = tblHomeDetails.iAddedBy;
                clsHomeDetail.dtEdited = tblHomeDetails.dtEdited;
                clsHomeDetail.iEditedBy = tblHomeDetails.iEditedBy;

                clsHomeDetail.strEmailAddress = tblHomeDetails.strEmailAddress;
                clsHomeDetail.strLatitude = tblHomeDetails.strLatitude;
                clsHomeDetail.strLongitude = tblHomeDetails.strLongitude;
                clsHomeDetail.strUnitNumber = tblHomeDetails.strUnitNumber;
                clsHomeDetail.strBlockNumber = tblHomeDetails.strBlockNumber;

                clsHomeDetail.iHomeOwnerID = tblHomeDetails.iHomeOwnerID;
                clsHomeDetail.iNumberOfPeopleInHome = tblHomeDetails.iNumberOfPeopleInHome;
                clsHomeDetail.iNumberOfStaffMembers = tblHomeDetails.iNumberOfStaffMembers;
                clsHomeDetail.iNumberOfVehicles = tblHomeDetails.iNumberOfVehicles;
                clsHomeDetail.iNumberOfPets = tblHomeDetails.iNumberOfPets;

                clsHomeDetail.bIsRented = tblHomeDetails.bIsRented;
                clsHomeDetail.iLeasingPeriodID = tblHomeDetails.iLeasingPeriodID;
                clsHomeDetail.bIsDeleted = tblHomeDetails.bIsDeleted;

                clsHomeDetail.lstFormTickets = new List<clsFormTickets>();
                clsHomeDetail.lstHomeHistoryLogs = new List<clsHomeHistoryLogs>();
                clsHomeDetail.lstHomeOccupants = new List<clsHomeOccupants>();
                clsHomeDetail.lstHomePets = new List<clsHomePets>();
                clsHomeDetail.lstHomeStaffMembers = new List<clsHomeStaffMembers>();
                clsHomeDetail.lstHomeVehicles = new List<clsHomeVehicles>();

                if (tblHomeDetails.tblFormTickets.Count > 0)
                {
                    foreach (var FormTicketstblHomeDetails in tblHomeDetails.tblFormTickets)
                    {
                        clsFormTickets clsFormTicket = clsFormTicketsManager.convertFormTicketsTableToClass(FormTicketstblHomeDetails);
                        clsHomeDetail.lstFormTickets.Add(clsFormTicket);
                    }
                }
                if (tblHomeDetails.tblHomeHistoryLogs.Count > 0)
                {
                    foreach (var HomeHistoryLogstblHomeDetails in tblHomeDetails.tblHomeHistoryLogs)
                    {
                        clsHomeHistoryLogs clsHomeHistoryLog = clsHomeHistoryLogsManager.convertHomeHistoryLogsTableToClass(HomeHistoryLogstblHomeDetails);
                        clsHomeDetail.lstHomeHistoryLogs.Add(clsHomeHistoryLog);
                    }
                }
                if (tblHomeDetails.tblHomeOccupants.Count > 0)
                {
                    foreach (var HomeOccupantstblHomeDetails in tblHomeDetails.tblHomeOccupants)
                    {
                        clsHomeOccupants clsHomeOccupant = clsHomeOccupantsManager.convertHomeOccupantsTableToClass(HomeOccupantstblHomeDetails);
                        clsHomeDetail.lstHomeOccupants.Add(clsHomeOccupant);
                    }
                }
                if (tblHomeDetails.tblHomePets.Count > 0)
                {
                    foreach (var HomePetstblHomeDetails in tblHomeDetails.tblHomePets)
                    {
                        clsHomePets clsHomePet = clsHomePetsManager.convertHomePetsTableToClass(HomePetstblHomeDetails);
                        clsHomeDetail.lstHomePets.Add(clsHomePet);
                    }
                }
                if (tblHomeDetails.tblHomeStaffMembers.Count > 0)
                {
                    foreach (var HomeStaffMemberstblHomeDetails in tblHomeDetails.tblHomeStaffMembers)
                    {
                        clsHomeStaffMembers clsHomeStaffMember = clsHomeStaffMembersManager.convertHomeStaffMembersTableToClass(HomeStaffMemberstblHomeDetails);
                        clsHomeDetail.lstHomeStaffMembers.Add(clsHomeStaffMember);
                    }
                }
                if (tblHomeDetails.tblHomeVehicles.Count > 0)
                {
                    foreach (var HomeVehiclestblHomeDetails in tblHomeDetails.tblHomeVehicles)
                    {
                        clsHomeVehicles clsHomeVehicle = clsHomeVehiclesManager.convertHomeVehiclesTableToClass(HomeVehiclestblHomeDetails);
                        clsHomeDetail.lstHomeVehicles.Add(clsHomeVehicle);
                    }
                }
                if (tblHomeDetails.tblHomeOwners != null)
                    clsHomeDetail.clsHomeOwner = clsHomeOwnersManager.convertHomeOwnersTableToClass(tblHomeDetails.tblHomeOwners);
                if (tblHomeDetails.tblLeasingPeriods != null)
                    clsHomeDetail.clsLeasingPeriod = clsLeasingPeriodsManager.convertLeasingPeriodsTableToClass(tblHomeDetails.tblLeasingPeriods);
            }

            return clsHomeDetail;
        }

        //Save
        public void saveHomeDetail(clsHomeDetails clsHomeDetail)
        {
            if (HttpContext.Current.Session["clsCMSUser"] != null)
            {
                clsCMSUsers clsCMSUser = (clsCMSUsers)HttpContext.Current.Session["clsCMSUser"];
                tblHomeDetails tblHomeDetails = new tblHomeDetails();

                tblHomeDetails.iHomeDetailID = clsHomeDetail.iHomeDetailID;

                tblHomeDetails.strEmailAddress = clsHomeDetail.strEmailAddress;
                tblHomeDetails.strLatitude = clsHomeDetail.strLatitude;
                tblHomeDetails.strLongitude = clsHomeDetail.strLongitude;
                tblHomeDetails.strUnitNumber = clsHomeDetail.strUnitNumber;
                tblHomeDetails.strBlockNumber = clsHomeDetail.strBlockNumber;

                tblHomeDetails.iHomeOwnerID = clsHomeDetail.iHomeOwnerID;
                tblHomeDetails.iNumberOfPeopleInHome = clsHomeDetail.iNumberOfPeopleInHome;
                tblHomeDetails.iNumberOfStaffMembers = clsHomeDetail.iNumberOfStaffMembers;
                tblHomeDetails.iNumberOfVehicles = clsHomeDetail.iNumberOfVehicles;
                tblHomeDetails.iNumberOfPets = clsHomeDetail.iNumberOfPets;

                tblHomeDetails.bIsRented = clsHomeDetail.bIsRented;
                tblHomeDetails.iLeasingPeriodID = clsHomeDetail.iLeasingPeriodID;
                tblHomeDetails.bIsDeleted = clsHomeDetail.bIsDeleted;

                //Add
                if (tblHomeDetails.iHomeDetailID == 0)
                {
                    tblHomeDetails.dtAdded = DateTime.Now;
                    tblHomeDetails.iAddedBy = clsCMSUser.iCMSUserID;
                    tblHomeDetails.dtEdited = DateTime.Now;
                    tblHomeDetails.iEditedBy = clsCMSUser.iCMSUserID;

                    db.tblHomeDetails.Add(tblHomeDetails);
                    db.SaveChanges();
                }
                //Update
                else
                {
                    tblHomeDetails.dtAdded = clsHomeDetail.dtAdded;
                    tblHomeDetails.iAddedBy = clsHomeDetail.iAddedBy;
                    tblHomeDetails.dtEdited = DateTime.Now;
                    tblHomeDetails.iEditedBy = clsCMSUser.iCMSUserID;

                    db.Set<tblHomeDetails>().AddOrUpdate(tblHomeDetails);
                    db.SaveChanges();
                }
            }
        }

        //Remove
        public void removeHomeDetailByID(int iHomeDetailID)
        {
            tblHomeDetails tblHomeDetail = db.tblHomeDetails.Find(iHomeDetailID);
            if (tblHomeDetail != null)
            {
                tblHomeDetail.bIsDeleted = true;
                db.Entry(tblHomeDetail).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        //Check
        public bool checkIfHomeDetailExists(int iHomeDetailID)
        {
            bool bHomeDetailExists = db.tblHomeDetails.Any(HomeDetail => HomeDetail.iHomeDetailID == iHomeDetailID && HomeDetail.bIsDeleted == false);
            return bHomeDetailExists;
        }

        //Convert database table to class
        public clsHomeDetails convertHomeDetailsTableToClass(tblHomeDetails tblHomeDetail)
        {
            clsHomeDetails clsHomeDetail = new clsHomeDetails();

            clsHomeDetail.iHomeDetailID = tblHomeDetail.iHomeDetailID;
            clsHomeDetail.dtAdded = tblHomeDetail.dtAdded;
            clsHomeDetail.iAddedBy = tblHomeDetail.iAddedBy;
            clsHomeDetail.dtEdited = tblHomeDetail.dtEdited;
            clsHomeDetail.iEditedBy = tblHomeDetail.iEditedBy;

            clsHomeDetail.strEmailAddress = tblHomeDetail.strEmailAddress;
            clsHomeDetail.strLatitude = tblHomeDetail.strLatitude;
            clsHomeDetail.strLongitude = tblHomeDetail.strLongitude;
            clsHomeDetail.strUnitNumber = tblHomeDetail.strUnitNumber;
            clsHomeDetail.strBlockNumber = tblHomeDetail.strBlockNumber;

            clsHomeDetail.iHomeOwnerID = tblHomeDetail.iHomeOwnerID;
            clsHomeDetail.iNumberOfPeopleInHome = tblHomeDetail.iNumberOfPeopleInHome;
            clsHomeDetail.iNumberOfStaffMembers = tblHomeDetail.iNumberOfStaffMembers;
            clsHomeDetail.iNumberOfVehicles = tblHomeDetail.iNumberOfVehicles;
            clsHomeDetail.iNumberOfPets = tblHomeDetail.iNumberOfPets;

            clsHomeDetail.bIsRented = tblHomeDetail.bIsRented;
            clsHomeDetail.iLeasingPeriodID = tblHomeDetail.iLeasingPeriodID;
            clsHomeDetail.bIsDeleted = tblHomeDetail.bIsDeleted;

            return clsHomeDetail;
        }
    }
}
